import { Item } from "./item";

export class SystemUserRole implements Item {
  public "@id"?: string;

  constructor(_id?: string) {
    this["@id"] = _id;
  }
}
