import { Item } from "./item";

export class ServiceSite implements Item {
  public "@id"?: string;

  constructor(
    _id?: string,
    public name?: string,
    public dateCreated?: Date,
    public dateUpdated?: Date
  ) {
    this["@id"] = _id;
  }
}
