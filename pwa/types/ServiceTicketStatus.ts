import { Item } from "./item";

export class ServiceTicketStatus implements Item {
  public "@id"?: string;

  constructor(_id?: string) {
    this["@id"] = _id;
  }
}
