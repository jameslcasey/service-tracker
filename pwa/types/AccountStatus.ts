import { Item } from "./item";

export class AccountStatus implements Item {
  public "@id"?: string;

  constructor(_id?: string) {
    this["@id"] = _id;
  }
}
