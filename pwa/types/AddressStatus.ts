import { Item } from "./item";

export class AddressStatus implements Item {
  public "@id"?: string;

  constructor(_id?: string) {
    this["@id"] = _id;
  }
}
