import { GetServerSideProps, NextComponentType, NextPageContext } from "next";
import Head from "next/head";
import { dehydrate, QueryClient, useQuery } from "react-query";

import Pagination from "../../components/common/Pagination";
import { List } from "../../components/addresstype/List";
import { PagedCollection } from "../../types/collection";
import { AddressType } from "../../types/AddressType";
import { fetch, FetchResponse } from "../../utils/dataAccess";
import { useMercure } from "../../utils/mercure";

const getAddressTypes = async () =>
  await fetch<PagedCollection<AddressType>>("/address_types");

const Page: NextComponentType<NextPageContext> = () => {
  const { data: { data: addresstypes, hubURL } = { hubURL: null } } = useQuery<
    FetchResponse<PagedCollection<AddressType>> | undefined
  >("address_types", getAddressTypes);
  const collection = useMercure(addresstypes, hubURL);

  if (!collection || !collection["hydra:member"]) return null;

  return (
    <div>
      <div>
        <Head>
          <title>AddressType List</title>
        </Head>
      </div>
      <List addresstypes={collection["hydra:member"]} />
      <Pagination collection={collection} />
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = async () => {
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery("address_types", getAddressTypes);

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
  };
};

export default Page;
