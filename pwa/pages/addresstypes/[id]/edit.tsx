import {
  GetStaticPaths,
  GetStaticProps,
  NextComponentType,
  NextPageContext,
} from "next";
import DefaultErrorPage from "next/error";
import Head from "next/head";
import { useRouter } from "next/router";
import { dehydrate, QueryClient, useQuery } from "react-query";

import { Form } from "../../../components/addresstype/Form";
import { PagedCollection } from "../../../types/collection";
import { AddressType } from "../../../types/AddressType";
import { fetch, FetchResponse, getPaths } from "../../../utils/dataAccess";

const getAddressType = async (id: string | string[] | undefined) =>
  id
    ? await fetch<AddressType>(`/address_types/${id}`)
    : Promise.resolve(undefined);

const Page: NextComponentType<NextPageContext> = () => {
  const router = useRouter();
  const { id } = router.query;

  const { data: { data: addresstype } = {} } = useQuery<
    FetchResponse<AddressType> | undefined
  >(["addresstype", id], () => getAddressType(id));

  if (!addresstype) {
    return <DefaultErrorPage statusCode={404} />;
  }

  return (
    <div>
      <div>
        <Head>
          <title>
            {addresstype && `Edit AddressType ${addresstype["@id"]}`}
          </title>
        </Head>
      </div>
      <Form addresstype={addresstype} />
    </div>
  );
};

export const getStaticProps: GetStaticProps = async ({
  params: { id } = {},
}) => {
  if (!id) throw new Error("id not in query param");
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(["addresstype", id], () =>
    getAddressType(id)
  );

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
    revalidate: 1,
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const response = await fetch<PagedCollection<AddressType>>("/address_types");
  const paths = await getPaths(
    response,
    "address_types",
    "/addresstypes/[id]/edit"
  );

  return {
    paths,
    fallback: true,
  };
};

export default Page;
