import {
  GetStaticPaths,
  GetStaticProps,
  NextComponentType,
  NextPageContext,
} from "next";
import DefaultErrorPage from "next/error";
import Head from "next/head";
import { useRouter } from "next/router";
import { dehydrate, QueryClient, useQuery } from "react-query";

import { Show } from "../../../components/addresstype/Show";
import { PagedCollection } from "../../../types/collection";
import { AddressType } from "../../../types/AddressType";
import { fetch, FetchResponse, getPaths } from "../../../utils/dataAccess";
import { useMercure } from "../../../utils/mercure";

const getAddressType = async (id: string | string[] | undefined) =>
  id
    ? await fetch<AddressType>(`/address_types/${id}`)
    : Promise.resolve(undefined);

const Page: NextComponentType<NextPageContext> = () => {
  const router = useRouter();
  const { id } = router.query;

  const {
    data: { data: addresstype, hubURL, text } = { hubURL: null, text: "" },
  } = useQuery<FetchResponse<AddressType> | undefined>(
    ["addresstype", id],
    () => getAddressType(id)
  );
  const addresstypeData = useMercure(addresstype, hubURL);

  if (!addresstypeData) {
    return <DefaultErrorPage statusCode={404} />;
  }

  return (
    <div>
      <div>
        <Head>
          <title>{`Show AddressType ${addresstypeData["@id"]}`}</title>
        </Head>
      </div>
      <Show addresstype={addresstypeData} text={text} />
    </div>
  );
};

export const getStaticProps: GetStaticProps = async ({
  params: { id } = {},
}) => {
  if (!id) throw new Error("id not in query param");
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(["addresstype", id], () =>
    getAddressType(id)
  );

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
    revalidate: 1,
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const response = await fetch<PagedCollection<AddressType>>("/address_types");
  const paths = await getPaths(response, "address_types", "/addresstypes/[id]");

  return {
    paths,
    fallback: true,
  };
};

export default Page;
