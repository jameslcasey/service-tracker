import { GetServerSideProps, NextComponentType, NextPageContext } from "next";
import Head from "next/head";
import { dehydrate, QueryClient, useQuery } from "react-query";

import Pagination from "../../components/common/Pagination";
import { List } from "../../components/product/List";
import { PagedCollection } from "../../types/collection";
import { Product } from "../../types/Product";
import { fetch, FetchResponse } from "../../utils/dataAccess";
import { useMercure } from "../../utils/mercure";

const getProducts = async () =>
  await fetch<PagedCollection<Product>>("/products");

const Page: NextComponentType<NextPageContext> = () => {
  const { data: { data: products, hubURL } = { hubURL: null } } = useQuery<
    FetchResponse<PagedCollection<Product>> | undefined
  >("products", getProducts);
  const collection = useMercure(products, hubURL);

  if (!collection || !collection["hydra:member"]) return null;

  return (
    <div>
      <div>
        <Head>
          <title>Product List</title>
        </Head>
      </div>
      <List products={collection["hydra:member"]} />
      <Pagination collection={collection} />
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = async () => {
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery("products", getProducts);

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
  };
};

export default Page;
