import {
  GetStaticPaths,
  GetStaticProps,
  NextComponentType,
  NextPageContext,
} from "next";
import DefaultErrorPage from "next/error";
import Head from "next/head";
import { useRouter } from "next/router";
import { dehydrate, QueryClient, useQuery } from "react-query";

import { Show } from "../../../components/productstatus/Show";
import { PagedCollection } from "../../../types/collection";
import { ProductStatus } from "../../../types/ProductStatus";
import { fetch, FetchResponse, getPaths } from "../../../utils/dataAccess";
import { useMercure } from "../../../utils/mercure";

const getProductStatus = async (id: string | string[] | undefined) =>
  id
    ? await fetch<ProductStatus>(`/product_statuses/${id}`)
    : Promise.resolve(undefined);

const Page: NextComponentType<NextPageContext> = () => {
  const router = useRouter();
  const { id } = router.query;

  const {
    data: { data: productstatus, hubURL, text } = { hubURL: null, text: "" },
  } = useQuery<FetchResponse<ProductStatus> | undefined>(
    ["productstatus", id],
    () => getProductStatus(id)
  );
  const productstatusData = useMercure(productstatus, hubURL);

  if (!productstatusData) {
    return <DefaultErrorPage statusCode={404} />;
  }

  return (
    <div>
      <div>
        <Head>
          <title>{`Show ProductStatus ${productstatusData["@id"]}`}</title>
        </Head>
      </div>
      <Show productstatus={productstatusData} text={text} />
    </div>
  );
};

export const getStaticProps: GetStaticProps = async ({
  params: { id } = {},
}) => {
  if (!id) throw new Error("id not in query param");
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(["productstatus", id], () =>
    getProductStatus(id)
  );

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
    revalidate: 1,
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const response = await fetch<PagedCollection<ProductStatus>>(
    "/product_statuses"
  );
  const paths = await getPaths(
    response,
    "product_statuses",
    "/productstatuss/[id]"
  );

  return {
    paths,
    fallback: true,
  };
};

export default Page;
