import { GetServerSideProps, NextComponentType, NextPageContext } from "next";
import Head from "next/head";
import { dehydrate, QueryClient, useQuery } from "react-query";

import Pagination from "../../components/common/Pagination";
import { List } from "../../components/productstatus/List";
import { PagedCollection } from "../../types/collection";
import { ProductStatus } from "../../types/ProductStatus";
import { fetch, FetchResponse } from "../../utils/dataAccess";
import { useMercure } from "../../utils/mercure";

const getProductStatuss = async () =>
  await fetch<PagedCollection<ProductStatus>>("/product_statuses");

const Page: NextComponentType<NextPageContext> = () => {
  const { data: { data: productstatuss, hubURL } = { hubURL: null } } =
    useQuery<FetchResponse<PagedCollection<ProductStatus>> | undefined>(
      "product_statuses",
      getProductStatuss
    );
  const collection = useMercure(productstatuss, hubURL);

  if (!collection || !collection["hydra:member"]) return null;

  return (
    <div>
      <div>
        <Head>
          <title>ProductStatus List</title>
        </Head>
      </div>
      <List productstatuss={collection["hydra:member"]} />
      <Pagination collection={collection} />
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = async () => {
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery("product_statuses", getProductStatuss);

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
  };
};

export default Page;
