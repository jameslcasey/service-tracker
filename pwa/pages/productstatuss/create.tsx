import { NextComponentType, NextPageContext } from "next";
import Head from "next/head";

import { Form } from "../../components/productstatus/Form";

const Page: NextComponentType<NextPageContext> = () => (
  <div>
    <div>
      <Head>
        <title>Create ProductStatus</title>
      </Head>
    </div>
    <Form />
  </div>
);

export default Page;
