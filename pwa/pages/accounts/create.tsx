import { NextComponentType, NextPageContext } from "next";
import Head from "next/head";

import { Form } from "../../components/account/Form";

const Page: NextComponentType<NextPageContext> = () => (
  <div>
    <div>
      <Head>
        <title>Create Account</title>
      </Head>
    </div>
    <Form />
  </div>
);

export default Page;
