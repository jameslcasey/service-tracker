import { GetServerSideProps, NextComponentType, NextPageContext } from "next";
import Head from "next/head";
import { dehydrate, QueryClient, useQuery } from "react-query";

import Pagination from "../../components/common/Pagination";
import { List } from "../../components/account/List";
import { PagedCollection } from "../../types/collection";
import { Account } from "../../types/Account";
import { fetch, FetchResponse } from "../../utils/dataAccess";
import { useMercure } from "../../utils/mercure";

const getAccounts = async () =>
  await fetch<PagedCollection<Account>>("/accounts");

const Page: NextComponentType<NextPageContext> = () => {
  const { data: { data: accounts, hubURL } = { hubURL: null } } = useQuery<
    FetchResponse<PagedCollection<Account>> | undefined
  >("accounts", getAccounts);
  const collection = useMercure(accounts, hubURL);

  if (!collection || !collection["hydra:member"]) return null;

  return (
    <div>
      <div>
        <Head>
          <title>Account List</title>
        </Head>
      </div>
      <List accounts={collection["hydra:member"]} />
      <Pagination collection={collection} />
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = async () => {
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery("accounts", getAccounts);

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
  };
};

export default Page;
