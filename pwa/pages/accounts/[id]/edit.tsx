import {
  GetStaticPaths,
  GetStaticProps,
  NextComponentType,
  NextPageContext,
} from "next";
import DefaultErrorPage from "next/error";
import Head from "next/head";
import { useRouter } from "next/router";
import { dehydrate, QueryClient, useQuery } from "react-query";

import { Form } from "../../../components/account/Form";
import { PagedCollection } from "../../../types/collection";
import { Account } from "../../../types/Account";
import { fetch, FetchResponse, getPaths } from "../../../utils/dataAccess";

const getAccount = async (id: string | string[] | undefined) =>
  id ? await fetch<Account>(`/accounts/${id}`) : Promise.resolve(undefined);

const Page: NextComponentType<NextPageContext> = () => {
  const router = useRouter();
  const { id } = router.query;

  const { data: { data: account } = {} } = useQuery<
    FetchResponse<Account> | undefined
  >(["account", id], () => getAccount(id));

  if (!account) {
    return <DefaultErrorPage statusCode={404} />;
  }

  return (
    <div>
      <div>
        <Head>
          <title>{account && `Edit Account ${account["@id"]}`}</title>
        </Head>
      </div>
      <Form account={account} />
    </div>
  );
};

export const getStaticProps: GetStaticProps = async ({
  params: { id } = {},
}) => {
  if (!id) throw new Error("id not in query param");
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(["account", id], () => getAccount(id));

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
    revalidate: 1,
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const response = await fetch<PagedCollection<Account>>("/accounts");
  const paths = await getPaths(response, "accounts", "/accounts/[id]/edit");

  return {
    paths,
    fallback: true,
  };
};

export default Page;
