import {
  GetStaticPaths,
  GetStaticProps,
  NextComponentType,
  NextPageContext,
} from "next";
import DefaultErrorPage from "next/error";
import Head from "next/head";
import { useRouter } from "next/router";
import { dehydrate, QueryClient, useQuery } from "react-query";

import { Show } from "../../../components/servicesite/Show";
import { PagedCollection } from "../../../types/collection";
import { ServiceSite } from "../../../types/ServiceSite";
import { fetch, FetchResponse, getPaths } from "../../../utils/dataAccess";
import { useMercure } from "../../../utils/mercure";

const getServiceSite = async (id: string | string[] | undefined) =>
  id
    ? await fetch<ServiceSite>(`/service_sites/${id}`)
    : Promise.resolve(undefined);

const Page: NextComponentType<NextPageContext> = () => {
  const router = useRouter();
  const { id } = router.query;

  const {
    data: { data: servicesite, hubURL, text } = { hubURL: null, text: "" },
  } = useQuery<FetchResponse<ServiceSite> | undefined>(
    ["servicesite", id],
    () => getServiceSite(id)
  );
  const servicesiteData = useMercure(servicesite, hubURL);

  if (!servicesiteData) {
    return <DefaultErrorPage statusCode={404} />;
  }

  return (
    <div>
      <div>
        <Head>
          <title>{`Show ServiceSite ${servicesiteData["@id"]}`}</title>
        </Head>
      </div>
      <Show servicesite={servicesiteData} text={text} />
    </div>
  );
};

export const getStaticProps: GetStaticProps = async ({
  params: { id } = {},
}) => {
  if (!id) throw new Error("id not in query param");
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(["servicesite", id], () =>
    getServiceSite(id)
  );

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
    revalidate: 1,
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const response = await fetch<PagedCollection<ServiceSite>>("/service_sites");
  const paths = await getPaths(response, "service_sites", "/servicesites/[id]");

  return {
    paths,
    fallback: true,
  };
};

export default Page;
