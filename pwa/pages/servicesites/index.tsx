import { GetServerSideProps, NextComponentType, NextPageContext } from "next";
import Head from "next/head";
import { dehydrate, QueryClient, useQuery } from "react-query";

import Pagination from "../../components/common/Pagination";
import { List } from "../../components/servicesite/List";
import { PagedCollection } from "../../types/collection";
import { ServiceSite } from "../../types/ServiceSite";
import { fetch, FetchResponse } from "../../utils/dataAccess";
import { useMercure } from "../../utils/mercure";

const getServiceSites = async () =>
  await fetch<PagedCollection<ServiceSite>>("/service_sites");

const Page: NextComponentType<NextPageContext> = () => {
  const { data: { data: servicesites, hubURL } = { hubURL: null } } = useQuery<
    FetchResponse<PagedCollection<ServiceSite>> | undefined
  >("service_sites", getServiceSites);
  const collection = useMercure(servicesites, hubURL);

  if (!collection || !collection["hydra:member"]) return null;

  return (
    <div>
      <div>
        <Head>
          <title>ServiceSite List</title>
        </Head>
      </div>
      <List servicesites={collection["hydra:member"]} />
      <Pagination collection={collection} />
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = async () => {
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery("service_sites", getServiceSites);

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
  };
};

export default Page;
