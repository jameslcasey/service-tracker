import { NextComponentType, NextPageContext } from "next";
import Head from "next/head";

import { Form } from "../../components/servicesite/Form";

const Page: NextComponentType<NextPageContext> = () => (
  <div>
    <div>
      <Head>
        <title>Create ServiceSite</title>
      </Head>
    </div>
    <Form />
  </div>
);

export default Page;
