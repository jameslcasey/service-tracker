import { NextComponentType, NextPageContext } from "next";
import Head from "next/head";

import { Form } from "../../components/servicerequeststatus/Form";

const Page: NextComponentType<NextPageContext> = () => (
  <div>
    <div>
      <Head>
        <title>Create ServiceRequestStatus</title>
      </Head>
    </div>
    <Form />
  </div>
);

export default Page;
