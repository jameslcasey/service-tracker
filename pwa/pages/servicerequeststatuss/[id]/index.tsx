import {
  GetStaticPaths,
  GetStaticProps,
  NextComponentType,
  NextPageContext,
} from "next";
import DefaultErrorPage from "next/error";
import Head from "next/head";
import { useRouter } from "next/router";
import { dehydrate, QueryClient, useQuery } from "react-query";

import { Show } from "../../../components/servicerequeststatus/Show";
import { PagedCollection } from "../../../types/collection";
import { ServiceRequestStatus } from "../../../types/ServiceRequestStatus";
import { fetch, FetchResponse, getPaths } from "../../../utils/dataAccess";
import { useMercure } from "../../../utils/mercure";

const getServiceRequestStatus = async (id: string | string[] | undefined) =>
  id
    ? await fetch<ServiceRequestStatus>(`/service_request_statuses/${id}`)
    : Promise.resolve(undefined);

const Page: NextComponentType<NextPageContext> = () => {
  const router = useRouter();
  const { id } = router.query;

  const {
    data: { data: servicerequeststatus, hubURL, text } = {
      hubURL: null,
      text: "",
    },
  } = useQuery<FetchResponse<ServiceRequestStatus> | undefined>(
    ["servicerequeststatus", id],
    () => getServiceRequestStatus(id)
  );
  const servicerequeststatusData = useMercure(servicerequeststatus, hubURL);

  if (!servicerequeststatusData) {
    return <DefaultErrorPage statusCode={404} />;
  }

  return (
    <div>
      <div>
        <Head>
          <title>{`Show ServiceRequestStatus ${servicerequeststatusData["@id"]}`}</title>
        </Head>
      </div>
      <Show servicerequeststatus={servicerequeststatusData} text={text} />
    </div>
  );
};

export const getStaticProps: GetStaticProps = async ({
  params: { id } = {},
}) => {
  if (!id) throw new Error("id not in query param");
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(["servicerequeststatus", id], () =>
    getServiceRequestStatus(id)
  );

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
    revalidate: 1,
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const response = await fetch<PagedCollection<ServiceRequestStatus>>(
    "/service_request_statuses"
  );
  const paths = await getPaths(
    response,
    "service_request_statuses",
    "/servicerequeststatuss/[id]"
  );

  return {
    paths,
    fallback: true,
  };
};

export default Page;
