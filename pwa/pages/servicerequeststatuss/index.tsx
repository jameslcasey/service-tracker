import { GetServerSideProps, NextComponentType, NextPageContext } from "next";
import Head from "next/head";
import { dehydrate, QueryClient, useQuery } from "react-query";

import Pagination from "../../components/common/Pagination";
import { List } from "../../components/servicerequeststatus/List";
import { PagedCollection } from "../../types/collection";
import { ServiceRequestStatus } from "../../types/ServiceRequestStatus";
import { fetch, FetchResponse } from "../../utils/dataAccess";
import { useMercure } from "../../utils/mercure";

const getServiceRequestStatuss = async () =>
  await fetch<PagedCollection<ServiceRequestStatus>>(
    "/service_request_statuses"
  );

const Page: NextComponentType<NextPageContext> = () => {
  const { data: { data: servicerequeststatuss, hubURL } = { hubURL: null } } =
    useQuery<FetchResponse<PagedCollection<ServiceRequestStatus>> | undefined>(
      "service_request_statuses",
      getServiceRequestStatuss
    );
  const collection = useMercure(servicerequeststatuss, hubURL);

  if (!collection || !collection["hydra:member"]) return null;

  return (
    <div>
      <div>
        <Head>
          <title>ServiceRequestStatus List</title>
        </Head>
      </div>
      <List servicerequeststatuss={collection["hydra:member"]} />
      <Pagination collection={collection} />
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = async () => {
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(
    "service_request_statuses",
    getServiceRequestStatuss
  );

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
  };
};

export default Page;
