import {
  GetStaticPaths,
  GetStaticProps,
  NextComponentType,
  NextPageContext,
} from "next";
import DefaultErrorPage from "next/error";
import Head from "next/head";
import { useRouter } from "next/router";
import { dehydrate, QueryClient, useQuery } from "react-query";

import { Show } from "../../../components/serviceticketstatus/Show";
import { PagedCollection } from "../../../types/collection";
import { ServiceTicketStatus } from "../../../types/ServiceTicketStatus";
import { fetch, FetchResponse, getPaths } from "../../../utils/dataAccess";
import { useMercure } from "../../../utils/mercure";

const getServiceTicketStatus = async (id: string | string[] | undefined) =>
  id
    ? await fetch<ServiceTicketStatus>(`/service_ticket_statuses/${id}`)
    : Promise.resolve(undefined);

const Page: NextComponentType<NextPageContext> = () => {
  const router = useRouter();
  const { id } = router.query;

  const {
    data: { data: serviceticketstatus, hubURL, text } = {
      hubURL: null,
      text: "",
    },
  } = useQuery<FetchResponse<ServiceTicketStatus> | undefined>(
    ["serviceticketstatus", id],
    () => getServiceTicketStatus(id)
  );
  const serviceticketstatusData = useMercure(serviceticketstatus, hubURL);

  if (!serviceticketstatusData) {
    return <DefaultErrorPage statusCode={404} />;
  }

  return (
    <div>
      <div>
        <Head>
          <title>{`Show ServiceTicketStatus ${serviceticketstatusData["@id"]}`}</title>
        </Head>
      </div>
      <Show serviceticketstatus={serviceticketstatusData} text={text} />
    </div>
  );
};

export const getStaticProps: GetStaticProps = async ({
  params: { id } = {},
}) => {
  if (!id) throw new Error("id not in query param");
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(["serviceticketstatus", id], () =>
    getServiceTicketStatus(id)
  );

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
    revalidate: 1,
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const response = await fetch<PagedCollection<ServiceTicketStatus>>(
    "/service_ticket_statuses"
  );
  const paths = await getPaths(
    response,
    "service_ticket_statuses",
    "/serviceticketstatuss/[id]"
  );

  return {
    paths,
    fallback: true,
  };
};

export default Page;
