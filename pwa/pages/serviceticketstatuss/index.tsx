import { GetServerSideProps, NextComponentType, NextPageContext } from "next";
import Head from "next/head";
import { dehydrate, QueryClient, useQuery } from "react-query";

import Pagination from "../../components/common/Pagination";
import { List } from "../../components/serviceticketstatus/List";
import { PagedCollection } from "../../types/collection";
import { ServiceTicketStatus } from "../../types/ServiceTicketStatus";
import { fetch, FetchResponse } from "../../utils/dataAccess";
import { useMercure } from "../../utils/mercure";

const getServiceTicketStatuss = async () =>
  await fetch<PagedCollection<ServiceTicketStatus>>("/service_ticket_statuses");

const Page: NextComponentType<NextPageContext> = () => {
  const { data: { data: serviceticketstatuss, hubURL } = { hubURL: null } } =
    useQuery<FetchResponse<PagedCollection<ServiceTicketStatus>> | undefined>(
      "service_ticket_statuses",
      getServiceTicketStatuss
    );
  const collection = useMercure(serviceticketstatuss, hubURL);

  if (!collection || !collection["hydra:member"]) return null;

  return (
    <div>
      <div>
        <Head>
          <title>ServiceTicketStatus List</title>
        </Head>
      </div>
      <List serviceticketstatuss={collection["hydra:member"]} />
      <Pagination collection={collection} />
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = async () => {
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(
    "service_ticket_statuses",
    getServiceTicketStatuss
  );

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
  };
};

export default Page;
