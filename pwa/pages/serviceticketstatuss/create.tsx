import { NextComponentType, NextPageContext } from "next";
import Head from "next/head";

import { Form } from "../../components/serviceticketstatus/Form";

const Page: NextComponentType<NextPageContext> = () => (
  <div>
    <div>
      <Head>
        <title>Create ServiceTicketStatus</title>
      </Head>
    </div>
    <Form />
  </div>
);

export default Page;
