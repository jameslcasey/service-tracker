import { GetServerSideProps, NextComponentType, NextPageContext } from "next";
import Head from "next/head";
import { dehydrate, QueryClient, useQuery } from "react-query";

import Pagination from "../../components/common/Pagination";
import { List } from "../../components/address/List";
import { PagedCollection } from "../../types/collection";
import { Address } from "../../types/Address";
import { fetch, FetchResponse } from "../../utils/dataAccess";
import { useMercure } from "../../utils/mercure";

const getAddresss = async () =>
  await fetch<PagedCollection<Address>>("/addresses");

const Page: NextComponentType<NextPageContext> = () => {
  const { data: { data: addresss, hubURL } = { hubURL: null } } = useQuery<
    FetchResponse<PagedCollection<Address>> | undefined
  >("addresses", getAddresss);
  const collection = useMercure(addresss, hubURL);

  if (!collection || !collection["hydra:member"]) return null;

  return (
    <div>
      <div>
        <Head>
          <title>Address List</title>
        </Head>
      </div>
      <List addresss={collection["hydra:member"]} />
      <Pagination collection={collection} />
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = async () => {
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery("addresses", getAddresss);

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
  };
};

export default Page;
