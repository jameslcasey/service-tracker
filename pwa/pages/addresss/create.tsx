import { NextComponentType, NextPageContext } from "next";
import Head from "next/head";

import { Form } from "../../components/address/Form";

const Page: NextComponentType<NextPageContext> = () => (
  <div>
    <div>
      <Head>
        <title>Create Address</title>
      </Head>
    </div>
    <Form />
  </div>
);

export default Page;
