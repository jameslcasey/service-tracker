import { NextComponentType, NextPageContext } from "next";
import Head from "next/head";

import { Form } from "../../components/addressstatus/Form";

const Page: NextComponentType<NextPageContext> = () => (
  <div>
    <div>
      <Head>
        <title>Create AddressStatus</title>
      </Head>
    </div>
    <Form />
  </div>
);

export default Page;
