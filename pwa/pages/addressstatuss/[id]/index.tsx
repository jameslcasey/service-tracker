import {
  GetStaticPaths,
  GetStaticProps,
  NextComponentType,
  NextPageContext,
} from "next";
import DefaultErrorPage from "next/error";
import Head from "next/head";
import { useRouter } from "next/router";
import { dehydrate, QueryClient, useQuery } from "react-query";

import { Show } from "../../../components/addressstatus/Show";
import { PagedCollection } from "../../../types/collection";
import { AddressStatus } from "../../../types/AddressStatus";
import { fetch, FetchResponse, getPaths } from "../../../utils/dataAccess";
import { useMercure } from "../../../utils/mercure";

const getAddressStatus = async (id: string | string[] | undefined) =>
  id
    ? await fetch<AddressStatus>(`/address_statuses/${id}`)
    : Promise.resolve(undefined);

const Page: NextComponentType<NextPageContext> = () => {
  const router = useRouter();
  const { id } = router.query;

  const {
    data: { data: addressstatus, hubURL, text } = { hubURL: null, text: "" },
  } = useQuery<FetchResponse<AddressStatus> | undefined>(
    ["addressstatus", id],
    () => getAddressStatus(id)
  );
  const addressstatusData = useMercure(addressstatus, hubURL);

  if (!addressstatusData) {
    return <DefaultErrorPage statusCode={404} />;
  }

  return (
    <div>
      <div>
        <Head>
          <title>{`Show AddressStatus ${addressstatusData["@id"]}`}</title>
        </Head>
      </div>
      <Show addressstatus={addressstatusData} text={text} />
    </div>
  );
};

export const getStaticProps: GetStaticProps = async ({
  params: { id } = {},
}) => {
  if (!id) throw new Error("id not in query param");
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(["addressstatus", id], () =>
    getAddressStatus(id)
  );

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
    revalidate: 1,
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const response = await fetch<PagedCollection<AddressStatus>>(
    "/address_statuses"
  );
  const paths = await getPaths(
    response,
    "address_statuses",
    "/addressstatuss/[id]"
  );

  return {
    paths,
    fallback: true,
  };
};

export default Page;
