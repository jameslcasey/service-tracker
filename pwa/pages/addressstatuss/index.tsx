import { GetServerSideProps, NextComponentType, NextPageContext } from "next";
import Head from "next/head";
import { dehydrate, QueryClient, useQuery } from "react-query";

import Pagination from "../../components/common/Pagination";
import { List } from "../../components/addressstatus/List";
import { PagedCollection } from "../../types/collection";
import { AddressStatus } from "../../types/AddressStatus";
import { fetch, FetchResponse } from "../../utils/dataAccess";
import { useMercure } from "../../utils/mercure";

const getAddressStatuss = async () =>
  await fetch<PagedCollection<AddressStatus>>("/address_statuses");

const Page: NextComponentType<NextPageContext> = () => {
  const { data: { data: addressstatuss, hubURL } = { hubURL: null } } =
    useQuery<FetchResponse<PagedCollection<AddressStatus>> | undefined>(
      "address_statuses",
      getAddressStatuss
    );
  const collection = useMercure(addressstatuss, hubURL);

  if (!collection || !collection["hydra:member"]) return null;

  return (
    <div>
      <div>
        <Head>
          <title>AddressStatus List</title>
        </Head>
      </div>
      <List addressstatuss={collection["hydra:member"]} />
      <Pagination collection={collection} />
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = async () => {
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery("address_statuses", getAddressStatuss);

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
  };
};

export default Page;
