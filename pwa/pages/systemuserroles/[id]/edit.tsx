import {
  GetStaticPaths,
  GetStaticProps,
  NextComponentType,
  NextPageContext,
} from "next";
import DefaultErrorPage from "next/error";
import Head from "next/head";
import { useRouter } from "next/router";
import { dehydrate, QueryClient, useQuery } from "react-query";

import { Form } from "../../../components/systemuserrole/Form";
import { PagedCollection } from "../../../types/collection";
import { SystemUserRole } from "../../../types/SystemUserRole";
import { fetch, FetchResponse, getPaths } from "../../../utils/dataAccess";

const getSystemUserRole = async (id: string | string[] | undefined) =>
  id
    ? await fetch<SystemUserRole>(`/system_user_roles/${id}`)
    : Promise.resolve(undefined);

const Page: NextComponentType<NextPageContext> = () => {
  const router = useRouter();
  const { id } = router.query;

  const { data: { data: systemuserrole } = {} } = useQuery<
    FetchResponse<SystemUserRole> | undefined
  >(["systemuserrole", id], () => getSystemUserRole(id));

  if (!systemuserrole) {
    return <DefaultErrorPage statusCode={404} />;
  }

  return (
    <div>
      <div>
        <Head>
          <title>
            {systemuserrole && `Edit SystemUserRole ${systemuserrole["@id"]}`}
          </title>
        </Head>
      </div>
      <Form systemuserrole={systemuserrole} />
    </div>
  );
};

export const getStaticProps: GetStaticProps = async ({
  params: { id } = {},
}) => {
  if (!id) throw new Error("id not in query param");
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(["systemuserrole", id], () =>
    getSystemUserRole(id)
  );

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
    revalidate: 1,
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const response = await fetch<PagedCollection<SystemUserRole>>(
    "/system_user_roles"
  );
  const paths = await getPaths(
    response,
    "system_user_roles",
    "/systemuserroles/[id]/edit"
  );

  return {
    paths,
    fallback: true,
  };
};

export default Page;
