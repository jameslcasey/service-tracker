import {
  GetStaticPaths,
  GetStaticProps,
  NextComponentType,
  NextPageContext,
} from "next";
import DefaultErrorPage from "next/error";
import Head from "next/head";
import { useRouter } from "next/router";
import { dehydrate, QueryClient, useQuery } from "react-query";

import { Show } from "../../../components/systemuserrole/Show";
import { PagedCollection } from "../../../types/collection";
import { SystemUserRole } from "../../../types/SystemUserRole";
import { fetch, FetchResponse, getPaths } from "../../../utils/dataAccess";
import { useMercure } from "../../../utils/mercure";

const getSystemUserRole = async (id: string | string[] | undefined) =>
  id
    ? await fetch<SystemUserRole>(`/system_user_roles/${id}`)
    : Promise.resolve(undefined);

const Page: NextComponentType<NextPageContext> = () => {
  const router = useRouter();
  const { id } = router.query;

  const {
    data: { data: systemuserrole, hubURL, text } = { hubURL: null, text: "" },
  } = useQuery<FetchResponse<SystemUserRole> | undefined>(
    ["systemuserrole", id],
    () => getSystemUserRole(id)
  );
  const systemuserroleData = useMercure(systemuserrole, hubURL);

  if (!systemuserroleData) {
    return <DefaultErrorPage statusCode={404} />;
  }

  return (
    <div>
      <div>
        <Head>
          <title>{`Show SystemUserRole ${systemuserroleData["@id"]}`}</title>
        </Head>
      </div>
      <Show systemuserrole={systemuserroleData} text={text} />
    </div>
  );
};

export const getStaticProps: GetStaticProps = async ({
  params: { id } = {},
}) => {
  if (!id) throw new Error("id not in query param");
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(["systemuserrole", id], () =>
    getSystemUserRole(id)
  );

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
    revalidate: 1,
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const response = await fetch<PagedCollection<SystemUserRole>>(
    "/system_user_roles"
  );
  const paths = await getPaths(
    response,
    "system_user_roles",
    "/systemuserroles/[id]"
  );

  return {
    paths,
    fallback: true,
  };
};

export default Page;
