import { NextComponentType, NextPageContext } from "next";
import Head from "next/head";

import { Form } from "../../components/systemuserrole/Form";

const Page: NextComponentType<NextPageContext> = () => (
  <div>
    <div>
      <Head>
        <title>Create SystemUserRole</title>
      </Head>
    </div>
    <Form />
  </div>
);

export default Page;
