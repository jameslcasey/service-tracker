import { GetServerSideProps, NextComponentType, NextPageContext } from "next";
import Head from "next/head";
import { dehydrate, QueryClient, useQuery } from "react-query";

import Pagination from "../../components/common/Pagination";
import { List } from "../../components/systemuserrole/List";
import { PagedCollection } from "../../types/collection";
import { SystemUserRole } from "../../types/SystemUserRole";
import { fetch, FetchResponse } from "../../utils/dataAccess";
import { useMercure } from "../../utils/mercure";

const getSystemUserRoles = async () =>
  await fetch<PagedCollection<SystemUserRole>>("/system_user_roles");

const Page: NextComponentType<NextPageContext> = () => {
  const { data: { data: systemuserroles, hubURL } = { hubURL: null } } =
    useQuery<FetchResponse<PagedCollection<SystemUserRole>> | undefined>(
      "system_user_roles",
      getSystemUserRoles
    );
  const collection = useMercure(systemuserroles, hubURL);

  if (!collection || !collection["hydra:member"]) return null;

  return (
    <div>
      <div>
        <Head>
          <title>SystemUserRole List</title>
        </Head>
      </div>
      <List systemuserroles={collection["hydra:member"]} />
      <Pagination collection={collection} />
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = async () => {
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery("system_user_roles", getSystemUserRoles);

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
  };
};

export default Page;
