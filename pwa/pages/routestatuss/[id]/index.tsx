import {
  GetStaticPaths,
  GetStaticProps,
  NextComponentType,
  NextPageContext,
} from "next";
import DefaultErrorPage from "next/error";
import Head from "next/head";
import { useRouter } from "next/router";
import { dehydrate, QueryClient, useQuery } from "react-query";

import { Show } from "../../../components/routestatus/Show";
import { PagedCollection } from "../../../types/collection";
import { RouteStatus } from "../../../types/RouteStatus";
import { fetch, FetchResponse, getPaths } from "../../../utils/dataAccess";
import { useMercure } from "../../../utils/mercure";

const getRouteStatus = async (id: string | string[] | undefined) =>
  id
    ? await fetch<RouteStatus>(`/route_statuses/${id}`)
    : Promise.resolve(undefined);

const Page: NextComponentType<NextPageContext> = () => {
  const router = useRouter();
  const { id } = router.query;

  const {
    data: { data: routestatus, hubURL, text } = { hubURL: null, text: "" },
  } = useQuery<FetchResponse<RouteStatus> | undefined>(
    ["routestatus", id],
    () => getRouteStatus(id)
  );
  const routestatusData = useMercure(routestatus, hubURL);

  if (!routestatusData) {
    return <DefaultErrorPage statusCode={404} />;
  }

  return (
    <div>
      <div>
        <Head>
          <title>{`Show RouteStatus ${routestatusData["@id"]}`}</title>
        </Head>
      </div>
      <Show routestatus={routestatusData} text={text} />
    </div>
  );
};

export const getStaticProps: GetStaticProps = async ({
  params: { id } = {},
}) => {
  if (!id) throw new Error("id not in query param");
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(["routestatus", id], () =>
    getRouteStatus(id)
  );

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
    revalidate: 1,
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const response = await fetch<PagedCollection<RouteStatus>>("/route_statuses");
  const paths = await getPaths(
    response,
    "route_statuses",
    "/routestatuss/[id]"
  );

  return {
    paths,
    fallback: true,
  };
};

export default Page;
