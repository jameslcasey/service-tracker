import { GetServerSideProps, NextComponentType, NextPageContext } from "next";
import Head from "next/head";
import { dehydrate, QueryClient, useQuery } from "react-query";

import Pagination from "../../components/common/Pagination";
import { List } from "../../components/routestatus/List";
import { PagedCollection } from "../../types/collection";
import { RouteStatus } from "../../types/RouteStatus";
import { fetch, FetchResponse } from "../../utils/dataAccess";
import { useMercure } from "../../utils/mercure";

const getRouteStatuss = async () =>
  await fetch<PagedCollection<RouteStatus>>("/route_statuses");

const Page: NextComponentType<NextPageContext> = () => {
  const { data: { data: routestatuss, hubURL } = { hubURL: null } } = useQuery<
    FetchResponse<PagedCollection<RouteStatus>> | undefined
  >("route_statuses", getRouteStatuss);
  const collection = useMercure(routestatuss, hubURL);

  if (!collection || !collection["hydra:member"]) return null;

  return (
    <div>
      <div>
        <Head>
          <title>RouteStatus List</title>
        </Head>
      </div>
      <List routestatuss={collection["hydra:member"]} />
      <Pagination collection={collection} />
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = async () => {
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery("route_statuses", getRouteStatuss);

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
  };
};

export default Page;
