import { GetServerSideProps, NextComponentType, NextPageContext } from "next";
import Head from "next/head";
import { dehydrate, QueryClient, useQuery } from "react-query";

import Pagination from "../../components/common/Pagination";
import { List } from "../../components/route/List";
import { PagedCollection } from "../../types/collection";
import { Route } from "../../types/Route";
import { fetch, FetchResponse } from "../../utils/dataAccess";
import { useMercure } from "../../utils/mercure";

const getRoutes = async () => await fetch<PagedCollection<Route>>("/routes");

const Page: NextComponentType<NextPageContext> = () => {
  const { data: { data: routes, hubURL } = { hubURL: null } } = useQuery<
    FetchResponse<PagedCollection<Route>> | undefined
  >("routes", getRoutes);
  const collection = useMercure(routes, hubURL);

  if (!collection || !collection["hydra:member"]) return null;

  return (
    <div>
      <div>
        <Head>
          <title>Route List</title>
        </Head>
      </div>
      <List routes={collection["hydra:member"]} />
      <Pagination collection={collection} />
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = async () => {
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery("routes", getRoutes);

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
  };
};

export default Page;
