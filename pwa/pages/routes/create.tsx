import { NextComponentType, NextPageContext } from "next";
import Head from "next/head";

import { Form } from "../../components/route/Form";

const Page: NextComponentType<NextPageContext> = () => (
  <div>
    <div>
      <Head>
        <title>Create Route</title>
      </Head>
    </div>
    <Form />
  </div>
);

export default Page;
