import {
  GetStaticPaths,
  GetStaticProps,
  NextComponentType,
  NextPageContext,
} from "next";
import DefaultErrorPage from "next/error";
import Head from "next/head";
import { useRouter } from "next/router";
import { dehydrate, QueryClient, useQuery } from "react-query";

import { Form } from "../../../components/route/Form";
import { PagedCollection } from "../../../types/collection";
import { Route } from "../../../types/Route";
import { fetch, FetchResponse, getPaths } from "../../../utils/dataAccess";

const getRoute = async (id: string | string[] | undefined) =>
  id ? await fetch<Route>(`/routes/${id}`) : Promise.resolve(undefined);

const Page: NextComponentType<NextPageContext> = () => {
  const router = useRouter();
  const { id } = router.query;

  const { data: { data: route } = {} } = useQuery<
    FetchResponse<Route> | undefined
  >(["route", id], () => getRoute(id));

  if (!route) {
    return <DefaultErrorPage statusCode={404} />;
  }

  return (
    <div>
      <div>
        <Head>
          <title>{route && `Edit Route ${route["@id"]}`}</title>
        </Head>
      </div>
      <Form route={route} />
    </div>
  );
};

export const getStaticProps: GetStaticProps = async ({
  params: { id } = {},
}) => {
  if (!id) throw new Error("id not in query param");
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(["route", id], () => getRoute(id));

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
    revalidate: 1,
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const response = await fetch<PagedCollection<Route>>("/routes");
  const paths = await getPaths(response, "routes", "/routes/[id]/edit");

  return {
    paths,
    fallback: true,
  };
};

export default Page;
