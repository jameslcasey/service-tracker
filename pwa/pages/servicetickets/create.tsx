import { NextComponentType, NextPageContext } from "next";
import Head from "next/head";

import { Form } from "../../components/serviceticket/Form";

const Page: NextComponentType<NextPageContext> = () => (
  <div>
    <div>
      <Head>
        <title>Create ServiceTicket</title>
      </Head>
    </div>
    <Form />
  </div>
);

export default Page;
