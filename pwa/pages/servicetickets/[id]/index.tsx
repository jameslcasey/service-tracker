import {
  GetStaticPaths,
  GetStaticProps,
  NextComponentType,
  NextPageContext,
} from "next";
import DefaultErrorPage from "next/error";
import Head from "next/head";
import { useRouter } from "next/router";
import { dehydrate, QueryClient, useQuery } from "react-query";

import { Show } from "../../../components/serviceticket/Show";
import { PagedCollection } from "../../../types/collection";
import { ServiceTicket } from "../../../types/ServiceTicket";
import { fetch, FetchResponse, getPaths } from "../../../utils/dataAccess";
import { useMercure } from "../../../utils/mercure";

const getServiceTicket = async (id: string | string[] | undefined) =>
  id
    ? await fetch<ServiceTicket>(`/service_tickets/${id}`)
    : Promise.resolve(undefined);

const Page: NextComponentType<NextPageContext> = () => {
  const router = useRouter();
  const { id } = router.query;

  const {
    data: { data: serviceticket, hubURL, text } = { hubURL: null, text: "" },
  } = useQuery<FetchResponse<ServiceTicket> | undefined>(
    ["serviceticket", id],
    () => getServiceTicket(id)
  );
  const serviceticketData = useMercure(serviceticket, hubURL);

  if (!serviceticketData) {
    return <DefaultErrorPage statusCode={404} />;
  }

  return (
    <div>
      <div>
        <Head>
          <title>{`Show ServiceTicket ${serviceticketData["@id"]}`}</title>
        </Head>
      </div>
      <Show serviceticket={serviceticketData} text={text} />
    </div>
  );
};

export const getStaticProps: GetStaticProps = async ({
  params: { id } = {},
}) => {
  if (!id) throw new Error("id not in query param");
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(["serviceticket", id], () =>
    getServiceTicket(id)
  );

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
    revalidate: 1,
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const response = await fetch<PagedCollection<ServiceTicket>>(
    "/service_tickets"
  );
  const paths = await getPaths(
    response,
    "service_tickets",
    "/servicetickets/[id]"
  );

  return {
    paths,
    fallback: true,
  };
};

export default Page;
