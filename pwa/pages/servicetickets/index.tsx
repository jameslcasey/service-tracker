import { GetServerSideProps, NextComponentType, NextPageContext } from "next";
import Head from "next/head";
import { dehydrate, QueryClient, useQuery } from "react-query";

import Pagination from "../../components/common/Pagination";
import { List } from "../../components/serviceticket/List";
import { PagedCollection } from "../../types/collection";
import { ServiceTicket } from "../../types/ServiceTicket";
import { fetch, FetchResponse } from "../../utils/dataAccess";
import { useMercure } from "../../utils/mercure";

const getServiceTickets = async () =>
  await fetch<PagedCollection<ServiceTicket>>("/service_tickets");

const Page: NextComponentType<NextPageContext> = () => {
  const { data: { data: servicetickets, hubURL } = { hubURL: null } } =
    useQuery<FetchResponse<PagedCollection<ServiceTicket>> | undefined>(
      "service_tickets",
      getServiceTickets
    );
  const collection = useMercure(servicetickets, hubURL);

  if (!collection || !collection["hydra:member"]) return null;

  return (
    <div>
      <div>
        <Head>
          <title>ServiceTicket List</title>
        </Head>
      </div>
      <List servicetickets={collection["hydra:member"]} />
      <Pagination collection={collection} />
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = async () => {
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery("service_tickets", getServiceTickets);

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
  };
};

export default Page;
