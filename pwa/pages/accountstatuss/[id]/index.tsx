import {
  GetStaticPaths,
  GetStaticProps,
  NextComponentType,
  NextPageContext,
} from "next";
import DefaultErrorPage from "next/error";
import Head from "next/head";
import { useRouter } from "next/router";
import { dehydrate, QueryClient, useQuery } from "react-query";

import { Show } from "../../../components/accountstatus/Show";
import { PagedCollection } from "../../../types/collection";
import { AccountStatus } from "../../../types/AccountStatus";
import { fetch, FetchResponse, getPaths } from "../../../utils/dataAccess";
import { useMercure } from "../../../utils/mercure";

const getAccountStatus = async (id: string | string[] | undefined) =>
  id
    ? await fetch<AccountStatus>(`/account_statuses/${id}`)
    : Promise.resolve(undefined);

const Page: NextComponentType<NextPageContext> = () => {
  const router = useRouter();
  const { id } = router.query;

  const {
    data: { data: accountstatus, hubURL, text } = { hubURL: null, text: "" },
  } = useQuery<FetchResponse<AccountStatus> | undefined>(
    ["accountstatus", id],
    () => getAccountStatus(id)
  );
  const accountstatusData = useMercure(accountstatus, hubURL);

  if (!accountstatusData) {
    return <DefaultErrorPage statusCode={404} />;
  }

  return (
    <div>
      <div>
        <Head>
          <title>{`Show AccountStatus ${accountstatusData["@id"]}`}</title>
        </Head>
      </div>
      <Show accountstatus={accountstatusData} text={text} />
    </div>
  );
};

export const getStaticProps: GetStaticProps = async ({
  params: { id } = {},
}) => {
  if (!id) throw new Error("id not in query param");
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(["accountstatus", id], () =>
    getAccountStatus(id)
  );

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
    revalidate: 1,
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const response = await fetch<PagedCollection<AccountStatus>>(
    "/account_statuses"
  );
  const paths = await getPaths(
    response,
    "account_statuses",
    "/accountstatuss/[id]"
  );

  return {
    paths,
    fallback: true,
  };
};

export default Page;
