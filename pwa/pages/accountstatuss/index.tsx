import { GetServerSideProps, NextComponentType, NextPageContext } from "next";
import Head from "next/head";
import { dehydrate, QueryClient, useQuery } from "react-query";

import Pagination from "../../components/common/Pagination";
import { List } from "../../components/accountstatus/List";
import { PagedCollection } from "../../types/collection";
import { AccountStatus } from "../../types/AccountStatus";
import { fetch, FetchResponse } from "../../utils/dataAccess";
import { useMercure } from "../../utils/mercure";

const getAccountStatuss = async () =>
  await fetch<PagedCollection<AccountStatus>>("/account_statuses");

const Page: NextComponentType<NextPageContext> = () => {
  const { data: { data: accountstatuss, hubURL } = { hubURL: null } } =
    useQuery<FetchResponse<PagedCollection<AccountStatus>> | undefined>(
      "account_statuses",
      getAccountStatuss
    );
  const collection = useMercure(accountstatuss, hubURL);

  if (!collection || !collection["hydra:member"]) return null;

  return (
    <div>
      <div>
        <Head>
          <title>AccountStatus List</title>
        </Head>
      </div>
      <List accountstatuss={collection["hydra:member"]} />
      <Pagination collection={collection} />
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = async () => {
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery("account_statuses", getAccountStatuss);

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
  };
};

export default Page;
