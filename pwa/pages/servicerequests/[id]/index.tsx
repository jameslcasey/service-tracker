import {
  GetStaticPaths,
  GetStaticProps,
  NextComponentType,
  NextPageContext,
} from "next";
import DefaultErrorPage from "next/error";
import Head from "next/head";
import { useRouter } from "next/router";
import { dehydrate, QueryClient, useQuery } from "react-query";

import { Show } from "../../../components/servicerequest/Show";
import { PagedCollection } from "../../../types/collection";
import { ServiceRequest } from "../../../types/ServiceRequest";
import { fetch, FetchResponse, getPaths } from "../../../utils/dataAccess";
import { useMercure } from "../../../utils/mercure";

const getServiceRequest = async (id: string | string[] | undefined) =>
  id
    ? await fetch<ServiceRequest>(`/service_requests/${id}`)
    : Promise.resolve(undefined);

const Page: NextComponentType<NextPageContext> = () => {
  const router = useRouter();
  const { id } = router.query;

  const {
    data: { data: servicerequest, hubURL, text } = { hubURL: null, text: "" },
  } = useQuery<FetchResponse<ServiceRequest> | undefined>(
    ["servicerequest", id],
    () => getServiceRequest(id)
  );
  const servicerequestData = useMercure(servicerequest, hubURL);

  if (!servicerequestData) {
    return <DefaultErrorPage statusCode={404} />;
  }

  return (
    <div>
      <div>
        <Head>
          <title>{`Show ServiceRequest ${servicerequestData["@id"]}`}</title>
        </Head>
      </div>
      <Show servicerequest={servicerequestData} text={text} />
    </div>
  );
};

export const getStaticProps: GetStaticProps = async ({
  params: { id } = {},
}) => {
  if (!id) throw new Error("id not in query param");
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(["servicerequest", id], () =>
    getServiceRequest(id)
  );

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
    revalidate: 1,
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const response = await fetch<PagedCollection<ServiceRequest>>(
    "/service_requests"
  );
  const paths = await getPaths(
    response,
    "service_requests",
    "/servicerequests/[id]"
  );

  return {
    paths,
    fallback: true,
  };
};

export default Page;
