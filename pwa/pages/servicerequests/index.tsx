import { GetServerSideProps, NextComponentType, NextPageContext } from "next";
import Head from "next/head";
import { dehydrate, QueryClient, useQuery } from "react-query";

import Pagination from "../../components/common/Pagination";
import { List } from "../../components/servicerequest/List";
import { PagedCollection } from "../../types/collection";
import { ServiceRequest } from "../../types/ServiceRequest";
import { fetch, FetchResponse } from "../../utils/dataAccess";
import { useMercure } from "../../utils/mercure";

const getServiceRequests = async () =>
  await fetch<PagedCollection<ServiceRequest>>("/service_requests");

const Page: NextComponentType<NextPageContext> = () => {
  const { data: { data: servicerequests, hubURL } = { hubURL: null } } =
    useQuery<FetchResponse<PagedCollection<ServiceRequest>> | undefined>(
      "service_requests",
      getServiceRequests
    );
  const collection = useMercure(servicerequests, hubURL);

  if (!collection || !collection["hydra:member"]) return null;

  return (
    <div>
      <div>
        <Head>
          <title>ServiceRequest List</title>
        </Head>
      </div>
      <List servicerequests={collection["hydra:member"]} />
      <Pagination collection={collection} />
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = async () => {
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery("service_requests", getServiceRequests);

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
  };
};

export default Page;
