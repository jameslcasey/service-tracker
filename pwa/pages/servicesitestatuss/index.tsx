import { GetServerSideProps, NextComponentType, NextPageContext } from "next";
import Head from "next/head";
import { dehydrate, QueryClient, useQuery } from "react-query";

import Pagination from "../../components/common/Pagination";
import { List } from "../../components/servicesitestatus/List";
import { PagedCollection } from "../../types/collection";
import { ServiceSiteStatus } from "../../types/ServiceSiteStatus";
import { fetch, FetchResponse } from "../../utils/dataAccess";
import { useMercure } from "../../utils/mercure";

const getServiceSiteStatuss = async () =>
  await fetch<PagedCollection<ServiceSiteStatus>>("/service_site_statuses");

const Page: NextComponentType<NextPageContext> = () => {
  const { data: { data: servicesitestatuss, hubURL } = { hubURL: null } } =
    useQuery<FetchResponse<PagedCollection<ServiceSiteStatus>> | undefined>(
      "service_site_statuses",
      getServiceSiteStatuss
    );
  const collection = useMercure(servicesitestatuss, hubURL);

  if (!collection || !collection["hydra:member"]) return null;

  return (
    <div>
      <div>
        <Head>
          <title>ServiceSiteStatus List</title>
        </Head>
      </div>
      <List servicesitestatuss={collection["hydra:member"]} />
      <Pagination collection={collection} />
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = async () => {
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(
    "service_site_statuses",
    getServiceSiteStatuss
  );

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
  };
};

export default Page;
