import { NextComponentType, NextPageContext } from "next";
import Head from "next/head";

import { Form } from "../../components/servicesitestatus/Form";

const Page: NextComponentType<NextPageContext> = () => (
  <div>
    <div>
      <Head>
        <title>Create ServiceSiteStatus</title>
      </Head>
    </div>
    <Form />
  </div>
);

export default Page;
