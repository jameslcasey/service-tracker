import {
  GetStaticPaths,
  GetStaticProps,
  NextComponentType,
  NextPageContext,
} from "next";
import DefaultErrorPage from "next/error";
import Head from "next/head";
import { useRouter } from "next/router";
import { dehydrate, QueryClient, useQuery } from "react-query";

import { Form } from "../../../components/servicesitestatus/Form";
import { PagedCollection } from "../../../types/collection";
import { ServiceSiteStatus } from "../../../types/ServiceSiteStatus";
import { fetch, FetchResponse, getPaths } from "../../../utils/dataAccess";

const getServiceSiteStatus = async (id: string | string[] | undefined) =>
  id
    ? await fetch<ServiceSiteStatus>(`/service_site_statuses/${id}`)
    : Promise.resolve(undefined);

const Page: NextComponentType<NextPageContext> = () => {
  const router = useRouter();
  const { id } = router.query;

  const { data: { data: servicesitestatus } = {} } = useQuery<
    FetchResponse<ServiceSiteStatus> | undefined
  >(["servicesitestatus", id], () => getServiceSiteStatus(id));

  if (!servicesitestatus) {
    return <DefaultErrorPage statusCode={404} />;
  }

  return (
    <div>
      <div>
        <Head>
          <title>
            {servicesitestatus &&
              `Edit ServiceSiteStatus ${servicesitestatus["@id"]}`}
          </title>
        </Head>
      </div>
      <Form servicesitestatus={servicesitestatus} />
    </div>
  );
};

export const getStaticProps: GetStaticProps = async ({
  params: { id } = {},
}) => {
  if (!id) throw new Error("id not in query param");
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(["servicesitestatus", id], () =>
    getServiceSiteStatus(id)
  );

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
    revalidate: 1,
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const response = await fetch<PagedCollection<ServiceSiteStatus>>(
    "/service_site_statuses"
  );
  const paths = await getPaths(
    response,
    "service_site_statuses",
    "/servicesitestatuss/[id]/edit"
  );

  return {
    paths,
    fallback: true,
  };
};

export default Page;
