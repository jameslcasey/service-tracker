import {
  GetStaticPaths,
  GetStaticProps,
  NextComponentType,
  NextPageContext,
} from "next";
import DefaultErrorPage from "next/error";
import Head from "next/head";
import { useRouter } from "next/router";
import { dehydrate, QueryClient, useQuery } from "react-query";

import { Show } from "../../../components/servicesitestatus/Show";
import { PagedCollection } from "../../../types/collection";
import { ServiceSiteStatus } from "../../../types/ServiceSiteStatus";
import { fetch, FetchResponse, getPaths } from "../../../utils/dataAccess";
import { useMercure } from "../../../utils/mercure";

const getServiceSiteStatus = async (id: string | string[] | undefined) =>
  id
    ? await fetch<ServiceSiteStatus>(`/service_site_statuses/${id}`)
    : Promise.resolve(undefined);

const Page: NextComponentType<NextPageContext> = () => {
  const router = useRouter();
  const { id } = router.query;

  const {
    data: { data: servicesitestatus, hubURL, text } = {
      hubURL: null,
      text: "",
    },
  } = useQuery<FetchResponse<ServiceSiteStatus> | undefined>(
    ["servicesitestatus", id],
    () => getServiceSiteStatus(id)
  );
  const servicesitestatusData = useMercure(servicesitestatus, hubURL);

  if (!servicesitestatusData) {
    return <DefaultErrorPage statusCode={404} />;
  }

  return (
    <div>
      <div>
        <Head>
          <title>{`Show ServiceSiteStatus ${servicesitestatusData["@id"]}`}</title>
        </Head>
      </div>
      <Show servicesitestatus={servicesitestatusData} text={text} />
    </div>
  );
};

export const getStaticProps: GetStaticProps = async ({
  params: { id } = {},
}) => {
  if (!id) throw new Error("id not in query param");
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(["servicesitestatus", id], () =>
    getServiceSiteStatus(id)
  );

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
    revalidate: 1,
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const response = await fetch<PagedCollection<ServiceSiteStatus>>(
    "/service_site_statuses"
  );
  const paths = await getPaths(
    response,
    "service_site_statuses",
    "/servicesitestatuss/[id]"
  );

  return {
    paths,
    fallback: true,
  };
};

export default Page;
