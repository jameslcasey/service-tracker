import {
  GetStaticPaths,
  GetStaticProps,
  NextComponentType,
  NextPageContext,
} from "next";
import DefaultErrorPage from "next/error";
import Head from "next/head";
import { useRouter } from "next/router";
import { dehydrate, QueryClient, useQuery } from "react-query";

import { Form } from "../../../components/personstatus/Form";
import { PagedCollection } from "../../../types/collection";
import { PersonStatus } from "../../../types/PersonStatus";
import { fetch, FetchResponse, getPaths } from "../../../utils/dataAccess";

const getPersonStatus = async (id: string | string[] | undefined) =>
  id
    ? await fetch<PersonStatus>(`/person_statuses/${id}`)
    : Promise.resolve(undefined);

const Page: NextComponentType<NextPageContext> = () => {
  const router = useRouter();
  const { id } = router.query;

  const { data: { data: personstatus } = {} } = useQuery<
    FetchResponse<PersonStatus> | undefined
  >(["personstatus", id], () => getPersonStatus(id));

  if (!personstatus) {
    return <DefaultErrorPage statusCode={404} />;
  }

  return (
    <div>
      <div>
        <Head>
          <title>
            {personstatus && `Edit PersonStatus ${personstatus["@id"]}`}
          </title>
        </Head>
      </div>
      <Form personstatus={personstatus} />
    </div>
  );
};

export const getStaticProps: GetStaticProps = async ({
  params: { id } = {},
}) => {
  if (!id) throw new Error("id not in query param");
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(["personstatus", id], () =>
    getPersonStatus(id)
  );

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
    revalidate: 1,
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const response = await fetch<PagedCollection<PersonStatus>>(
    "/person_statuses"
  );
  const paths = await getPaths(
    response,
    "person_statuses",
    "/personstatuss/[id]/edit"
  );

  return {
    paths,
    fallback: true,
  };
};

export default Page;
