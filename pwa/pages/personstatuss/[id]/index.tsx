import {
  GetStaticPaths,
  GetStaticProps,
  NextComponentType,
  NextPageContext,
} from "next";
import DefaultErrorPage from "next/error";
import Head from "next/head";
import { useRouter } from "next/router";
import { dehydrate, QueryClient, useQuery } from "react-query";

import { Show } from "../../../components/personstatus/Show";
import { PagedCollection } from "../../../types/collection";
import { PersonStatus } from "../../../types/PersonStatus";
import { fetch, FetchResponse, getPaths } from "../../../utils/dataAccess";
import { useMercure } from "../../../utils/mercure";

const getPersonStatus = async (id: string | string[] | undefined) =>
  id
    ? await fetch<PersonStatus>(`/person_statuses/${id}`)
    : Promise.resolve(undefined);

const Page: NextComponentType<NextPageContext> = () => {
  const router = useRouter();
  const { id } = router.query;

  const {
    data: { data: personstatus, hubURL, text } = { hubURL: null, text: "" },
  } = useQuery<FetchResponse<PersonStatus> | undefined>(
    ["personstatus", id],
    () => getPersonStatus(id)
  );
  const personstatusData = useMercure(personstatus, hubURL);

  if (!personstatusData) {
    return <DefaultErrorPage statusCode={404} />;
  }

  return (
    <div>
      <div>
        <Head>
          <title>{`Show PersonStatus ${personstatusData["@id"]}`}</title>
        </Head>
      </div>
      <Show personstatus={personstatusData} text={text} />
    </div>
  );
};

export const getStaticProps: GetStaticProps = async ({
  params: { id } = {},
}) => {
  if (!id) throw new Error("id not in query param");
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(["personstatus", id], () =>
    getPersonStatus(id)
  );

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
    revalidate: 1,
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const response = await fetch<PagedCollection<PersonStatus>>(
    "/person_statuses"
  );
  const paths = await getPaths(
    response,
    "person_statuses",
    "/personstatuss/[id]"
  );

  return {
    paths,
    fallback: true,
  };
};

export default Page;
