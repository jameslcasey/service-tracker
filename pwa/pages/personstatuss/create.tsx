import { NextComponentType, NextPageContext } from "next";
import Head from "next/head";

import { Form } from "../../components/personstatus/Form";

const Page: NextComponentType<NextPageContext> = () => (
  <div>
    <div>
      <Head>
        <title>Create PersonStatus</title>
      </Head>
    </div>
    <Form />
  </div>
);

export default Page;
