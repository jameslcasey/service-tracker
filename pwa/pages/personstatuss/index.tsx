import { GetServerSideProps, NextComponentType, NextPageContext } from "next";
import Head from "next/head";
import { dehydrate, QueryClient, useQuery } from "react-query";

import Pagination from "../../components/common/Pagination";
import { List } from "../../components/personstatus/List";
import { PagedCollection } from "../../types/collection";
import { PersonStatus } from "../../types/PersonStatus";
import { fetch, FetchResponse } from "../../utils/dataAccess";
import { useMercure } from "../../utils/mercure";

const getPersonStatuss = async () =>
  await fetch<PagedCollection<PersonStatus>>("/person_statuses");

const Page: NextComponentType<NextPageContext> = () => {
  const { data: { data: personstatuss, hubURL } = { hubURL: null } } = useQuery<
    FetchResponse<PagedCollection<PersonStatus>> | undefined
  >("person_statuses", getPersonStatuss);
  const collection = useMercure(personstatuss, hubURL);

  if (!collection || !collection["hydra:member"]) return null;

  return (
    <div>
      <div>
        <Head>
          <title>PersonStatus List</title>
        </Head>
      </div>
      <List personstatuss={collection["hydra:member"]} />
      <Pagination collection={collection} />
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = async () => {
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery("person_statuses", getPersonStatuss);

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
  };
};

export default Page;
