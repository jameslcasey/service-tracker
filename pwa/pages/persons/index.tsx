import { GetServerSideProps, NextComponentType, NextPageContext } from "next";
import Head from "next/head";
import { dehydrate, QueryClient, useQuery } from "react-query";

import Pagination from "../../components/common/Pagination";
import { List } from "../../components/person/List";
import { PagedCollection } from "../../types/collection";
import { Person } from "../../types/Person";
import { fetch, FetchResponse } from "../../utils/dataAccess";
import { useMercure } from "../../utils/mercure";

const getPersons = async () => await fetch<PagedCollection<Person>>("/people");

const Page: NextComponentType<NextPageContext> = () => {
  const { data: { data: persons, hubURL } = { hubURL: null } } = useQuery<
    FetchResponse<PagedCollection<Person>> | undefined
  >("people", getPersons);
  const collection = useMercure(persons, hubURL);

  if (!collection || !collection["hydra:member"]) return null;

  return (
    <div>
      <div>
        <Head>
          <title>Person List</title>
        </Head>
      </div>
      <List persons={collection["hydra:member"]} />
      <Pagination collection={collection} />
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = async () => {
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery("people", getPersons);

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
  };
};

export default Page;
