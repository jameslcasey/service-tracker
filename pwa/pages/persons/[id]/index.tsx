import {
  GetStaticPaths,
  GetStaticProps,
  NextComponentType,
  NextPageContext,
} from "next";
import DefaultErrorPage from "next/error";
import Head from "next/head";
import { useRouter } from "next/router";
import { dehydrate, QueryClient, useQuery } from "react-query";

import { Show } from "../../../components/person/Show";
import { PagedCollection } from "../../../types/collection";
import { Person } from "../../../types/Person";
import { fetch, FetchResponse, getPaths } from "../../../utils/dataAccess";
import { useMercure } from "../../../utils/mercure";

const getPerson = async (id: string | string[] | undefined) =>
  id ? await fetch<Person>(`/people/${id}`) : Promise.resolve(undefined);

const Page: NextComponentType<NextPageContext> = () => {
  const router = useRouter();
  const { id } = router.query;

  const { data: { data: person, hubURL, text } = { hubURL: null, text: "" } } =
    useQuery<FetchResponse<Person> | undefined>(["person", id], () =>
      getPerson(id)
    );
  const personData = useMercure(person, hubURL);

  if (!personData) {
    return <DefaultErrorPage statusCode={404} />;
  }

  return (
    <div>
      <div>
        <Head>
          <title>{`Show Person ${personData["@id"]}`}</title>
        </Head>
      </div>
      <Show person={personData} text={text} />
    </div>
  );
};

export const getStaticProps: GetStaticProps = async ({
  params: { id } = {},
}) => {
  if (!id) throw new Error("id not in query param");
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(["person", id], () => getPerson(id));

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
    revalidate: 1,
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const response = await fetch<PagedCollection<Person>>("/people");
  const paths = await getPaths(response, "people", "/persons/[id]");

  return {
    paths,
    fallback: true,
  };
};

export default Page;
