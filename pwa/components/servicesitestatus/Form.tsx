import { FunctionComponent, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { ErrorMessage, Formik } from "formik";
import { useMutation } from "react-query";

import { fetch, FetchError, FetchResponse } from "../../utils/dataAccess";
import { ServiceSiteStatus } from "../../types/ServiceSiteStatus";

interface Props {
  servicesitestatus?: ServiceSiteStatus;
}

interface SaveParams {
  values: ServiceSiteStatus;
}

interface DeleteParams {
  id: string;
}

const saveServiceSiteStatus = async ({ values }: SaveParams) =>
  await fetch<ServiceSiteStatus>(
    !values["@id"] ? "/service_site_statuses" : values["@id"],
    {
      method: !values["@id"] ? "POST" : "PUT",
      body: JSON.stringify(values),
    }
  );

const deleteServiceSiteStatus = async (id: string) =>
  await fetch<ServiceSiteStatus>(id, { method: "DELETE" });

export const Form: FunctionComponent<Props> = ({ servicesitestatus }) => {
  const [, setError] = useState<string | null>(null);
  const router = useRouter();

  const saveMutation = useMutation<
    FetchResponse<ServiceSiteStatus> | undefined,
    Error | FetchError,
    SaveParams
  >((saveParams) => saveServiceSiteStatus(saveParams));

  const deleteMutation = useMutation<
    FetchResponse<ServiceSiteStatus> | undefined,
    Error | FetchError,
    DeleteParams
  >(({ id }) => deleteServiceSiteStatus(id), {
    onSuccess: () => {
      router.push("/servicesitestatuss");
    },
    onError: (error) => {
      setError(`Error when deleting the resource: ${error}`);
      console.error(error);
    },
  });

  const handleDelete = () => {
    if (!servicesitestatus || !servicesitestatus["@id"]) return;
    if (!window.confirm("Are you sure you want to delete this item?")) return;
    deleteMutation.mutate({ id: servicesitestatus["@id"] });
  };

  return (
    <div>
      <h1>
        {servicesitestatus
          ? `Edit ServiceSiteStatus ${servicesitestatus["@id"]}`
          : `Create ServiceSiteStatus`}
      </h1>
      <Formik
        initialValues={
          servicesitestatus
            ? {
                ...servicesitestatus,
              }
            : new ServiceSiteStatus()
        }
        validate={() => {
          const errors = {};
          // add your validation logic here
          return errors;
        }}
        onSubmit={(values, { setSubmitting, setStatus, setErrors }) => {
          const isCreation = !values["@id"];
          saveMutation.mutate(
            { values },
            {
              onSuccess: () => {
                setStatus({
                  isValid: true,
                  msg: `Element ${isCreation ? "created" : "updated"}.`,
                });
                router.push("/service_site_statuses");
              },
              onError: (error) => {
                setStatus({
                  isValid: false,
                  msg: `${error.message}`,
                });
                if ("fields" in error) {
                  setErrors(error.fields);
                }
              },
              onSettled: () => {
                setSubmitting(false);
              },
            }
          );
        }}
      >
        {({
          values,
          status,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
          <form onSubmit={handleSubmit}>
            {status && status.msg && (
              <div
                className={`alert ${
                  status.isValid ? "alert-success" : "alert-danger"
                }`}
                role="alert"
              >
                {status.msg}
              </div>
            )}
            <button
              type="submit"
              className="btn btn-success"
              disabled={isSubmitting}
            >
              Submit
            </button>
          </form>
        )}
      </Formik>
      <Link href="/servicesitestatuss">
        <a className="btn btn-primary">Back to list</a>
      </Link>
      {servicesitestatus && (
        <button className="btn btn-danger" onClick={handleDelete}>
          <a>Delete</a>
        </button>
      )}
    </div>
  );
};
