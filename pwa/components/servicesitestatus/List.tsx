import { FunctionComponent } from "react";
import Link from "next/link";

import ReferenceLinks from "../common/ReferenceLinks";
import { getPath } from "../../utils/dataAccess";
import { ServiceSiteStatus } from "../../types/ServiceSiteStatus";

interface Props {
  servicesitestatuss: ServiceSiteStatus[];
}

export const List: FunctionComponent<Props> = ({ servicesitestatuss }) => (
  <div>
    <h1>ServiceSiteStatus List</h1>
    <Link href="/servicesitestatuss/create">
      <a className="btn btn-primary">Create</a>
    </Link>
    <table className="table table-responsive table-striped table-hover">
      <thead>
        <tr>
          <th>id</th>
          <th />
        </tr>
      </thead>
      <tbody>
        {servicesitestatuss &&
          servicesitestatuss.length !== 0 &&
          servicesitestatuss.map(
            (servicesitestatus) =>
              servicesitestatus["@id"] && (
                <tr key={servicesitestatus["@id"]}>
                  <th scope="row">
                    <ReferenceLinks
                      items={{
                        href: getPath(
                          servicesitestatus["@id"],
                          "/servicesitestatuss/[id]"
                        ),
                        name: servicesitestatus["@id"],
                      }}
                    />
                  </th>
                  <td>
                    <Link
                      href={getPath(
                        servicesitestatus["@id"],
                        "/servicesitestatuss/[id]"
                      )}
                    >
                      <a>
                        <i className="bi bi-search" aria-hidden="true"></i>
                        <span className="sr-only">Show</span>
                      </a>
                    </Link>
                  </td>
                  <td>
                    <Link
                      href={getPath(
                        servicesitestatus["@id"],
                        "/servicesitestatuss/[id]/edit"
                      )}
                    >
                      <a>
                        <i className="bi bi-pen" aria-hidden="true" />
                        <span className="sr-only">Edit</span>
                      </a>
                    </Link>
                  </td>
                </tr>
              )
          )}
      </tbody>
    </table>
  </div>
);
