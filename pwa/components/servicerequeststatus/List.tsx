import { FunctionComponent } from "react";
import Link from "next/link";

import ReferenceLinks from "../common/ReferenceLinks";
import { getPath } from "../../utils/dataAccess";
import { ServiceRequestStatus } from "../../types/ServiceRequestStatus";

interface Props {
  servicerequeststatuss: ServiceRequestStatus[];
}

export const List: FunctionComponent<Props> = ({ servicerequeststatuss }) => (
  <div>
    <h1>ServiceRequestStatus List</h1>
    <Link href="/servicerequeststatuss/create">
      <a className="btn btn-primary">Create</a>
    </Link>
    <table className="table table-responsive table-striped table-hover">
      <thead>
        <tr>
          <th>id</th>
          <th />
        </tr>
      </thead>
      <tbody>
        {servicerequeststatuss &&
          servicerequeststatuss.length !== 0 &&
          servicerequeststatuss.map(
            (servicerequeststatus) =>
              servicerequeststatus["@id"] && (
                <tr key={servicerequeststatus["@id"]}>
                  <th scope="row">
                    <ReferenceLinks
                      items={{
                        href: getPath(
                          servicerequeststatus["@id"],
                          "/servicerequeststatuss/[id]"
                        ),
                        name: servicerequeststatus["@id"],
                      }}
                    />
                  </th>
                  <td>
                    <Link
                      href={getPath(
                        servicerequeststatus["@id"],
                        "/servicerequeststatuss/[id]"
                      )}
                    >
                      <a>
                        <i className="bi bi-search" aria-hidden="true"></i>
                        <span className="sr-only">Show</span>
                      </a>
                    </Link>
                  </td>
                  <td>
                    <Link
                      href={getPath(
                        servicerequeststatus["@id"],
                        "/servicerequeststatuss/[id]/edit"
                      )}
                    >
                      <a>
                        <i className="bi bi-pen" aria-hidden="true" />
                        <span className="sr-only">Edit</span>
                      </a>
                    </Link>
                  </td>
                </tr>
              )
          )}
      </tbody>
    </table>
  </div>
);
