import { FunctionComponent, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { ErrorMessage, Formik } from "formik";
import { useMutation } from "react-query";

import { fetch, FetchError, FetchResponse } from "../../utils/dataAccess";
import { ServiceRequestStatus } from "../../types/ServiceRequestStatus";

interface Props {
  servicerequeststatus?: ServiceRequestStatus;
}

interface SaveParams {
  values: ServiceRequestStatus;
}

interface DeleteParams {
  id: string;
}

const saveServiceRequestStatus = async ({ values }: SaveParams) =>
  await fetch<ServiceRequestStatus>(
    !values["@id"] ? "/service_request_statuses" : values["@id"],
    {
      method: !values["@id"] ? "POST" : "PUT",
      body: JSON.stringify(values),
    }
  );

const deleteServiceRequestStatus = async (id: string) =>
  await fetch<ServiceRequestStatus>(id, { method: "DELETE" });

export const Form: FunctionComponent<Props> = ({ servicerequeststatus }) => {
  const [, setError] = useState<string | null>(null);
  const router = useRouter();

  const saveMutation = useMutation<
    FetchResponse<ServiceRequestStatus> | undefined,
    Error | FetchError,
    SaveParams
  >((saveParams) => saveServiceRequestStatus(saveParams));

  const deleteMutation = useMutation<
    FetchResponse<ServiceRequestStatus> | undefined,
    Error | FetchError,
    DeleteParams
  >(({ id }) => deleteServiceRequestStatus(id), {
    onSuccess: () => {
      router.push("/servicerequeststatuss");
    },
    onError: (error) => {
      setError(`Error when deleting the resource: ${error}`);
      console.error(error);
    },
  });

  const handleDelete = () => {
    if (!servicerequeststatus || !servicerequeststatus["@id"]) return;
    if (!window.confirm("Are you sure you want to delete this item?")) return;
    deleteMutation.mutate({ id: servicerequeststatus["@id"] });
  };

  return (
    <div>
      <h1>
        {servicerequeststatus
          ? `Edit ServiceRequestStatus ${servicerequeststatus["@id"]}`
          : `Create ServiceRequestStatus`}
      </h1>
      <Formik
        initialValues={
          servicerequeststatus
            ? {
                ...servicerequeststatus,
              }
            : new ServiceRequestStatus()
        }
        validate={() => {
          const errors = {};
          // add your validation logic here
          return errors;
        }}
        onSubmit={(values, { setSubmitting, setStatus, setErrors }) => {
          const isCreation = !values["@id"];
          saveMutation.mutate(
            { values },
            {
              onSuccess: () => {
                setStatus({
                  isValid: true,
                  msg: `Element ${isCreation ? "created" : "updated"}.`,
                });
                router.push("/service_request_statuses");
              },
              onError: (error) => {
                setStatus({
                  isValid: false,
                  msg: `${error.message}`,
                });
                if ("fields" in error) {
                  setErrors(error.fields);
                }
              },
              onSettled: () => {
                setSubmitting(false);
              },
            }
          );
        }}
      >
        {({
          values,
          status,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
          <form onSubmit={handleSubmit}>
            {status && status.msg && (
              <div
                className={`alert ${
                  status.isValid ? "alert-success" : "alert-danger"
                }`}
                role="alert"
              >
                {status.msg}
              </div>
            )}
            <button
              type="submit"
              className="btn btn-success"
              disabled={isSubmitting}
            >
              Submit
            </button>
          </form>
        )}
      </Formik>
      <Link href="/servicerequeststatuss">
        <a className="btn btn-primary">Back to list</a>
      </Link>
      {servicerequeststatus && (
        <button className="btn btn-danger" onClick={handleDelete}>
          <a>Delete</a>
        </button>
      )}
    </div>
  );
};
