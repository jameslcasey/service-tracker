import { FunctionComponent, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { ErrorMessage, Formik } from "formik";
import { useMutation } from "react-query";

import { fetch, FetchError, FetchResponse } from "../../utils/dataAccess";
import { AccountStatus } from "../../types/AccountStatus";

interface Props {
  accountstatus?: AccountStatus;
}

interface SaveParams {
  values: AccountStatus;
}

interface DeleteParams {
  id: string;
}

const saveAccountStatus = async ({ values }: SaveParams) =>
  await fetch<AccountStatus>(
    !values["@id"] ? "/account_statuses" : values["@id"],
    {
      method: !values["@id"] ? "POST" : "PUT",
      body: JSON.stringify(values),
    }
  );

const deleteAccountStatus = async (id: string) =>
  await fetch<AccountStatus>(id, { method: "DELETE" });

export const Form: FunctionComponent<Props> = ({ accountstatus }) => {
  const [, setError] = useState<string | null>(null);
  const router = useRouter();

  const saveMutation = useMutation<
    FetchResponse<AccountStatus> | undefined,
    Error | FetchError,
    SaveParams
  >((saveParams) => saveAccountStatus(saveParams));

  const deleteMutation = useMutation<
    FetchResponse<AccountStatus> | undefined,
    Error | FetchError,
    DeleteParams
  >(({ id }) => deleteAccountStatus(id), {
    onSuccess: () => {
      router.push("/accountstatuss");
    },
    onError: (error) => {
      setError(`Error when deleting the resource: ${error}`);
      console.error(error);
    },
  });

  const handleDelete = () => {
    if (!accountstatus || !accountstatus["@id"]) return;
    if (!window.confirm("Are you sure you want to delete this item?")) return;
    deleteMutation.mutate({ id: accountstatus["@id"] });
  };

  return (
    <div>
      <h1>
        {accountstatus
          ? `Edit AccountStatus ${accountstatus["@id"]}`
          : `Create AccountStatus`}
      </h1>
      <Formik
        initialValues={
          accountstatus
            ? {
                ...accountstatus,
              }
            : new AccountStatus()
        }
        validate={() => {
          const errors = {};
          // add your validation logic here
          return errors;
        }}
        onSubmit={(values, { setSubmitting, setStatus, setErrors }) => {
          const isCreation = !values["@id"];
          saveMutation.mutate(
            { values },
            {
              onSuccess: () => {
                setStatus({
                  isValid: true,
                  msg: `Element ${isCreation ? "created" : "updated"}.`,
                });
                router.push("/account_statuses");
              },
              onError: (error) => {
                setStatus({
                  isValid: false,
                  msg: `${error.message}`,
                });
                if ("fields" in error) {
                  setErrors(error.fields);
                }
              },
              onSettled: () => {
                setSubmitting(false);
              },
            }
          );
        }}
      >
        {({
          values,
          status,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
          <form onSubmit={handleSubmit}>
            {status && status.msg && (
              <div
                className={`alert ${
                  status.isValid ? "alert-success" : "alert-danger"
                }`}
                role="alert"
              >
                {status.msg}
              </div>
            )}
            <button
              type="submit"
              className="btn btn-success"
              disabled={isSubmitting}
            >
              Submit
            </button>
          </form>
        )}
      </Formik>
      <Link href="/accountstatuss">
        <a className="btn btn-primary">Back to list</a>
      </Link>
      {accountstatus && (
        <button className="btn btn-danger" onClick={handleDelete}>
          <a>Delete</a>
        </button>
      )}
    </div>
  );
};
