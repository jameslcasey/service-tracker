import { FunctionComponent } from "react";
import Link from "next/link";

import ReferenceLinks from "../common/ReferenceLinks";
import { getPath } from "../../utils/dataAccess";
import { AccountStatus } from "../../types/AccountStatus";

interface Props {
  accountstatuss: AccountStatus[];
}

export const List: FunctionComponent<Props> = ({ accountstatuss }) => (
  <div>
    <h1>AccountStatus List</h1>
    <Link href="/accountstatuss/create">
      <a className="btn btn-primary">Create</a>
    </Link>
    <table className="table table-responsive table-striped table-hover">
      <thead>
        <tr>
          <th>id</th>
          <th />
        </tr>
      </thead>
      <tbody>
        {accountstatuss &&
          accountstatuss.length !== 0 &&
          accountstatuss.map(
            (accountstatus) =>
              accountstatus["@id"] && (
                <tr key={accountstatus["@id"]}>
                  <th scope="row">
                    <ReferenceLinks
                      items={{
                        href: getPath(
                          accountstatus["@id"],
                          "/accountstatuss/[id]"
                        ),
                        name: accountstatus["@id"],
                      }}
                    />
                  </th>
                  <td>
                    <Link
                      href={getPath(
                        accountstatus["@id"],
                        "/accountstatuss/[id]"
                      )}
                    >
                      <a>
                        <i className="bi bi-search" aria-hidden="true"></i>
                        <span className="sr-only">Show</span>
                      </a>
                    </Link>
                  </td>
                  <td>
                    <Link
                      href={getPath(
                        accountstatus["@id"],
                        "/accountstatuss/[id]/edit"
                      )}
                    >
                      <a>
                        <i className="bi bi-pen" aria-hidden="true" />
                        <span className="sr-only">Edit</span>
                      </a>
                    </Link>
                  </td>
                </tr>
              )
          )}
      </tbody>
    </table>
  </div>
);
