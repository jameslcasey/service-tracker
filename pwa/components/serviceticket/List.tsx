import { FunctionComponent } from "react";
import Link from "next/link";

import ReferenceLinks from "../common/ReferenceLinks";
import { getPath } from "../../utils/dataAccess";
import { ServiceTicket } from "../../types/ServiceTicket";

interface Props {
  servicetickets: ServiceTicket[];
}

export const List: FunctionComponent<Props> = ({ servicetickets }) => (
  <div>
    <h1>ServiceTicket List</h1>
    <Link href="/servicetickets/create">
      <a className="btn btn-primary">Create</a>
    </Link>
    <table className="table table-responsive table-striped table-hover">
      <thead>
        <tr>
          <th>id</th>
          <th />
        </tr>
      </thead>
      <tbody>
        {servicetickets &&
          servicetickets.length !== 0 &&
          servicetickets.map(
            (serviceticket) =>
              serviceticket["@id"] && (
                <tr key={serviceticket["@id"]}>
                  <th scope="row">
                    <ReferenceLinks
                      items={{
                        href: getPath(
                          serviceticket["@id"],
                          "/servicetickets/[id]"
                        ),
                        name: serviceticket["@id"],
                      }}
                    />
                  </th>
                  <td>
                    <Link
                      href={getPath(
                        serviceticket["@id"],
                        "/servicetickets/[id]"
                      )}
                    >
                      <a>
                        <i className="bi bi-search" aria-hidden="true"></i>
                        <span className="sr-only">Show</span>
                      </a>
                    </Link>
                  </td>
                  <td>
                    <Link
                      href={getPath(
                        serviceticket["@id"],
                        "/servicetickets/[id]/edit"
                      )}
                    >
                      <a>
                        <i className="bi bi-pen" aria-hidden="true" />
                        <span className="sr-only">Edit</span>
                      </a>
                    </Link>
                  </td>
                </tr>
              )
          )}
      </tbody>
    </table>
  </div>
);
