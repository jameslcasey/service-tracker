import { FunctionComponent, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { ErrorMessage, Formik } from "formik";
import { useMutation } from "react-query";

import { fetch, FetchError, FetchResponse } from "../../utils/dataAccess";
import { ServiceTicket } from "../../types/ServiceTicket";

interface Props {
  serviceticket?: ServiceTicket;
}

interface SaveParams {
  values: ServiceTicket;
}

interface DeleteParams {
  id: string;
}

const saveServiceTicket = async ({ values }: SaveParams) =>
  await fetch<ServiceTicket>(
    !values["@id"] ? "/service_tickets" : values["@id"],
    {
      method: !values["@id"] ? "POST" : "PUT",
      body: JSON.stringify(values),
    }
  );

const deleteServiceTicket = async (id: string) =>
  await fetch<ServiceTicket>(id, { method: "DELETE" });

export const Form: FunctionComponent<Props> = ({ serviceticket }) => {
  const [, setError] = useState<string | null>(null);
  const router = useRouter();

  const saveMutation = useMutation<
    FetchResponse<ServiceTicket> | undefined,
    Error | FetchError,
    SaveParams
  >((saveParams) => saveServiceTicket(saveParams));

  const deleteMutation = useMutation<
    FetchResponse<ServiceTicket> | undefined,
    Error | FetchError,
    DeleteParams
  >(({ id }) => deleteServiceTicket(id), {
    onSuccess: () => {
      router.push("/servicetickets");
    },
    onError: (error) => {
      setError(`Error when deleting the resource: ${error}`);
      console.error(error);
    },
  });

  const handleDelete = () => {
    if (!serviceticket || !serviceticket["@id"]) return;
    if (!window.confirm("Are you sure you want to delete this item?")) return;
    deleteMutation.mutate({ id: serviceticket["@id"] });
  };

  return (
    <div>
      <h1>
        {serviceticket
          ? `Edit ServiceTicket ${serviceticket["@id"]}`
          : `Create ServiceTicket`}
      </h1>
      <Formik
        initialValues={
          serviceticket
            ? {
                ...serviceticket,
              }
            : new ServiceTicket()
        }
        validate={() => {
          const errors = {};
          // add your validation logic here
          return errors;
        }}
        onSubmit={(values, { setSubmitting, setStatus, setErrors }) => {
          const isCreation = !values["@id"];
          saveMutation.mutate(
            { values },
            {
              onSuccess: () => {
                setStatus({
                  isValid: true,
                  msg: `Element ${isCreation ? "created" : "updated"}.`,
                });
                router.push("/service_tickets");
              },
              onError: (error) => {
                setStatus({
                  isValid: false,
                  msg: `${error.message}`,
                });
                if ("fields" in error) {
                  setErrors(error.fields);
                }
              },
              onSettled: () => {
                setSubmitting(false);
              },
            }
          );
        }}
      >
        {({
          values,
          status,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
          <form onSubmit={handleSubmit}>
            {status && status.msg && (
              <div
                className={`alert ${
                  status.isValid ? "alert-success" : "alert-danger"
                }`}
                role="alert"
              >
                {status.msg}
              </div>
            )}
            <button
              type="submit"
              className="btn btn-success"
              disabled={isSubmitting}
            >
              Submit
            </button>
          </form>
        )}
      </Formik>
      <Link href="/servicetickets">
        <a className="btn btn-primary">Back to list</a>
      </Link>
      {serviceticket && (
        <button className="btn btn-danger" onClick={handleDelete}>
          <a>Delete</a>
        </button>
      )}
    </div>
  );
};
