import { FunctionComponent } from "react";
import Link from "next/link";

import ReferenceLinks from "../common/ReferenceLinks";
import { getPath } from "../../utils/dataAccess";
import { ProductStatus } from "../../types/ProductStatus";

interface Props {
  productstatuss: ProductStatus[];
}

export const List: FunctionComponent<Props> = ({ productstatuss }) => (
  <div>
    <h1>ProductStatus List</h1>
    <Link href="/productstatuss/create">
      <a className="btn btn-primary">Create</a>
    </Link>
    <table className="table table-responsive table-striped table-hover">
      <thead>
        <tr>
          <th>id</th>
          <th />
        </tr>
      </thead>
      <tbody>
        {productstatuss &&
          productstatuss.length !== 0 &&
          productstatuss.map(
            (productstatus) =>
              productstatus["@id"] && (
                <tr key={productstatus["@id"]}>
                  <th scope="row">
                    <ReferenceLinks
                      items={{
                        href: getPath(
                          productstatus["@id"],
                          "/productstatuss/[id]"
                        ),
                        name: productstatus["@id"],
                      }}
                    />
                  </th>
                  <td>
                    <Link
                      href={getPath(
                        productstatus["@id"],
                        "/productstatuss/[id]"
                      )}
                    >
                      <a>
                        <i className="bi bi-search" aria-hidden="true"></i>
                        <span className="sr-only">Show</span>
                      </a>
                    </Link>
                  </td>
                  <td>
                    <Link
                      href={getPath(
                        productstatus["@id"],
                        "/productstatuss/[id]/edit"
                      )}
                    >
                      <a>
                        <i className="bi bi-pen" aria-hidden="true" />
                        <span className="sr-only">Edit</span>
                      </a>
                    </Link>
                  </td>
                </tr>
              )
          )}
      </tbody>
    </table>
  </div>
);
