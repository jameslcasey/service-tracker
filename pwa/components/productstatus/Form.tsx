import { FunctionComponent, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { ErrorMessage, Formik } from "formik";
import { useMutation } from "react-query";

import { fetch, FetchError, FetchResponse } from "../../utils/dataAccess";
import { ProductStatus } from "../../types/ProductStatus";

interface Props {
  productstatus?: ProductStatus;
}

interface SaveParams {
  values: ProductStatus;
}

interface DeleteParams {
  id: string;
}

const saveProductStatus = async ({ values }: SaveParams) =>
  await fetch<ProductStatus>(
    !values["@id"] ? "/product_statuses" : values["@id"],
    {
      method: !values["@id"] ? "POST" : "PUT",
      body: JSON.stringify(values),
    }
  );

const deleteProductStatus = async (id: string) =>
  await fetch<ProductStatus>(id, { method: "DELETE" });

export const Form: FunctionComponent<Props> = ({ productstatus }) => {
  const [, setError] = useState<string | null>(null);
  const router = useRouter();

  const saveMutation = useMutation<
    FetchResponse<ProductStatus> | undefined,
    Error | FetchError,
    SaveParams
  >((saveParams) => saveProductStatus(saveParams));

  const deleteMutation = useMutation<
    FetchResponse<ProductStatus> | undefined,
    Error | FetchError,
    DeleteParams
  >(({ id }) => deleteProductStatus(id), {
    onSuccess: () => {
      router.push("/productstatuss");
    },
    onError: (error) => {
      setError(`Error when deleting the resource: ${error}`);
      console.error(error);
    },
  });

  const handleDelete = () => {
    if (!productstatus || !productstatus["@id"]) return;
    if (!window.confirm("Are you sure you want to delete this item?")) return;
    deleteMutation.mutate({ id: productstatus["@id"] });
  };

  return (
    <div>
      <h1>
        {productstatus
          ? `Edit ProductStatus ${productstatus["@id"]}`
          : `Create ProductStatus`}
      </h1>
      <Formik
        initialValues={
          productstatus
            ? {
                ...productstatus,
              }
            : new ProductStatus()
        }
        validate={() => {
          const errors = {};
          // add your validation logic here
          return errors;
        }}
        onSubmit={(values, { setSubmitting, setStatus, setErrors }) => {
          const isCreation = !values["@id"];
          saveMutation.mutate(
            { values },
            {
              onSuccess: () => {
                setStatus({
                  isValid: true,
                  msg: `Element ${isCreation ? "created" : "updated"}.`,
                });
                router.push("/product_statuses");
              },
              onError: (error) => {
                setStatus({
                  isValid: false,
                  msg: `${error.message}`,
                });
                if ("fields" in error) {
                  setErrors(error.fields);
                }
              },
              onSettled: () => {
                setSubmitting(false);
              },
            }
          );
        }}
      >
        {({
          values,
          status,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
          <form onSubmit={handleSubmit}>
            {status && status.msg && (
              <div
                className={`alert ${
                  status.isValid ? "alert-success" : "alert-danger"
                }`}
                role="alert"
              >
                {status.msg}
              </div>
            )}
            <button
              type="submit"
              className="btn btn-success"
              disabled={isSubmitting}
            >
              Submit
            </button>
          </form>
        )}
      </Formik>
      <Link href="/productstatuss">
        <a className="btn btn-primary">Back to list</a>
      </Link>
      {productstatus && (
        <button className="btn btn-danger" onClick={handleDelete}>
          <a>Delete</a>
        </button>
      )}
    </div>
  );
};
