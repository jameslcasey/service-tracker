import { FunctionComponent } from "react";
import Link from "next/link";

import ReferenceLinks from "../common/ReferenceLinks";
import { getPath } from "../../utils/dataAccess";
import { AddressStatus } from "../../types/AddressStatus";

interface Props {
  addressstatuss: AddressStatus[];
}

export const List: FunctionComponent<Props> = ({ addressstatuss }) => (
  <div>
    <h1>AddressStatus List</h1>
    <Link href="/addressstatuss/create">
      <a className="btn btn-primary">Create</a>
    </Link>
    <table className="table table-responsive table-striped table-hover">
      <thead>
        <tr>
          <th>id</th>
          <th />
        </tr>
      </thead>
      <tbody>
        {addressstatuss &&
          addressstatuss.length !== 0 &&
          addressstatuss.map(
            (addressstatus) =>
              addressstatus["@id"] && (
                <tr key={addressstatus["@id"]}>
                  <th scope="row">
                    <ReferenceLinks
                      items={{
                        href: getPath(
                          addressstatus["@id"],
                          "/addressstatuss/[id]"
                        ),
                        name: addressstatus["@id"],
                      }}
                    />
                  </th>
                  <td>
                    <Link
                      href={getPath(
                        addressstatus["@id"],
                        "/addressstatuss/[id]"
                      )}
                    >
                      <a>
                        <i className="bi bi-search" aria-hidden="true"></i>
                        <span className="sr-only">Show</span>
                      </a>
                    </Link>
                  </td>
                  <td>
                    <Link
                      href={getPath(
                        addressstatus["@id"],
                        "/addressstatuss/[id]/edit"
                      )}
                    >
                      <a>
                        <i className="bi bi-pen" aria-hidden="true" />
                        <span className="sr-only">Edit</span>
                      </a>
                    </Link>
                  </td>
                </tr>
              )
          )}
      </tbody>
    </table>
  </div>
);
