import { FunctionComponent, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { ErrorMessage, Formik } from "formik";
import { useMutation } from "react-query";

import { fetch, FetchError, FetchResponse } from "../../utils/dataAccess";
import { AddressStatus } from "../../types/AddressStatus";

interface Props {
  addressstatus?: AddressStatus;
}

interface SaveParams {
  values: AddressStatus;
}

interface DeleteParams {
  id: string;
}

const saveAddressStatus = async ({ values }: SaveParams) =>
  await fetch<AddressStatus>(
    !values["@id"] ? "/address_statuses" : values["@id"],
    {
      method: !values["@id"] ? "POST" : "PUT",
      body: JSON.stringify(values),
    }
  );

const deleteAddressStatus = async (id: string) =>
  await fetch<AddressStatus>(id, { method: "DELETE" });

export const Form: FunctionComponent<Props> = ({ addressstatus }) => {
  const [, setError] = useState<string | null>(null);
  const router = useRouter();

  const saveMutation = useMutation<
    FetchResponse<AddressStatus> | undefined,
    Error | FetchError,
    SaveParams
  >((saveParams) => saveAddressStatus(saveParams));

  const deleteMutation = useMutation<
    FetchResponse<AddressStatus> | undefined,
    Error | FetchError,
    DeleteParams
  >(({ id }) => deleteAddressStatus(id), {
    onSuccess: () => {
      router.push("/addressstatuss");
    },
    onError: (error) => {
      setError(`Error when deleting the resource: ${error}`);
      console.error(error);
    },
  });

  const handleDelete = () => {
    if (!addressstatus || !addressstatus["@id"]) return;
    if (!window.confirm("Are you sure you want to delete this item?")) return;
    deleteMutation.mutate({ id: addressstatus["@id"] });
  };

  return (
    <div>
      <h1>
        {addressstatus
          ? `Edit AddressStatus ${addressstatus["@id"]}`
          : `Create AddressStatus`}
      </h1>
      <Formik
        initialValues={
          addressstatus
            ? {
                ...addressstatus,
              }
            : new AddressStatus()
        }
        validate={() => {
          const errors = {};
          // add your validation logic here
          return errors;
        }}
        onSubmit={(values, { setSubmitting, setStatus, setErrors }) => {
          const isCreation = !values["@id"];
          saveMutation.mutate(
            { values },
            {
              onSuccess: () => {
                setStatus({
                  isValid: true,
                  msg: `Element ${isCreation ? "created" : "updated"}.`,
                });
                router.push("/address_statuses");
              },
              onError: (error) => {
                setStatus({
                  isValid: false,
                  msg: `${error.message}`,
                });
                if ("fields" in error) {
                  setErrors(error.fields);
                }
              },
              onSettled: () => {
                setSubmitting(false);
              },
            }
          );
        }}
      >
        {({
          values,
          status,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
          <form onSubmit={handleSubmit}>
            {status && status.msg && (
              <div
                className={`alert ${
                  status.isValid ? "alert-success" : "alert-danger"
                }`}
                role="alert"
              >
                {status.msg}
              </div>
            )}
            <button
              type="submit"
              className="btn btn-success"
              disabled={isSubmitting}
            >
              Submit
            </button>
          </form>
        )}
      </Formik>
      <Link href="/addressstatuss">
        <a className="btn btn-primary">Back to list</a>
      </Link>
      {addressstatus && (
        <button className="btn btn-danger" onClick={handleDelete}>
          <a>Delete</a>
        </button>
      )}
    </div>
  );
};
