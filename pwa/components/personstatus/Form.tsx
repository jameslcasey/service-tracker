import { FunctionComponent, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { ErrorMessage, Formik } from "formik";
import { useMutation } from "react-query";

import { fetch, FetchError, FetchResponse } from "../../utils/dataAccess";
import { PersonStatus } from "../../types/PersonStatus";

interface Props {
  personstatus?: PersonStatus;
}

interface SaveParams {
  values: PersonStatus;
}

interface DeleteParams {
  id: string;
}

const savePersonStatus = async ({ values }: SaveParams) =>
  await fetch<PersonStatus>(
    !values["@id"] ? "/person_statuses" : values["@id"],
    {
      method: !values["@id"] ? "POST" : "PUT",
      body: JSON.stringify(values),
    }
  );

const deletePersonStatus = async (id: string) =>
  await fetch<PersonStatus>(id, { method: "DELETE" });

export const Form: FunctionComponent<Props> = ({ personstatus }) => {
  const [, setError] = useState<string | null>(null);
  const router = useRouter();

  const saveMutation = useMutation<
    FetchResponse<PersonStatus> | undefined,
    Error | FetchError,
    SaveParams
  >((saveParams) => savePersonStatus(saveParams));

  const deleteMutation = useMutation<
    FetchResponse<PersonStatus> | undefined,
    Error | FetchError,
    DeleteParams
  >(({ id }) => deletePersonStatus(id), {
    onSuccess: () => {
      router.push("/personstatuss");
    },
    onError: (error) => {
      setError(`Error when deleting the resource: ${error}`);
      console.error(error);
    },
  });

  const handleDelete = () => {
    if (!personstatus || !personstatus["@id"]) return;
    if (!window.confirm("Are you sure you want to delete this item?")) return;
    deleteMutation.mutate({ id: personstatus["@id"] });
  };

  return (
    <div>
      <h1>
        {personstatus
          ? `Edit PersonStatus ${personstatus["@id"]}`
          : `Create PersonStatus`}
      </h1>
      <Formik
        initialValues={
          personstatus
            ? {
                ...personstatus,
              }
            : new PersonStatus()
        }
        validate={() => {
          const errors = {};
          // add your validation logic here
          return errors;
        }}
        onSubmit={(values, { setSubmitting, setStatus, setErrors }) => {
          const isCreation = !values["@id"];
          saveMutation.mutate(
            { values },
            {
              onSuccess: () => {
                setStatus({
                  isValid: true,
                  msg: `Element ${isCreation ? "created" : "updated"}.`,
                });
                router.push("/person_statuses");
              },
              onError: (error) => {
                setStatus({
                  isValid: false,
                  msg: `${error.message}`,
                });
                if ("fields" in error) {
                  setErrors(error.fields);
                }
              },
              onSettled: () => {
                setSubmitting(false);
              },
            }
          );
        }}
      >
        {({
          values,
          status,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
          <form onSubmit={handleSubmit}>
            {status && status.msg && (
              <div
                className={`alert ${
                  status.isValid ? "alert-success" : "alert-danger"
                }`}
                role="alert"
              >
                {status.msg}
              </div>
            )}
            <button
              type="submit"
              className="btn btn-success"
              disabled={isSubmitting}
            >
              Submit
            </button>
          </form>
        )}
      </Formik>
      <Link href="/personstatuss">
        <a className="btn btn-primary">Back to list</a>
      </Link>
      {personstatus && (
        <button className="btn btn-danger" onClick={handleDelete}>
          <a>Delete</a>
        </button>
      )}
    </div>
  );
};
