import { FunctionComponent } from "react";
import Link from "next/link";

import ReferenceLinks from "../common/ReferenceLinks";
import { getPath } from "../../utils/dataAccess";
import { PersonStatus } from "../../types/PersonStatus";

interface Props {
  personstatuss: PersonStatus[];
}

export const List: FunctionComponent<Props> = ({ personstatuss }) => (
  <div>
    <h1>PersonStatus List</h1>
    <Link href="/personstatuss/create">
      <a className="btn btn-primary">Create</a>
    </Link>
    <table className="table table-responsive table-striped table-hover">
      <thead>
        <tr>
          <th>id</th>
          <th />
        </tr>
      </thead>
      <tbody>
        {personstatuss &&
          personstatuss.length !== 0 &&
          personstatuss.map(
            (personstatus) =>
              personstatus["@id"] && (
                <tr key={personstatus["@id"]}>
                  <th scope="row">
                    <ReferenceLinks
                      items={{
                        href: getPath(
                          personstatus["@id"],
                          "/personstatuss/[id]"
                        ),
                        name: personstatus["@id"],
                      }}
                    />
                  </th>
                  <td>
                    <Link
                      href={getPath(personstatus["@id"], "/personstatuss/[id]")}
                    >
                      <a>
                        <i className="bi bi-search" aria-hidden="true"></i>
                        <span className="sr-only">Show</span>
                      </a>
                    </Link>
                  </td>
                  <td>
                    <Link
                      href={getPath(
                        personstatus["@id"],
                        "/personstatuss/[id]/edit"
                      )}
                    >
                      <a>
                        <i className="bi bi-pen" aria-hidden="true" />
                        <span className="sr-only">Edit</span>
                      </a>
                    </Link>
                  </td>
                </tr>
              )
          )}
      </tbody>
    </table>
  </div>
);
