import { FunctionComponent } from "react";
import Link from "next/link";

import ReferenceLinks from "../common/ReferenceLinks";
import { getPath } from "../../utils/dataAccess";
import { Address } from "../../types/Address";

interface Props {
  addresss: Address[];
}

export const List: FunctionComponent<Props> = ({ addresss }) => (
  <div>
    <h1>Address List</h1>
    <Link href="/addresss/create">
      <a className="btn btn-primary">Create</a>
    </Link>
    <table className="table table-responsive table-striped table-hover">
      <thead>
        <tr>
          <th>id</th>
          <th />
        </tr>
      </thead>
      <tbody>
        {addresss &&
          addresss.length !== 0 &&
          addresss.map(
            (address) =>
              address["@id"] && (
                <tr key={address["@id"]}>
                  <th scope="row">
                    <ReferenceLinks
                      items={{
                        href: getPath(address["@id"], "/addresss/[id]"),
                        name: address["@id"],
                      }}
                    />
                  </th>
                  <td>
                    <Link href={getPath(address["@id"], "/addresss/[id]")}>
                      <a>
                        <i className="bi bi-search" aria-hidden="true"></i>
                        <span className="sr-only">Show</span>
                      </a>
                    </Link>
                  </td>
                  <td>
                    <Link href={getPath(address["@id"], "/addresss/[id]/edit")}>
                      <a>
                        <i className="bi bi-pen" aria-hidden="true" />
                        <span className="sr-only">Edit</span>
                      </a>
                    </Link>
                  </td>
                </tr>
              )
          )}
      </tbody>
    </table>
  </div>
);
