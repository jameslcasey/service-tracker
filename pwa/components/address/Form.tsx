import { FunctionComponent, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { ErrorMessage, Formik } from "formik";
import { useMutation } from "react-query";

import { fetch, FetchError, FetchResponse } from "../../utils/dataAccess";
import { Address } from "../../types/Address";

interface Props {
  address?: Address;
}

interface SaveParams {
  values: Address;
}

interface DeleteParams {
  id: string;
}

const saveAddress = async ({ values }: SaveParams) =>
  await fetch<Address>(!values["@id"] ? "/addresses" : values["@id"], {
    method: !values["@id"] ? "POST" : "PUT",
    body: JSON.stringify(values),
  });

const deleteAddress = async (id: string) =>
  await fetch<Address>(id, { method: "DELETE" });

export const Form: FunctionComponent<Props> = ({ address }) => {
  const [, setError] = useState<string | null>(null);
  const router = useRouter();

  const saveMutation = useMutation<
    FetchResponse<Address> | undefined,
    Error | FetchError,
    SaveParams
  >((saveParams) => saveAddress(saveParams));

  const deleteMutation = useMutation<
    FetchResponse<Address> | undefined,
    Error | FetchError,
    DeleteParams
  >(({ id }) => deleteAddress(id), {
    onSuccess: () => {
      router.push("/addresss");
    },
    onError: (error) => {
      setError(`Error when deleting the resource: ${error}`);
      console.error(error);
    },
  });

  const handleDelete = () => {
    if (!address || !address["@id"]) return;
    if (!window.confirm("Are you sure you want to delete this item?")) return;
    deleteMutation.mutate({ id: address["@id"] });
  };

  return (
    <div>
      <h1>{address ? `Edit Address ${address["@id"]}` : `Create Address`}</h1>
      <Formik
        initialValues={
          address
            ? {
                ...address,
              }
            : new Address()
        }
        validate={() => {
          const errors = {};
          // add your validation logic here
          return errors;
        }}
        onSubmit={(values, { setSubmitting, setStatus, setErrors }) => {
          const isCreation = !values["@id"];
          saveMutation.mutate(
            { values },
            {
              onSuccess: () => {
                setStatus({
                  isValid: true,
                  msg: `Element ${isCreation ? "created" : "updated"}.`,
                });
                router.push("/addresses");
              },
              onError: (error) => {
                setStatus({
                  isValid: false,
                  msg: `${error.message}`,
                });
                if ("fields" in error) {
                  setErrors(error.fields);
                }
              },
              onSettled: () => {
                setSubmitting(false);
              },
            }
          );
        }}
      >
        {({
          values,
          status,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
          <form onSubmit={handleSubmit}>
            {status && status.msg && (
              <div
                className={`alert ${
                  status.isValid ? "alert-success" : "alert-danger"
                }`}
                role="alert"
              >
                {status.msg}
              </div>
            )}
            <button
              type="submit"
              className="btn btn-success"
              disabled={isSubmitting}
            >
              Submit
            </button>
          </form>
        )}
      </Formik>
      <Link href="/addresss">
        <a className="btn btn-primary">Back to list</a>
      </Link>
      {address && (
        <button className="btn btn-danger" onClick={handleDelete}>
          <a>Delete</a>
        </button>
      )}
    </div>
  );
};
