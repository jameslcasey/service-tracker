import { FunctionComponent, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { ErrorMessage, Formik } from "formik";
import { useMutation } from "react-query";

import { fetch, FetchError, FetchResponse } from "../../utils/dataAccess";
import { Account } from "../../types/Account";

interface Props {
  account?: Account;
}

interface SaveParams {
  values: Account;
}

interface DeleteParams {
  id: string;
}

const saveAccount = async ({ values }: SaveParams) =>
  await fetch<Account>(!values["@id"] ? "/accounts" : values["@id"], {
    method: !values["@id"] ? "POST" : "PUT",
    body: JSON.stringify(values),
  });

const deleteAccount = async (id: string) =>
  await fetch<Account>(id, { method: "DELETE" });

export const Form: FunctionComponent<Props> = ({ account }) => {
  const [, setError] = useState<string | null>(null);
  const router = useRouter();

  const saveMutation = useMutation<
    FetchResponse<Account> | undefined,
    Error | FetchError,
    SaveParams
  >((saveParams) => saveAccount(saveParams));

  const deleteMutation = useMutation<
    FetchResponse<Account> | undefined,
    Error | FetchError,
    DeleteParams
  >(({ id }) => deleteAccount(id), {
    onSuccess: () => {
      router.push("/accounts");
    },
    onError: (error) => {
      setError(`Error when deleting the resource: ${error}`);
      console.error(error);
    },
  });

  const handleDelete = () => {
    if (!account || !account["@id"]) return;
    if (!window.confirm("Are you sure you want to delete this item?")) return;
    deleteMutation.mutate({ id: account["@id"] });
  };

  return (
    <div>
      <h1>{account ? `Edit Account ${account["@id"]}` : `Create Account`}</h1>
      <Formik
        initialValues={
          account
            ? {
                ...account,
              }
            : new Account()
        }
        validate={() => {
          const errors = {};
          // add your validation logic here
          return errors;
        }}
        onSubmit={(values, { setSubmitting, setStatus, setErrors }) => {
          const isCreation = !values["@id"];
          saveMutation.mutate(
            { values },
            {
              onSuccess: () => {
                setStatus({
                  isValid: true,
                  msg: `Element ${isCreation ? "created" : "updated"}.`,
                });
                router.push("/accounts");
              },
              onError: (error) => {
                setStatus({
                  isValid: false,
                  msg: `${error.message}`,
                });
                if ("fields" in error) {
                  setErrors(error.fields);
                }
              },
              onSettled: () => {
                setSubmitting(false);
              },
            }
          );
        }}
      >
        {({
          values,
          status,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
          <form onSubmit={handleSubmit}>
            {status && status.msg && (
              <div
                className={`alert ${
                  status.isValid ? "alert-success" : "alert-danger"
                }`}
                role="alert"
              >
                {status.msg}
              </div>
            )}
            <button
              type="submit"
              className="btn btn-success"
              disabled={isSubmitting}
            >
              Submit
            </button>
          </form>
        )}
      </Formik>
      <Link href="/accounts">
        <a className="btn btn-primary">Back to list</a>
      </Link>
      {account && (
        <button className="btn btn-danger" onClick={handleDelete}>
          <a>Delete</a>
        </button>
      )}
    </div>
  );
};
