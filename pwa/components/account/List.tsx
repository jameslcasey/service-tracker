import { FunctionComponent } from "react";
import Link from "next/link";

import ReferenceLinks from "../common/ReferenceLinks";
import { getPath } from "../../utils/dataAccess";
import { Account } from "../../types/Account";

interface Props {
  accounts: Account[];
}

export const List: FunctionComponent<Props> = ({ accounts }) => (
  <div>
    <h1>Account List</h1>
    <Link href="/accounts/create">
      <a className="btn btn-primary">Create</a>
    </Link>
    <table className="table table-responsive table-striped table-hover">
      <thead>
        <tr>
          <th>id</th>
          <th />
        </tr>
      </thead>
      <tbody>
        {accounts &&
          accounts.length !== 0 &&
          accounts.map(
            (account) =>
              account["@id"] && (
                <tr key={account["@id"]}>
                  <th scope="row">
                    <ReferenceLinks
                      items={{
                        href: getPath(account["@id"], "/accounts/[id]"),
                        name: account["@id"],
                      }}
                    />
                  </th>
                  <td>
                    <Link href={getPath(account["@id"], "/accounts/[id]")}>
                      <a>
                        <i className="bi bi-search" aria-hidden="true"></i>
                        <span className="sr-only">Show</span>
                      </a>
                    </Link>
                  </td>
                  <td>
                    <Link href={getPath(account["@id"], "/accounts/[id]/edit")}>
                      <a>
                        <i className="bi bi-pen" aria-hidden="true" />
                        <span className="sr-only">Edit</span>
                      </a>
                    </Link>
                  </td>
                </tr>
              )
          )}
      </tbody>
    </table>
  </div>
);
