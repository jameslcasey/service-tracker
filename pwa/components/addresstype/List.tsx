import { FunctionComponent } from "react";
import Link from "next/link";

import ReferenceLinks from "../common/ReferenceLinks";
import { getPath } from "../../utils/dataAccess";
import { AddressType } from "../../types/AddressType";

interface Props {
  addresstypes: AddressType[];
}

export const List: FunctionComponent<Props> = ({ addresstypes }) => (
  <div>
    <h1>AddressType List</h1>
    <Link href="/addresstypes/create">
      <a className="btn btn-primary">Create</a>
    </Link>
    <table className="table table-responsive table-striped table-hover">
      <thead>
        <tr>
          <th>id</th>
          <th />
        </tr>
      </thead>
      <tbody>
        {addresstypes &&
          addresstypes.length !== 0 &&
          addresstypes.map(
            (addresstype) =>
              addresstype["@id"] && (
                <tr key={addresstype["@id"]}>
                  <th scope="row">
                    <ReferenceLinks
                      items={{
                        href: getPath(addresstype["@id"], "/addresstypes/[id]"),
                        name: addresstype["@id"],
                      }}
                    />
                  </th>
                  <td>
                    <Link
                      href={getPath(addresstype["@id"], "/addresstypes/[id]")}
                    >
                      <a>
                        <i className="bi bi-search" aria-hidden="true"></i>
                        <span className="sr-only">Show</span>
                      </a>
                    </Link>
                  </td>
                  <td>
                    <Link
                      href={getPath(
                        addresstype["@id"],
                        "/addresstypes/[id]/edit"
                      )}
                    >
                      <a>
                        <i className="bi bi-pen" aria-hidden="true" />
                        <span className="sr-only">Edit</span>
                      </a>
                    </Link>
                  </td>
                </tr>
              )
          )}
      </tbody>
    </table>
  </div>
);
