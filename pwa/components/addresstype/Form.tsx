import { FunctionComponent, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { ErrorMessage, Formik } from "formik";
import { useMutation } from "react-query";

import { fetch, FetchError, FetchResponse } from "../../utils/dataAccess";
import { AddressType } from "../../types/AddressType";

interface Props {
  addresstype?: AddressType;
}

interface SaveParams {
  values: AddressType;
}

interface DeleteParams {
  id: string;
}

const saveAddressType = async ({ values }: SaveParams) =>
  await fetch<AddressType>(!values["@id"] ? "/address_types" : values["@id"], {
    method: !values["@id"] ? "POST" : "PUT",
    body: JSON.stringify(values),
  });

const deleteAddressType = async (id: string) =>
  await fetch<AddressType>(id, { method: "DELETE" });

export const Form: FunctionComponent<Props> = ({ addresstype }) => {
  const [, setError] = useState<string | null>(null);
  const router = useRouter();

  const saveMutation = useMutation<
    FetchResponse<AddressType> | undefined,
    Error | FetchError,
    SaveParams
  >((saveParams) => saveAddressType(saveParams));

  const deleteMutation = useMutation<
    FetchResponse<AddressType> | undefined,
    Error | FetchError,
    DeleteParams
  >(({ id }) => deleteAddressType(id), {
    onSuccess: () => {
      router.push("/addresstypes");
    },
    onError: (error) => {
      setError(`Error when deleting the resource: ${error}`);
      console.error(error);
    },
  });

  const handleDelete = () => {
    if (!addresstype || !addresstype["@id"]) return;
    if (!window.confirm("Are you sure you want to delete this item?")) return;
    deleteMutation.mutate({ id: addresstype["@id"] });
  };

  return (
    <div>
      <h1>
        {addresstype
          ? `Edit AddressType ${addresstype["@id"]}`
          : `Create AddressType`}
      </h1>
      <Formik
        initialValues={
          addresstype
            ? {
                ...addresstype,
              }
            : new AddressType()
        }
        validate={() => {
          const errors = {};
          // add your validation logic here
          return errors;
        }}
        onSubmit={(values, { setSubmitting, setStatus, setErrors }) => {
          const isCreation = !values["@id"];
          saveMutation.mutate(
            { values },
            {
              onSuccess: () => {
                setStatus({
                  isValid: true,
                  msg: `Element ${isCreation ? "created" : "updated"}.`,
                });
                router.push("/address_types");
              },
              onError: (error) => {
                setStatus({
                  isValid: false,
                  msg: `${error.message}`,
                });
                if ("fields" in error) {
                  setErrors(error.fields);
                }
              },
              onSettled: () => {
                setSubmitting(false);
              },
            }
          );
        }}
      >
        {({
          values,
          status,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
          <form onSubmit={handleSubmit}>
            {status && status.msg && (
              <div
                className={`alert ${
                  status.isValid ? "alert-success" : "alert-danger"
                }`}
                role="alert"
              >
                {status.msg}
              </div>
            )}
            <button
              type="submit"
              className="btn btn-success"
              disabled={isSubmitting}
            >
              Submit
            </button>
          </form>
        )}
      </Formik>
      <Link href="/addresstypes">
        <a className="btn btn-primary">Back to list</a>
      </Link>
      {addresstype && (
        <button className="btn btn-danger" onClick={handleDelete}>
          <a>Delete</a>
        </button>
      )}
    </div>
  );
};
