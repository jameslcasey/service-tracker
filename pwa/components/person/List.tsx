import { FunctionComponent } from "react";
import Link from "next/link";

import ReferenceLinks from "../common/ReferenceLinks";
import { getPath } from "../../utils/dataAccess";
import { Person } from "../../types/Person";

interface Props {
  persons: Person[];
}

export const List: FunctionComponent<Props> = ({ persons }) => (
  <div>
    <h1>Person List</h1>
    <Link href="/persons/create">
      <a className="btn btn-primary">Create</a>
    </Link>
    <table className="table table-responsive table-striped table-hover">
      <thead>
        <tr>
          <th>id</th>
          <th />
        </tr>
      </thead>
      <tbody>
        {persons &&
          persons.length !== 0 &&
          persons.map(
            (person) =>
              person["@id"] && (
                <tr key={person["@id"]}>
                  <th scope="row">
                    <ReferenceLinks
                      items={{
                        href: getPath(person["@id"], "/persons/[id]"),
                        name: person["@id"],
                      }}
                    />
                  </th>
                  <td>
                    <Link href={getPath(person["@id"], "/persons/[id]")}>
                      <a>
                        <i className="bi bi-search" aria-hidden="true"></i>
                        <span className="sr-only">Show</span>
                      </a>
                    </Link>
                  </td>
                  <td>
                    <Link href={getPath(person["@id"], "/persons/[id]/edit")}>
                      <a>
                        <i className="bi bi-pen" aria-hidden="true" />
                        <span className="sr-only">Edit</span>
                      </a>
                    </Link>
                  </td>
                </tr>
              )
          )}
      </tbody>
    </table>
  </div>
);
