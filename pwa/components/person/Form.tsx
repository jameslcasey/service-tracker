import { FunctionComponent, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { ErrorMessage, Formik } from "formik";
import { useMutation } from "react-query";

import { fetch, FetchError, FetchResponse } from "../../utils/dataAccess";
import { Person } from "../../types/Person";

interface Props {
  person?: Person;
}

interface SaveParams {
  values: Person;
}

interface DeleteParams {
  id: string;
}

const savePerson = async ({ values }: SaveParams) =>
  await fetch<Person>(!values["@id"] ? "/people" : values["@id"], {
    method: !values["@id"] ? "POST" : "PUT",
    body: JSON.stringify(values),
  });

const deletePerson = async (id: string) =>
  await fetch<Person>(id, { method: "DELETE" });

export const Form: FunctionComponent<Props> = ({ person }) => {
  const [, setError] = useState<string | null>(null);
  const router = useRouter();

  const saveMutation = useMutation<
    FetchResponse<Person> | undefined,
    Error | FetchError,
    SaveParams
  >((saveParams) => savePerson(saveParams));

  const deleteMutation = useMutation<
    FetchResponse<Person> | undefined,
    Error | FetchError,
    DeleteParams
  >(({ id }) => deletePerson(id), {
    onSuccess: () => {
      router.push("/persons");
    },
    onError: (error) => {
      setError(`Error when deleting the resource: ${error}`);
      console.error(error);
    },
  });

  const handleDelete = () => {
    if (!person || !person["@id"]) return;
    if (!window.confirm("Are you sure you want to delete this item?")) return;
    deleteMutation.mutate({ id: person["@id"] });
  };

  return (
    <div>
      <h1>{person ? `Edit Person ${person["@id"]}` : `Create Person`}</h1>
      <Formik
        initialValues={
          person
            ? {
                ...person,
              }
            : new Person()
        }
        validate={() => {
          const errors = {};
          // add your validation logic here
          return errors;
        }}
        onSubmit={(values, { setSubmitting, setStatus, setErrors }) => {
          const isCreation = !values["@id"];
          saveMutation.mutate(
            { values },
            {
              onSuccess: () => {
                setStatus({
                  isValid: true,
                  msg: `Element ${isCreation ? "created" : "updated"}.`,
                });
                router.push("/people");
              },
              onError: (error) => {
                setStatus({
                  isValid: false,
                  msg: `${error.message}`,
                });
                if ("fields" in error) {
                  setErrors(error.fields);
                }
              },
              onSettled: () => {
                setSubmitting(false);
              },
            }
          );
        }}
      >
        {({
          values,
          status,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
          <form onSubmit={handleSubmit}>
            {status && status.msg && (
              <div
                className={`alert ${
                  status.isValid ? "alert-success" : "alert-danger"
                }`}
                role="alert"
              >
                {status.msg}
              </div>
            )}
            <button
              type="submit"
              className="btn btn-success"
              disabled={isSubmitting}
            >
              Submit
            </button>
          </form>
        )}
      </Formik>
      <Link href="/persons">
        <a className="btn btn-primary">Back to list</a>
      </Link>
      {person && (
        <button className="btn btn-danger" onClick={handleDelete}>
          <a>Delete</a>
        </button>
      )}
    </div>
  );
};
