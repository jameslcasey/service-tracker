import { FunctionComponent, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { ErrorMessage, Formik } from "formik";
import { useMutation } from "react-query";

import { fetch, FetchError, FetchResponse } from "../../utils/dataAccess";
import { RouteStatus } from "../../types/RouteStatus";

interface Props {
  routestatus?: RouteStatus;
}

interface SaveParams {
  values: RouteStatus;
}

interface DeleteParams {
  id: string;
}

const saveRouteStatus = async ({ values }: SaveParams) =>
  await fetch<RouteStatus>(!values["@id"] ? "/route_statuses" : values["@id"], {
    method: !values["@id"] ? "POST" : "PUT",
    body: JSON.stringify(values),
  });

const deleteRouteStatus = async (id: string) =>
  await fetch<RouteStatus>(id, { method: "DELETE" });

export const Form: FunctionComponent<Props> = ({ routestatus }) => {
  const [, setError] = useState<string | null>(null);
  const router = useRouter();

  const saveMutation = useMutation<
    FetchResponse<RouteStatus> | undefined,
    Error | FetchError,
    SaveParams
  >((saveParams) => saveRouteStatus(saveParams));

  const deleteMutation = useMutation<
    FetchResponse<RouteStatus> | undefined,
    Error | FetchError,
    DeleteParams
  >(({ id }) => deleteRouteStatus(id), {
    onSuccess: () => {
      router.push("/routestatuss");
    },
    onError: (error) => {
      setError(`Error when deleting the resource: ${error}`);
      console.error(error);
    },
  });

  const handleDelete = () => {
    if (!routestatus || !routestatus["@id"]) return;
    if (!window.confirm("Are you sure you want to delete this item?")) return;
    deleteMutation.mutate({ id: routestatus["@id"] });
  };

  return (
    <div>
      <h1>
        {routestatus
          ? `Edit RouteStatus ${routestatus["@id"]}`
          : `Create RouteStatus`}
      </h1>
      <Formik
        initialValues={
          routestatus
            ? {
                ...routestatus,
              }
            : new RouteStatus()
        }
        validate={() => {
          const errors = {};
          // add your validation logic here
          return errors;
        }}
        onSubmit={(values, { setSubmitting, setStatus, setErrors }) => {
          const isCreation = !values["@id"];
          saveMutation.mutate(
            { values },
            {
              onSuccess: () => {
                setStatus({
                  isValid: true,
                  msg: `Element ${isCreation ? "created" : "updated"}.`,
                });
                router.push("/route_statuses");
              },
              onError: (error) => {
                setStatus({
                  isValid: false,
                  msg: `${error.message}`,
                });
                if ("fields" in error) {
                  setErrors(error.fields);
                }
              },
              onSettled: () => {
                setSubmitting(false);
              },
            }
          );
        }}
      >
        {({
          values,
          status,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
          <form onSubmit={handleSubmit}>
            {status && status.msg && (
              <div
                className={`alert ${
                  status.isValid ? "alert-success" : "alert-danger"
                }`}
                role="alert"
              >
                {status.msg}
              </div>
            )}
            <button
              type="submit"
              className="btn btn-success"
              disabled={isSubmitting}
            >
              Submit
            </button>
          </form>
        )}
      </Formik>
      <Link href="/routestatuss">
        <a className="btn btn-primary">Back to list</a>
      </Link>
      {routestatus && (
        <button className="btn btn-danger" onClick={handleDelete}>
          <a>Delete</a>
        </button>
      )}
    </div>
  );
};
