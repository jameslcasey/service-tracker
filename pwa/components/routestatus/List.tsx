import { FunctionComponent } from "react";
import Link from "next/link";

import ReferenceLinks from "../common/ReferenceLinks";
import { getPath } from "../../utils/dataAccess";
import { RouteStatus } from "../../types/RouteStatus";

interface Props {
  routestatuss: RouteStatus[];
}

export const List: FunctionComponent<Props> = ({ routestatuss }) => (
  <div>
    <h1>RouteStatus List</h1>
    <Link href="/routestatuss/create">
      <a className="btn btn-primary">Create</a>
    </Link>
    <table className="table table-responsive table-striped table-hover">
      <thead>
        <tr>
          <th>id</th>
          <th />
        </tr>
      </thead>
      <tbody>
        {routestatuss &&
          routestatuss.length !== 0 &&
          routestatuss.map(
            (routestatus) =>
              routestatus["@id"] && (
                <tr key={routestatus["@id"]}>
                  <th scope="row">
                    <ReferenceLinks
                      items={{
                        href: getPath(routestatus["@id"], "/routestatuss/[id]"),
                        name: routestatus["@id"],
                      }}
                    />
                  </th>
                  <td>
                    <Link
                      href={getPath(routestatus["@id"], "/routestatuss/[id]")}
                    >
                      <a>
                        <i className="bi bi-search" aria-hidden="true"></i>
                        <span className="sr-only">Show</span>
                      </a>
                    </Link>
                  </td>
                  <td>
                    <Link
                      href={getPath(
                        routestatus["@id"],
                        "/routestatuss/[id]/edit"
                      )}
                    >
                      <a>
                        <i className="bi bi-pen" aria-hidden="true" />
                        <span className="sr-only">Edit</span>
                      </a>
                    </Link>
                  </td>
                </tr>
              )
          )}
      </tbody>
    </table>
  </div>
);
