import { FunctionComponent, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { ErrorMessage, Formik } from "formik";
import { useMutation } from "react-query";

import { fetch, FetchError, FetchResponse } from "../../utils/dataAccess";
import { Route } from "../../types/Route";

interface Props {
  route?: Route;
}

interface SaveParams {
  values: Route;
}

interface DeleteParams {
  id: string;
}

const saveRoute = async ({ values }: SaveParams) =>
  await fetch<Route>(!values["@id"] ? "/routes" : values["@id"], {
    method: !values["@id"] ? "POST" : "PUT",
    body: JSON.stringify(values),
  });

const deleteRoute = async (id: string) =>
  await fetch<Route>(id, { method: "DELETE" });

export const Form: FunctionComponent<Props> = ({ route }) => {
  const [, setError] = useState<string | null>(null);
  const router = useRouter();

  const saveMutation = useMutation<
    FetchResponse<Route> | undefined,
    Error | FetchError,
    SaveParams
  >((saveParams) => saveRoute(saveParams));

  const deleteMutation = useMutation<
    FetchResponse<Route> | undefined,
    Error | FetchError,
    DeleteParams
  >(({ id }) => deleteRoute(id), {
    onSuccess: () => {
      router.push("/routes");
    },
    onError: (error) => {
      setError(`Error when deleting the resource: ${error}`);
      console.error(error);
    },
  });

  const handleDelete = () => {
    if (!route || !route["@id"]) return;
    if (!window.confirm("Are you sure you want to delete this item?")) return;
    deleteMutation.mutate({ id: route["@id"] });
  };

  return (
    <div>
      <h1>{route ? `Edit Route ${route["@id"]}` : `Create Route`}</h1>
      <Formik
        initialValues={
          route
            ? {
                ...route,
              }
            : new Route()
        }
        validate={() => {
          const errors = {};
          // add your validation logic here
          return errors;
        }}
        onSubmit={(values, { setSubmitting, setStatus, setErrors }) => {
          const isCreation = !values["@id"];
          saveMutation.mutate(
            { values },
            {
              onSuccess: () => {
                setStatus({
                  isValid: true,
                  msg: `Element ${isCreation ? "created" : "updated"}.`,
                });
                router.push("/routes");
              },
              onError: (error) => {
                setStatus({
                  isValid: false,
                  msg: `${error.message}`,
                });
                if ("fields" in error) {
                  setErrors(error.fields);
                }
              },
              onSettled: () => {
                setSubmitting(false);
              },
            }
          );
        }}
      >
        {({
          values,
          status,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
          <form onSubmit={handleSubmit}>
            {status && status.msg && (
              <div
                className={`alert ${
                  status.isValid ? "alert-success" : "alert-danger"
                }`}
                role="alert"
              >
                {status.msg}
              </div>
            )}
            <button
              type="submit"
              className="btn btn-success"
              disabled={isSubmitting}
            >
              Submit
            </button>
          </form>
        )}
      </Formik>
      <Link href="/routes">
        <a className="btn btn-primary">Back to list</a>
      </Link>
      {route && (
        <button className="btn btn-danger" onClick={handleDelete}>
          <a>Delete</a>
        </button>
      )}
    </div>
  );
};
