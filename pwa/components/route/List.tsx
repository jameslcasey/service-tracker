import { FunctionComponent } from "react";
import Link from "next/link";

import ReferenceLinks from "../common/ReferenceLinks";
import { getPath } from "../../utils/dataAccess";
import { Route } from "../../types/Route";

interface Props {
  routes: Route[];
}

export const List: FunctionComponent<Props> = ({ routes }) => (
  <div>
    <h1>Route List</h1>
    <Link href="/routes/create">
      <a className="btn btn-primary">Create</a>
    </Link>
    <table className="table table-responsive table-striped table-hover">
      <thead>
        <tr>
          <th>id</th>
          <th />
        </tr>
      </thead>
      <tbody>
        {routes &&
          routes.length !== 0 &&
          routes.map(
            (route) =>
              route["@id"] && (
                <tr key={route["@id"]}>
                  <th scope="row">
                    <ReferenceLinks
                      items={{
                        href: getPath(route["@id"], "/routes/[id]"),
                        name: route["@id"],
                      }}
                    />
                  </th>
                  <td>
                    <Link href={getPath(route["@id"], "/routes/[id]")}>
                      <a>
                        <i className="bi bi-search" aria-hidden="true"></i>
                        <span className="sr-only">Show</span>
                      </a>
                    </Link>
                  </td>
                  <td>
                    <Link href={getPath(route["@id"], "/routes/[id]/edit")}>
                      <a>
                        <i className="bi bi-pen" aria-hidden="true" />
                        <span className="sr-only">Edit</span>
                      </a>
                    </Link>
                  </td>
                </tr>
              )
          )}
      </tbody>
    </table>
  </div>
);
