import { FunctionComponent, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { ErrorMessage, Formik } from "formik";
import { useMutation } from "react-query";

import { fetch, FetchError, FetchResponse } from "../../utils/dataAccess";
import { Product } from "../../types/Product";

interface Props {
  product?: Product;
}

interface SaveParams {
  values: Product;
}

interface DeleteParams {
  id: string;
}

const saveProduct = async ({ values }: SaveParams) =>
  await fetch<Product>(!values["@id"] ? "/products" : values["@id"], {
    method: !values["@id"] ? "POST" : "PUT",
    body: JSON.stringify(values),
  });

const deleteProduct = async (id: string) =>
  await fetch<Product>(id, { method: "DELETE" });

export const Form: FunctionComponent<Props> = ({ product }) => {
  const [, setError] = useState<string | null>(null);
  const router = useRouter();

  const saveMutation = useMutation<
    FetchResponse<Product> | undefined,
    Error | FetchError,
    SaveParams
  >((saveParams) => saveProduct(saveParams));

  const deleteMutation = useMutation<
    FetchResponse<Product> | undefined,
    Error | FetchError,
    DeleteParams
  >(({ id }) => deleteProduct(id), {
    onSuccess: () => {
      router.push("/products");
    },
    onError: (error) => {
      setError(`Error when deleting the resource: ${error}`);
      console.error(error);
    },
  });

  const handleDelete = () => {
    if (!product || !product["@id"]) return;
    if (!window.confirm("Are you sure you want to delete this item?")) return;
    deleteMutation.mutate({ id: product["@id"] });
  };

  return (
    <div>
      <h1>{product ? `Edit Product ${product["@id"]}` : `Create Product`}</h1>
      <Formik
        initialValues={
          product
            ? {
                ...product,
              }
            : new Product()
        }
        validate={() => {
          const errors = {};
          // add your validation logic here
          return errors;
        }}
        onSubmit={(values, { setSubmitting, setStatus, setErrors }) => {
          const isCreation = !values["@id"];
          saveMutation.mutate(
            { values },
            {
              onSuccess: () => {
                setStatus({
                  isValid: true,
                  msg: `Element ${isCreation ? "created" : "updated"}.`,
                });
                router.push("/products");
              },
              onError: (error) => {
                setStatus({
                  isValid: false,
                  msg: `${error.message}`,
                });
                if ("fields" in error) {
                  setErrors(error.fields);
                }
              },
              onSettled: () => {
                setSubmitting(false);
              },
            }
          );
        }}
      >
        {({
          values,
          status,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
          <form onSubmit={handleSubmit}>
            {status && status.msg && (
              <div
                className={`alert ${
                  status.isValid ? "alert-success" : "alert-danger"
                }`}
                role="alert"
              >
                {status.msg}
              </div>
            )}
            <button
              type="submit"
              className="btn btn-success"
              disabled={isSubmitting}
            >
              Submit
            </button>
          </form>
        )}
      </Formik>
      <Link href="/products">
        <a className="btn btn-primary">Back to list</a>
      </Link>
      {product && (
        <button className="btn btn-danger" onClick={handleDelete}>
          <a>Delete</a>
        </button>
      )}
    </div>
  );
};
