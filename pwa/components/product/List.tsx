import { FunctionComponent } from "react";
import Link from "next/link";

import ReferenceLinks from "../common/ReferenceLinks";
import { getPath } from "../../utils/dataAccess";
import { Product } from "../../types/Product";

interface Props {
  products: Product[];
}

export const List: FunctionComponent<Props> = ({ products }) => (
  <div>
    <h1>Product List</h1>
    <Link href="/products/create">
      <a className="btn btn-primary">Create</a>
    </Link>
    <table className="table table-responsive table-striped table-hover">
      <thead>
        <tr>
          <th>id</th>
          <th />
        </tr>
      </thead>
      <tbody>
        {products &&
          products.length !== 0 &&
          products.map(
            (product) =>
              product["@id"] && (
                <tr key={product["@id"]}>
                  <th scope="row">
                    <ReferenceLinks
                      items={{
                        href: getPath(product["@id"], "/products/[id]"),
                        name: product["@id"],
                      }}
                    />
                  </th>
                  <td>
                    <Link href={getPath(product["@id"], "/products/[id]")}>
                      <a>
                        <i className="bi bi-search" aria-hidden="true"></i>
                        <span className="sr-only">Show</span>
                      </a>
                    </Link>
                  </td>
                  <td>
                    <Link href={getPath(product["@id"], "/products/[id]/edit")}>
                      <a>
                        <i className="bi bi-pen" aria-hidden="true" />
                        <span className="sr-only">Edit</span>
                      </a>
                    </Link>
                  </td>
                </tr>
              )
          )}
      </tbody>
    </table>
  </div>
);
