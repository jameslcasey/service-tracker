import { FunctionComponent, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { ErrorMessage, Formik } from "formik";
import { useMutation } from "react-query";

import { fetch, FetchError, FetchResponse } from "../../utils/dataAccess";
import { SystemUserRole } from "../../types/SystemUserRole";

interface Props {
  systemuserrole?: SystemUserRole;
}

interface SaveParams {
  values: SystemUserRole;
}

interface DeleteParams {
  id: string;
}

const saveSystemUserRole = async ({ values }: SaveParams) =>
  await fetch<SystemUserRole>(
    !values["@id"] ? "/system_user_roles" : values["@id"],
    {
      method: !values["@id"] ? "POST" : "PUT",
      body: JSON.stringify(values),
    }
  );

const deleteSystemUserRole = async (id: string) =>
  await fetch<SystemUserRole>(id, { method: "DELETE" });

export const Form: FunctionComponent<Props> = ({ systemuserrole }) => {
  const [, setError] = useState<string | null>(null);
  const router = useRouter();

  const saveMutation = useMutation<
    FetchResponse<SystemUserRole> | undefined,
    Error | FetchError,
    SaveParams
  >((saveParams) => saveSystemUserRole(saveParams));

  const deleteMutation = useMutation<
    FetchResponse<SystemUserRole> | undefined,
    Error | FetchError,
    DeleteParams
  >(({ id }) => deleteSystemUserRole(id), {
    onSuccess: () => {
      router.push("/systemuserroles");
    },
    onError: (error) => {
      setError(`Error when deleting the resource: ${error}`);
      console.error(error);
    },
  });

  const handleDelete = () => {
    if (!systemuserrole || !systemuserrole["@id"]) return;
    if (!window.confirm("Are you sure you want to delete this item?")) return;
    deleteMutation.mutate({ id: systemuserrole["@id"] });
  };

  return (
    <div>
      <h1>
        {systemuserrole
          ? `Edit SystemUserRole ${systemuserrole["@id"]}`
          : `Create SystemUserRole`}
      </h1>
      <Formik
        initialValues={
          systemuserrole
            ? {
                ...systemuserrole,
              }
            : new SystemUserRole()
        }
        validate={() => {
          const errors = {};
          // add your validation logic here
          return errors;
        }}
        onSubmit={(values, { setSubmitting, setStatus, setErrors }) => {
          const isCreation = !values["@id"];
          saveMutation.mutate(
            { values },
            {
              onSuccess: () => {
                setStatus({
                  isValid: true,
                  msg: `Element ${isCreation ? "created" : "updated"}.`,
                });
                router.push("/system_user_roles");
              },
              onError: (error) => {
                setStatus({
                  isValid: false,
                  msg: `${error.message}`,
                });
                if ("fields" in error) {
                  setErrors(error.fields);
                }
              },
              onSettled: () => {
                setSubmitting(false);
              },
            }
          );
        }}
      >
        {({
          values,
          status,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
          <form onSubmit={handleSubmit}>
            {status && status.msg && (
              <div
                className={`alert ${
                  status.isValid ? "alert-success" : "alert-danger"
                }`}
                role="alert"
              >
                {status.msg}
              </div>
            )}
            <button
              type="submit"
              className="btn btn-success"
              disabled={isSubmitting}
            >
              Submit
            </button>
          </form>
        )}
      </Formik>
      <Link href="/systemuserroles">
        <a className="btn btn-primary">Back to list</a>
      </Link>
      {systemuserrole && (
        <button className="btn btn-danger" onClick={handleDelete}>
          <a>Delete</a>
        </button>
      )}
    </div>
  );
};
