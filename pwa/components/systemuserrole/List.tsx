import { FunctionComponent } from "react";
import Link from "next/link";

import ReferenceLinks from "../common/ReferenceLinks";
import { getPath } from "../../utils/dataAccess";
import { SystemUserRole } from "../../types/SystemUserRole";

interface Props {
  systemuserroles: SystemUserRole[];
}

export const List: FunctionComponent<Props> = ({ systemuserroles }) => (
  <div>
    <h1>SystemUserRole List</h1>
    <Link href="/systemuserroles/create">
      <a className="btn btn-primary">Create</a>
    </Link>
    <table className="table table-responsive table-striped table-hover">
      <thead>
        <tr>
          <th>id</th>
          <th />
        </tr>
      </thead>
      <tbody>
        {systemuserroles &&
          systemuserroles.length !== 0 &&
          systemuserroles.map(
            (systemuserrole) =>
              systemuserrole["@id"] && (
                <tr key={systemuserrole["@id"]}>
                  <th scope="row">
                    <ReferenceLinks
                      items={{
                        href: getPath(
                          systemuserrole["@id"],
                          "/systemuserroles/[id]"
                        ),
                        name: systemuserrole["@id"],
                      }}
                    />
                  </th>
                  <td>
                    <Link
                      href={getPath(
                        systemuserrole["@id"],
                        "/systemuserroles/[id]"
                      )}
                    >
                      <a>
                        <i className="bi bi-search" aria-hidden="true"></i>
                        <span className="sr-only">Show</span>
                      </a>
                    </Link>
                  </td>
                  <td>
                    <Link
                      href={getPath(
                        systemuserrole["@id"],
                        "/systemuserroles/[id]/edit"
                      )}
                    >
                      <a>
                        <i className="bi bi-pen" aria-hidden="true" />
                        <span className="sr-only">Edit</span>
                      </a>
                    </Link>
                  </td>
                </tr>
              )
          )}
      </tbody>
    </table>
  </div>
);
