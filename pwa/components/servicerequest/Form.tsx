import { FunctionComponent, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { ErrorMessage, Formik } from "formik";
import { useMutation } from "react-query";

import { fetch, FetchError, FetchResponse } from "../../utils/dataAccess";
import { ServiceRequest } from "../../types/ServiceRequest";

interface Props {
  servicerequest?: ServiceRequest;
}

interface SaveParams {
  values: ServiceRequest;
}

interface DeleteParams {
  id: string;
}

const saveServiceRequest = async ({ values }: SaveParams) =>
  await fetch<ServiceRequest>(
    !values["@id"] ? "/service_requests" : values["@id"],
    {
      method: !values["@id"] ? "POST" : "PUT",
      body: JSON.stringify(values),
    }
  );

const deleteServiceRequest = async (id: string) =>
  await fetch<ServiceRequest>(id, { method: "DELETE" });

export const Form: FunctionComponent<Props> = ({ servicerequest }) => {
  const [, setError] = useState<string | null>(null);
  const router = useRouter();

  const saveMutation = useMutation<
    FetchResponse<ServiceRequest> | undefined,
    Error | FetchError,
    SaveParams
  >((saveParams) => saveServiceRequest(saveParams));

  const deleteMutation = useMutation<
    FetchResponse<ServiceRequest> | undefined,
    Error | FetchError,
    DeleteParams
  >(({ id }) => deleteServiceRequest(id), {
    onSuccess: () => {
      router.push("/servicerequests");
    },
    onError: (error) => {
      setError(`Error when deleting the resource: ${error}`);
      console.error(error);
    },
  });

  const handleDelete = () => {
    if (!servicerequest || !servicerequest["@id"]) return;
    if (!window.confirm("Are you sure you want to delete this item?")) return;
    deleteMutation.mutate({ id: servicerequest["@id"] });
  };

  return (
    <div>
      <h1>
        {servicerequest
          ? `Edit ServiceRequest ${servicerequest["@id"]}`
          : `Create ServiceRequest`}
      </h1>
      <Formik
        initialValues={
          servicerequest
            ? {
                ...servicerequest,
              }
            : new ServiceRequest()
        }
        validate={() => {
          const errors = {};
          // add your validation logic here
          return errors;
        }}
        onSubmit={(values, { setSubmitting, setStatus, setErrors }) => {
          const isCreation = !values["@id"];
          saveMutation.mutate(
            { values },
            {
              onSuccess: () => {
                setStatus({
                  isValid: true,
                  msg: `Element ${isCreation ? "created" : "updated"}.`,
                });
                router.push("/service_requests");
              },
              onError: (error) => {
                setStatus({
                  isValid: false,
                  msg: `${error.message}`,
                });
                if ("fields" in error) {
                  setErrors(error.fields);
                }
              },
              onSettled: () => {
                setSubmitting(false);
              },
            }
          );
        }}
      >
        {({
          values,
          status,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
          <form onSubmit={handleSubmit}>
            {status && status.msg && (
              <div
                className={`alert ${
                  status.isValid ? "alert-success" : "alert-danger"
                }`}
                role="alert"
              >
                {status.msg}
              </div>
            )}
            <button
              type="submit"
              className="btn btn-success"
              disabled={isSubmitting}
            >
              Submit
            </button>
          </form>
        )}
      </Formik>
      <Link href="/servicerequests">
        <a className="btn btn-primary">Back to list</a>
      </Link>
      {servicerequest && (
        <button className="btn btn-danger" onClick={handleDelete}>
          <a>Delete</a>
        </button>
      )}
    </div>
  );
};
