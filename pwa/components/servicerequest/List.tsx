import { FunctionComponent } from "react";
import Link from "next/link";

import ReferenceLinks from "../common/ReferenceLinks";
import { getPath } from "../../utils/dataAccess";
import { ServiceRequest } from "../../types/ServiceRequest";

interface Props {
  servicerequests: ServiceRequest[];
}

export const List: FunctionComponent<Props> = ({ servicerequests }) => (
  <div>
    <h1>ServiceRequest List</h1>
    <Link href="/servicerequests/create">
      <a className="btn btn-primary">Create</a>
    </Link>
    <table className="table table-responsive table-striped table-hover">
      <thead>
        <tr>
          <th>id</th>
          <th />
        </tr>
      </thead>
      <tbody>
        {servicerequests &&
          servicerequests.length !== 0 &&
          servicerequests.map(
            (servicerequest) =>
              servicerequest["@id"] && (
                <tr key={servicerequest["@id"]}>
                  <th scope="row">
                    <ReferenceLinks
                      items={{
                        href: getPath(
                          servicerequest["@id"],
                          "/servicerequests/[id]"
                        ),
                        name: servicerequest["@id"],
                      }}
                    />
                  </th>
                  <td>
                    <Link
                      href={getPath(
                        servicerequest["@id"],
                        "/servicerequests/[id]"
                      )}
                    >
                      <a>
                        <i className="bi bi-search" aria-hidden="true"></i>
                        <span className="sr-only">Show</span>
                      </a>
                    </Link>
                  </td>
                  <td>
                    <Link
                      href={getPath(
                        servicerequest["@id"],
                        "/servicerequests/[id]/edit"
                      )}
                    >
                      <a>
                        <i className="bi bi-pen" aria-hidden="true" />
                        <span className="sr-only">Edit</span>
                      </a>
                    </Link>
                  </td>
                </tr>
              )
          )}
      </tbody>
    </table>
  </div>
);
