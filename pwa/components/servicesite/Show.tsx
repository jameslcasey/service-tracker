import { FunctionComponent, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import Head from "next/head";

import { fetch, getPath } from "../../utils/dataAccess";
import { ServiceSite } from "../../types/ServiceSite";

interface Props {
  servicesite: ServiceSite;
  text: string;
}

export const Show: FunctionComponent<Props> = ({ servicesite, text }) => {
  const [error, setError] = useState<string | null>(null);
  const router = useRouter();

  const handleDelete = async () => {
    if (!servicesite["@id"]) return;
    if (!window.confirm("Are you sure you want to delete this item?")) return;

    try {
      await fetch(servicesite["@id"], { method: "DELETE" });
      router.push("/servicesites");
    } catch (error) {
      setError("Error when deleting the resource.");
      console.error(error);
    }
  };

  return (
    <div>
      <Head>
        <title>{`Show ServiceSite ${servicesite["@id"]}`}</title>
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{ __html: text }}
        />
      </Head>
      <h1>{`Show ServiceSite ${servicesite["@id"]}`}</h1>
      <table className="table table-responsive table-striped table-hover">
        <thead>
          <tr>
            <th>Field</th>
            <th>Value</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">name</th>
            <td>{servicesite["name"]}</td>
          </tr>
          <tr>
            <th scope="row">dateCreated</th>
            <td>{servicesite["dateCreated"]?.toLocaleString()}</td>
          </tr>
          <tr>
            <th scope="row">dateUpdated</th>
            <td>{servicesite["dateUpdated"]?.toLocaleString()}</td>
          </tr>
        </tbody>
      </table>
      {error && (
        <div className="alert alert-danger" role="alert">
          {error}
        </div>
      )}
      <Link href="/servicesites">
        <a className="btn btn-primary">Back to list</a>
      </Link>{" "}
      <Link href={getPath(servicesite["@id"], "/servicesites/[id]/edit")}>
        <a className="btn btn-warning">Edit</a>
      </Link>
      <button className="btn btn-danger" onClick={handleDelete}>
        Delete
      </button>
    </div>
  );
};
