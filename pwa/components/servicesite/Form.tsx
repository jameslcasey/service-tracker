import { FunctionComponent, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { ErrorMessage, Formik } from "formik";
import { useMutation } from "react-query";

import { fetch, FetchError, FetchResponse } from "../../utils/dataAccess";
import { ServiceSite } from "../../types/ServiceSite";

interface Props {
  servicesite?: ServiceSite;
}

interface SaveParams {
  values: ServiceSite;
}

interface DeleteParams {
  id: string;
}

const saveServiceSite = async ({ values }: SaveParams) =>
  await fetch<ServiceSite>(!values["@id"] ? "/service_sites" : values["@id"], {
    method: !values["@id"] ? "POST" : "PUT",
    body: JSON.stringify(values),
  });

const deleteServiceSite = async (id: string) =>
  await fetch<ServiceSite>(id, { method: "DELETE" });

export const Form: FunctionComponent<Props> = ({ servicesite }) => {
  const [, setError] = useState<string | null>(null);
  const router = useRouter();

  const saveMutation = useMutation<
    FetchResponse<ServiceSite> | undefined,
    Error | FetchError,
    SaveParams
  >((saveParams) => saveServiceSite(saveParams));

  const deleteMutation = useMutation<
    FetchResponse<ServiceSite> | undefined,
    Error | FetchError,
    DeleteParams
  >(({ id }) => deleteServiceSite(id), {
    onSuccess: () => {
      router.push("/servicesites");
    },
    onError: (error) => {
      setError(`Error when deleting the resource: ${error}`);
      console.error(error);
    },
  });

  const handleDelete = () => {
    if (!servicesite || !servicesite["@id"]) return;
    if (!window.confirm("Are you sure you want to delete this item?")) return;
    deleteMutation.mutate({ id: servicesite["@id"] });
  };

  return (
    <div>
      <h1>
        {servicesite
          ? `Edit ServiceSite ${servicesite["@id"]}`
          : `Create ServiceSite`}
      </h1>
      <Formik
        initialValues={
          servicesite
            ? {
                ...servicesite,
              }
            : new ServiceSite()
        }
        validate={() => {
          const errors = {};
          // add your validation logic here
          return errors;
        }}
        onSubmit={(values, { setSubmitting, setStatus, setErrors }) => {
          const isCreation = !values["@id"];
          saveMutation.mutate(
            { values },
            {
              onSuccess: () => {
                setStatus({
                  isValid: true,
                  msg: `Element ${isCreation ? "created" : "updated"}.`,
                });
                router.push("/service_sites");
              },
              onError: (error) => {
                setStatus({
                  isValid: false,
                  msg: `${error.message}`,
                });
                if ("fields" in error) {
                  setErrors(error.fields);
                }
              },
              onSettled: () => {
                setSubmitting(false);
              },
            }
          );
        }}
      >
        {({
          values,
          status,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
          <form onSubmit={handleSubmit}>
            <div className="form-group">
              <label className="form-control-label" htmlFor="servicesite_name">
                name
              </label>
              <input
                name="name"
                id="servicesite_name"
                value={values.name ?? ""}
                type="text"
                placeholder=""
                className={`form-control${
                  errors.name && touched.name ? " is-invalid" : ""
                }`}
                aria-invalid={errors.name && touched.name ? "true" : undefined}
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <ErrorMessage
                className="invalid-feedback"
                component="div"
                name="name"
              />
            </div>
            <div className="form-group">
              <label
                className="form-control-label"
                htmlFor="servicesite_dateCreated"
              >
                dateCreated
              </label>
              <input
                name="dateCreated"
                id="servicesite_dateCreated"
                value={values.dateCreated?.toLocaleString() ?? ""}
                type="dateTime"
                placeholder=""
                className={`form-control${
                  errors.dateCreated && touched.dateCreated ? " is-invalid" : ""
                }`}
                aria-invalid={
                  errors.dateCreated && touched.dateCreated ? "true" : undefined
                }
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <ErrorMessage
                className="invalid-feedback"
                component="div"
                name="dateCreated"
              />
            </div>
            <div className="form-group">
              <label
                className="form-control-label"
                htmlFor="servicesite_dateUpdated"
              >
                dateUpdated
              </label>
              <input
                name="dateUpdated"
                id="servicesite_dateUpdated"
                value={values.dateUpdated?.toLocaleString() ?? ""}
                type="dateTime"
                placeholder=""
                className={`form-control${
                  errors.dateUpdated && touched.dateUpdated ? " is-invalid" : ""
                }`}
                aria-invalid={
                  errors.dateUpdated && touched.dateUpdated ? "true" : undefined
                }
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <ErrorMessage
                className="invalid-feedback"
                component="div"
                name="dateUpdated"
              />
            </div>
            {status && status.msg && (
              <div
                className={`alert ${
                  status.isValid ? "alert-success" : "alert-danger"
                }`}
                role="alert"
              >
                {status.msg}
              </div>
            )}
            <button
              type="submit"
              className="btn btn-success"
              disabled={isSubmitting}
            >
              Submit
            </button>
          </form>
        )}
      </Formik>
      <Link href="/servicesites">
        <a className="btn btn-primary">Back to list</a>
      </Link>
      {servicesite && (
        <button className="btn btn-danger" onClick={handleDelete}>
          <a>Delete</a>
        </button>
      )}
    </div>
  );
};
