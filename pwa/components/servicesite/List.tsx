import { FunctionComponent } from "react";
import Link from "next/link";

import ReferenceLinks from "../common/ReferenceLinks";
import { getPath } from "../../utils/dataAccess";
import { ServiceSite } from "../../types/ServiceSite";

interface Props {
  servicesites: ServiceSite[];
}

export const List: FunctionComponent<Props> = ({ servicesites }) => (
  <div>
    <h1>ServiceSite List</h1>
    <Link href="/servicesites/create">
      <a className="btn btn-primary">Create</a>
    </Link>
    <table className="table table-responsive table-striped table-hover">
      <thead>
        <tr>
          <th>id</th>
          <th>name</th>
          <th>dateCreated</th>
          <th>dateUpdated</th>
          <th />
        </tr>
      </thead>
      <tbody>
        {servicesites &&
          servicesites.length !== 0 &&
          servicesites.map(
            (servicesite) =>
              servicesite["@id"] && (
                <tr key={servicesite["@id"]}>
                  <th scope="row">
                    <ReferenceLinks
                      items={{
                        href: getPath(servicesite["@id"], "/servicesites/[id]"),
                        name: servicesite["@id"],
                      }}
                    />
                  </th>
                  <td>{servicesite["name"]}</td>
                  <td>{servicesite["dateCreated"]?.toLocaleString()}</td>
                  <td>{servicesite["dateUpdated"]?.toLocaleString()}</td>
                  <td>
                    <Link
                      href={getPath(servicesite["@id"], "/servicesites/[id]")}
                    >
                      <a>
                        <i className="bi bi-search" aria-hidden="true"></i>
                        <span className="sr-only">Show</span>
                      </a>
                    </Link>
                  </td>
                  <td>
                    <Link
                      href={getPath(
                        servicesite["@id"],
                        "/servicesites/[id]/edit"
                      )}
                    >
                      <a>
                        <i className="bi bi-pen" aria-hidden="true" />
                        <span className="sr-only">Edit</span>
                      </a>
                    </Link>
                  </td>
                </tr>
              )
          )}
      </tbody>
    </table>
  </div>
);
