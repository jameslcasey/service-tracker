import { FunctionComponent } from "react";
import Link from "next/link";

import ReferenceLinks from "../common/ReferenceLinks";
import { getPath } from "../../utils/dataAccess";
import { ServiceTicketStatus } from "../../types/ServiceTicketStatus";

interface Props {
  serviceticketstatuss: ServiceTicketStatus[];
}

export const List: FunctionComponent<Props> = ({ serviceticketstatuss }) => (
  <div>
    <h1>ServiceTicketStatus List</h1>
    <Link href="/serviceticketstatuss/create">
      <a className="btn btn-primary">Create</a>
    </Link>
    <table className="table table-responsive table-striped table-hover">
      <thead>
        <tr>
          <th>id</th>
          <th />
        </tr>
      </thead>
      <tbody>
        {serviceticketstatuss &&
          serviceticketstatuss.length !== 0 &&
          serviceticketstatuss.map(
            (serviceticketstatus) =>
              serviceticketstatus["@id"] && (
                <tr key={serviceticketstatus["@id"]}>
                  <th scope="row">
                    <ReferenceLinks
                      items={{
                        href: getPath(
                          serviceticketstatus["@id"],
                          "/serviceticketstatuss/[id]"
                        ),
                        name: serviceticketstatus["@id"],
                      }}
                    />
                  </th>
                  <td>
                    <Link
                      href={getPath(
                        serviceticketstatus["@id"],
                        "/serviceticketstatuss/[id]"
                      )}
                    >
                      <a>
                        <i className="bi bi-search" aria-hidden="true"></i>
                        <span className="sr-only">Show</span>
                      </a>
                    </Link>
                  </td>
                  <td>
                    <Link
                      href={getPath(
                        serviceticketstatus["@id"],
                        "/serviceticketstatuss/[id]/edit"
                      )}
                    >
                      <a>
                        <i className="bi bi-pen" aria-hidden="true" />
                        <span className="sr-only">Edit</span>
                      </a>
                    </Link>
                  </td>
                </tr>
              )
          )}
      </tbody>
    </table>
  </div>
);
