<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use App\Entity\Enum\AccountStatus;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
#[ApiResource(
    normalizationContext: ['groups' => ['account:read']],
    denormalizationContext: ['groups' => ['account:write']],
)]
class Account
{
    #[ORM\Id, ORM\Column, ORM\GeneratedValue]
    #[Groups(['account:read'])]
    private ?int $id = null;

    #[ORM\Column]
    #[Assert\NotBlank]
    #[Groups(['account:read', 'account:write'])]
    private ?string $name = null;

    #[ORM\JoinColumn, ORM\ManyToOne(targetEntity: AccountStatus::class)]
    #[Groups(['account:read', 'account:write'])]
    #[ApiProperty(push: true)]
    private ?AccountStatus $accountStatus = null;

    #[ORM\OneToMany(mappedBy: "account", targetEntity: ServiceSite::class)]
    #[Groups(['account:read', 'account:write'])]
    #[ApiProperty(push: true)]
    private ?Collection $serviceSites = null;

    #[ORM\Column]
    #[Groups(['account:read'])]
    private ?DateTimeImmutable $dateCreated = null;

    #[ORM\Column]
    #[Groups(['account:read'])]
    private ?DateTimeImmutable $dateUpdated = null;

    public function __construct()
    {
        $this->serviceSites = new ArrayCollection();
        $this->dateCreated = new DateTimeImmutable();
        $this->dateUpdated = new DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): Account
    {
        $this->name = $name;

        return $this;
    }

    public function getAccountStatus(): ?AccountStatus
    {
        return $this->accountStatus;
    }

    public function setAccountStatus(?AccountStatus $accountStatus): Account
    {
        $this->accountStatus = $accountStatus;

        return $this;
    }

    public function getServiceSites(): ?Collection
    {
        return $this->serviceSites;
    }

    public function setServiceSites(?Collection $serviceSites): Account
    {
        $this->serviceSites = $serviceSites;

        return $this;
    }

    public function addServiceSite(ServiceSite $serviceSite): Account
    {
        if ($this->serviceSites->contains($serviceSite)) {
            return $this;
        }

        $this->serviceSites->add($serviceSite);
        $serviceSite->setAccount($this);

        return $this;
    }

    public function removeServiceSite(ServiceSite $serviceSite): Account
    {
        if ($this->serviceSites->contains($serviceSite)) {
            $this->serviceSites->removeElement($serviceSite);
            $serviceSite->setAccount(null);
        }

        return $this;
    }

    public function getDateCreated(): ?DateTimeImmutable
    {
        return $this->dateCreated;
    }

    public function setDateCreated(?DateTimeImmutable $dateCreated): self
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    public function getDateUpdated(): ?DateTimeImmutable
    {
        return $this->dateUpdated;
    }

    public function setDateUpdated(?DateTimeImmutable $dateUpdated): Account
    {
        $this->dateUpdated = $dateUpdated;

        return $this;
    }

}
