<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
#[ApiResource(
    normalizationContext: ['groups' => ['service_schedule:read']],
    denormalizationContext: ['groups' => ['service_schedule:write']],
)]
class ServiceSchedule
{
    #[ORM\Id, ORM\Column, ORM\GeneratedValue]
    private ?int $id = null;

    #[ORM\JoinColumn, ORM\ManyToOne(targetEntity: ServiceSite::class)]
    #[Groups(['service_schedule:read', 'service_schedule:write'])]
    #[ApiProperty(push: true)]
    private ?ServiceSite $serviceSite = null;

    #[ORM\JoinColumn, ORM\ManyToOne(targetEntity: Route::class)]
    #[Groups(['service_schedule:read', 'service_schedule:write'])]
    #[ApiProperty(push: true)]
    private ?Route $route = null;

    #[ORM\JoinColumn, ORM\ManyToOne(targetEntity: ScheduleData::class)]
    #[Groups(['service_schedule:read', 'service_schedule:write'])]
    #[ApiProperty(push: true)]
    private ?ScheduleData $scheduleData = null;

    #[ORM\OneToMany(mappedBy: 'serviceSchedule', targetEntity: ScheduleData::class)]
    #[Groups(['service_schedule:read', 'service_schedule:write'])]
    #[ApiProperty(push: true)]
    private ?ArrayCollection $items = null;

    #[ORM\Column]
    #[Groups(['service_schedule:read'])]
    private ?DateTimeImmutable $dateCreated = null;

    #[ORM\Column]
    #[Groups(['service_schedule:read'])]
    private ?DateTimeImmutable $dateUpdated = null;

    public function __construct()
    {
        $this->dateCreated = new DateTimeImmutable();
        $this->dateUpdated = new DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getServiceSite(): ?ServiceSite
    {
        return $this->serviceSite;
    }

    public function setServiceSite(?ServiceSite $serviceSite): ServiceSchedule
    {
        $this->serviceSite = $serviceSite;

        return $this;
    }

    public function getRoute(): ?Route
    {
        return $this->route;
    }

    public function setRoute(?Route $route): ServiceSchedule
    {
        $this->route = $route;

        return $this;
    }

    public function getScheduleData(): ?ScheduleData
    {
        return $this->scheduleData;
    }

    public function setScheduleData(?ScheduleData $scheduleData): ServiceSchedule
    {
        $this->scheduleData = $scheduleData;

        return $this;
    }

    public function getItems(): ?ArrayCollection
    {
        return $this->items;
    }

    public function setItems(?ArrayCollection $items): ServiceSchedule
    {
        $this->items = $items;

        return $this;
    }

    public function addItem(ServiceScheduleItem $serviceScheduleItem): ServiceSchedule
    {
        if ($this->items->contains($serviceScheduleItem)) {
            return $this;
        }

        $this->items->add($serviceScheduleItem);
        $serviceScheduleItem->setServiceSchedule($this);

        return $this;
    }

    public function removeItem(ServiceScheduleItem $serviceScheduleItem): ServiceSchedule
    {
        if ($this->items->contains($serviceScheduleItem)) {
            $this->items->removeElement($serviceScheduleItem);
            $serviceScheduleItem->setServiceSchedule(null);
        }
        
        return $this;
    }

    public function getDateCreated(): ?DateTimeImmutable
    {
        return $this->dateCreated;
    }

    public function setDateCreated(?DateTimeImmutable $dateCreated): self
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    public function getDateUpdated(): ?DateTimeImmutable
    {
        return $this->dateUpdated;
    }

    public function setDateUpdated(?DateTimeImmutable $dateUpdated): ServiceSchedule
    {
        $this->dateUpdated = $dateUpdated;

        return $this;
    }

}