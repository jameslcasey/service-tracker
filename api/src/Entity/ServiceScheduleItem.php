<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
#[ApiResource(
    normalizationContext: ['groups' => ['service_schedule_item:read']],
    denormalizationContext: ['groups' => ['service_schedule_item:write']],
)]
class ServiceScheduleItem
{
    #[ORM\Id, ORM\Column, ORM\GeneratedValue]
    private ?int $id = null;

    #[ORM\JoinColumn, ORM\ManyToOne(targetEntity: ServiceSchedule::class, inversedBy: 'items')]
    #[Groups(['service_schedule_item:read', 'service_schedule_item:write'])]
    #[ApiProperty(push: true)]
    private ?ServiceSchedule $serviceSchedule = null;

    #[ORM\JoinColumn, ORM\ManyToOne(targetEntity: Product::class)]
    #[Groups(['service_schedule_item:read', 'service_schedule_item:write'])]
    #[ApiProperty(push: true)]
    private ?Product $product = null;

    #[ORM\Column]
    #[Groups(['service_schedule_item:read', 'service_schedule_item:write'])]
    #[ApiProperty(push: true)]
    private ?int $quantity = null;

    #[ORM\Column]
    #[Groups(['service_schedule_item:read'])]
    private ?DateTimeImmutable $dateCreated = null;

    #[ORM\Column]
    #[Groups(['service_schedule_item:read'])]
    private ?DateTimeImmutable $dateUpdated = null;

    public function __construct()
    {
        $this->dateCreated = new DateTimeImmutable();
        $this->dateUpdated = new DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getServiceSchedule(): ?ServiceSchedule
    {
        return $this->serviceSchedule;
    }

    public function setServiceSchedule(?ServiceSchedule $serviceSchedule): ServiceScheduleItem
    {
        $this->serviceSchedule = $serviceSchedule;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): ServiceScheduleItem
    {
        $this->product = $product;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(?int $quantity): ServiceScheduleItem
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getDateCreated(): ?DateTimeImmutable
    {
        return $this->dateCreated;
    }

    public function setDateCreated(?DateTimeImmutable $dateCreated): self
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    public function getDateUpdated(): ?DateTimeImmutable
    {
        return $this->dateUpdated;
    }

    public function setDateUpdated(?DateTimeImmutable $dateUpdated): ServiceScheduleItem
    {
        $this->dateUpdated = $dateUpdated;

        return $this;
    }

}