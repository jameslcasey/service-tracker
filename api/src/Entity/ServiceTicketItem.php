<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
#[ApiResource(
    normalizationContext: ['groups' => ['service_ticket_item:read']],
    denormalizationContext: ['groups' => ['service_ticket_item:write']],
)]
class ServiceTicketItem
{
    #[ORM\Id, ORM\Column, ORM\GeneratedValue]
    private ?int $id = null;

    #[ORM\JoinColumn, ORM\ManyToOne(targetEntity: ServiceTicket::class)]
    #[Groups(['service_ticket_item:read', 'service_ticket_item:write'])]
    #[ApiProperty(push: true)]
    private ?ServiceTicket $serviceTicket = null;

    #[ORM\JoinColumn, ORM\ManyToOne(targetEntity: Product::class)]
    #[Groups(['service_ticket_item:read', 'service_ticket_item:write'])]
    #[ApiProperty(push: true)]
    private ?Product $product = null;

    #[ORM\Column]
    #[Groups(['service_ticket_item:read'])]
    private ?DateTimeImmutable $dateCreated = null;

    #[ORM\Column]
    #[Groups(['service_ticket_item:read'])]
    private ?DateTimeImmutable $dateUpdated = null;

    public function __construct()
    {
        $this->dateCreated = new DateTimeImmutable();
        $this->dateUpdated = new DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getServiceTicket(): ?ServiceTicket
    {
        return $this->serviceTicket;
    }

    public function setServiceTicket(?ServiceTicket $serviceTicket): ServiceTicketItem
    {
        $this->serviceTicket = $serviceTicket;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): ServiceTicketItem
    {
        $this->product = $product;

        return $this;
    }

    public function getDateCreated(): ?DateTimeImmutable
    {
        return $this->dateCreated;
    }

    public function setDateCreated(?DateTimeImmutable $dateCreated): self
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    public function getDateUpdated(): ?DateTimeImmutable
    {
        return $this->dateUpdated;
    }

    public function setDateUpdated(?DateTimeImmutable $dateUpdated): ServiceTicketItem
    {
        $this->dateUpdated = $dateUpdated;

        return $this;
    }

}