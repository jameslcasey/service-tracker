<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use App\Entity\Enum\ServiceTicketStatus;
use DateTimeImmutable;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
#[ApiResource(
    normalizationContext: ['groups' => ['service_ticket:read']],
    denormalizationContext: ['groups' => ['service_ticket:write']],
)]
class ServiceTicket
{
    #[ORM\Id, ORM\Column, ORM\GeneratedValue]
    private ?int $id = null;

    #[ORM\JoinColumn, ORM\ManyToOne(targetEntity: ServiceTicketStatus::class)]
    #[Groups(['service_ticket:read', 'service_ticket:write'])]
    #[ApiProperty(push: true)]
    private ?ServiceTicketStatus $serviceTicketStatus = null;

    #[ORM\JoinColumn, ORM\ManyToOne(targetEntity: Person::class)]
    #[Groups(['service_ticket:read', 'service_ticket:write'])]
    #[ApiProperty(push: true)]
    private ?Person $driver = null;

    #[ORM\JoinColumn, ORM\ManyToOne(targetEntity: ServiceSite::class, inversedBy: 'serviceSite')]
    #[Groups(['service_ticket:read', 'service_ticket:write'])]
    #[ApiProperty(push: true)]
    private ?ServiceSite $serviceSite = null;

    #[ORM\OneToMany(targetEntity: ServiceTicketItem::class, mappedBy: 'serviceTicket')]
    #[Groups(['service_ticket:read', 'service_ticket:write'])]
    #[ApiProperty(push: true)]
    private ?Collection $serviceTicketItems = null;

    #[ORM\Column]
    #[Groups(['service_ticket:read'])]
    private ?DateTimeImmutable $dateCreated = null;

    #[ORM\Column]
    #[Groups(['service_ticket:read'])]
    private ?DateTimeImmutable $dateUpdated = null;

    public function __construct()
    {
        $this->dateCreated = new DateTimeImmutable();
        $this->dateUpdated = new DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getServiceTicketStatus(): ?ServiceTicketStatus
    {
        return $this->serviceTicketStatus;
    }

    public function setServiceTicketStatus(?ServiceTicketStatus $serviceTicketStatus): ServiceTicket
    {
        $this->serviceTicketStatus = $serviceTicketStatus;

        return $this;
    }

    public function getDriver(): ?Person
    {
        return $this->driver;
    }

    public function setDriver(?Person $driver): ServiceTicket
    {
        $this->driver = $driver;

        return $this;
    }

    public function getServiceSite(): ?ServiceSite
    {
        return $this->serviceSite;
    }

    public function setServiceSite(?ServiceSite $serviceSite): ServiceTicket
    {
        $this->serviceSite = $serviceSite;

        return $this;
    }

    public function getServiceTicketItems(): ?Collection
    {
        return $this->serviceTicketItems;
    }

    public function setServiceTicketItems(?Collection $serviceTicketItems): ServiceTicket
    {
        $this->serviceTicketItems = $serviceTicketItems;

        return $this;
    }

    public function addServiceTicketItem(ServiceTicketItem $serviceTicketItem): ServiceTicket
    {
        if ($this->serviceTicketItems->contains($serviceTicketItem)) {
            return $this;
        }

        $this->serviceTicketItems->add($serviceTicketItem);
        $serviceTicketItem->setServiceTicket($this);

        return $this;
    }

    public function removeServiceTicketItem(ServiceTicketItem $serviceTicketItem): ServiceTicket
    {
        if ($this->serviceTicketItems->contains($serviceTicketItem)) {
            $this->serviceTicketItems->removeElement($serviceTicketItem);
            $serviceTicketItem->setServiceTicket(null);
        }

        return $this;
    }

    public function getDateCreated(): ?DateTimeImmutable
    {
        return $this->dateCreated;
    }

    public function setDateCreated(?DateTimeImmutable $dateCreated): self
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    public function getDateUpdated(): ?DateTimeImmutable
    {
        return $this->dateUpdated;
    }

    public function setDateUpdated(?DateTimeImmutable $dateUpdated): ServiceTicket
    {
        $this->dateUpdated = $dateUpdated;

        return $this;
    }

}
