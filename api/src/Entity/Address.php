<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use App\Entity\Enum\AddressStatus;
use App\Entity\Enum\AddressType;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
#[ApiResource(
    normalizationContext: ['groups' => ['address:read']],
    denormalizationContext: ['groups' => ['address:write']],
)]
class Address
{
    #[ORM\Id, ORM\Column, ORM\GeneratedValue]
    private ?int $id = null;

    #[ORM\JoinColumn, ORM\ManyToOne]
    #[Groups(['address:read', 'address:write'])]
    #[ApiProperty(push: true)]
    private ?AddressStatus $addressStatus = null;

    #[ORM\JoinColumn, ORM\ManyToOne]
    #[Groups(['address:read', 'address:write'])]
    #[ApiProperty(push: true)]
    private ?AddressType $addressType = null;

    #[ORM\Column]
    #[Groups(['address:read', 'address:write'])]
    private ?string $address1 = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['address:read', 'address:write'])]
    private ?string $address2 = null;

    #[ORM\Column]
    #[Groups(['address:read', 'address:write'])]
    private ?string $city = null;

    #[ORM\Column]
    #[Groups(['address:read', 'address:write'])]
    private ?string $state = null;

    #[ORM\Column]
    #[Groups(['address:read', 'address:write'])]
    private ?string $zip = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['address:read', 'address:write'])]
    private ?string $coordinates = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['address:read', 'address:write'])]
    private ?string $what3Words = null;

    #[ORM\Column]
    #[Groups(['address:read'])]
    private ?DateTimeImmutable $dateCreated = null;

    #[ORM\Column]
    #[Groups(['address:read'])]
    private ?DateTimeImmutable $dateUpdated = null;

    public function __construct()
    {
        $this->dateCreated = new DateTimeImmutable();
        $this->dateUpdated = new DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getAddressStatus(): ?AddressStatus
    {
        return $this->addressStatus;
    }

    public function setAddressStatus(?AddressStatus $addressStatus): Address
    {
        $this->addressStatus = $addressStatus;

        return $this;
    }

    public function getAddressType(): ?AddressType
    {
        return $this->addressType;
    }

    public function setAddressType(?AddressType $addressType): Address
    {
        $this->addressType = $addressType;

        return $this;
    }

    public function getAddress1(): ?string
    {
        return $this->address1;
    }

    public function setAddress1(?string $address1): Address
    {
        $this->address1 = $address1;

        return $this;
    }

    public function getAddress2(): ?string
    {
        return $this->address2;
    }

    public function setAddress2(?string $address2): Address
    {
        $this->address2 = $address2;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): Address
    {
        $this->city = $city;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(?string $state): Address
    {
        $this->state = $state;

        return $this;
    }

    public function getZip(): ?string
    {
        return $this->zip;
    }

    public function setZip(?string $zip): Address
    {
        $this->zip = $zip;

        return $this;
    }

    public function getCoordinates(): ?string
    {
        return $this->coordinates;
    }

    public function setCoordinates(?string $coordinates): Address
    {
        $this->coordinates = $coordinates;

        return $this;
    }

    public function getWhat3Words(): ?string
    {
        return $this->what3Words;
    }

    public function setWhat3Words(?string $what3Words): Address
    {
        $this->what3Words = $what3Words;

        return $this;
    }

    public function getDateCreated(): ?DateTimeImmutable
    {
        return $this->dateCreated;
    }

    public function setDateCreated(?DateTimeImmutable $dateCreated): self
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    public function getDateUpdated(): ?DateTimeImmutable
    {
        return $this->dateUpdated;
    }

    public function setDateUpdated(?DateTimeImmutable $dateUpdated): Address
    {
        $this->dateUpdated = $dateUpdated;

        return $this;
    }

}
