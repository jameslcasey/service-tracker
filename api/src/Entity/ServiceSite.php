<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use App\Entity\Enum\ServiceSiteStatus;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
#[ApiResource(
    normalizationContext: ['groups' => ['service_site:read']],
    denormalizationContext: ['groups' => ['service_site:write']],
)]
class ServiceSite
{
    #[ORM\Id, ORM\Column, ORM\GeneratedValue]
    private ?int $id = null;

    #[ORM\JoinColumn, ORM\ManyToOne(targetEntity: ServiceSiteStatus::class)]
    #[Groups(['service_site:read', 'service_site:write'])]
    #[ApiProperty(push: true)]
    private ?ServiceSiteStatus $serviceSiteStatus = null;

    #[ORM\Column]
    #[Groups(['service_site:read', 'service_site:write'])]
    private ?string $name = null;

    #[ORM\JoinColumn, ORM\ManyToOne(targetEntity: Account::class, inversedBy: 'serviceSites')]
    #[Groups(['service_site:read', 'service_site:write'])]
    private ?Account $account = null;

    #[ORM\JoinColumn, ORM\ManyToOne(targetEntity: Address::class)]
    #[Groups(['service_site:read', 'service_site:write'])]
    private ?Address $physicalAddress = null;

    #[ORM\JoinColumn, ORM\ManyToOne(targetEntity: Address::class)]
    #[Groups(['service_site:read', 'service_site:write'])]
    private ?Address $billingAddress = null;

    #[Groups(['service_site:read', 'service_site:write'])]
    #[ApiProperty(push: true)]
    private ?Collection $routes = null;

    #[ORM\OneToMany(targetEntity: ServiceTicket::class, mappedBy: 'serviceSite', cascade: ['persist', 'remove'])]
    #[Groups(['service_site:read', 'service_site:write'])]
    #[ApiProperty(push: true)]
    private ?Collection $serviceTickets = null;

    #[ORM\Column]
    #[Groups(['service_site:read'])]
    private ?DateTimeImmutable $dateCreated = null;

    #[ORM\Column]
    #[Groups(['service_site:read'])]
    private ?DateTimeImmutable $dateUpdated = null;

    public function __construct()
    {
        $this->dateCreated = new DateTimeImmutable();
        $this->dateUpdated = new DateTimeImmutable();
        $this->routes = new ArrayCollection();
        $this->serviceTickets = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getAccount(): ?Account
    {
        return $this->account;
    }

    public function setAccount(?Account $account): ServiceSite
    {
        $this->account = $account;

        return $this;
    }

    public function getServiceSiteStatus(): ?ServiceSiteStatus
    {
        return $this->serviceSiteStatus;
    }

    public function setServiceSiteStatus(?ServiceSiteStatus $serviceSiteStatus): ServiceSite
    {
        $this->serviceSiteStatus = $serviceSiteStatus;

        return $this;
    }

    public function getPhysicalAddress(): ?Address
    {
        return $this->physicalAddress;
    }

    public function setPhysicalAddress(?Address $physicalAddress): ServiceSite
    {
        $this->physicalAddress = $physicalAddress;

        return $this;
    }

    public function getBillingAddress(): ?Address
    {
        return $this->billingAddress;
    }

    public function setBillingAddress(?Address $billingAddress): ServiceSite
    {
        $this->billingAddress = $billingAddress;

        return $this;
    }

    public function getRoutes(): ?Collection
    {
        return $this->routes;
    }

    public function setRoutes(?Collection $routes): ServiceSite
    {
        $this->routes = $routes;

        return $this;
    }

    public function addRoute(Route $route): ServiceSite
    {
        if ($this->routes->contains($route)) {
            return $this;
        }

        $this->routes->add($route);
        $route->addServiceSite($this);

        return $this;
    }

    public function removeRoute(Route $route): ServiceSite
    {
        if ($this->routes->contains($route)) {
            $this->routes->removeElement($route);
            $route->removeServiceSite($this);
        }

        return $this;
    }

    public function getServiceTickets(): ?Collection
    {
        return $this->serviceTickets;
    }

    public function setServiceTickets(?Collection $serviceTickets): ServiceSite
    {
        $this->serviceTickets = $serviceTickets;

        return $this;
    }

    public function addServiceTicket(ServiceTicket $serviceTicket): ServiceSite
    {
        if ($this->serviceTickets->contains($serviceTicket)) {
            return $this;
        }

        $this->serviceTickets->add($serviceTicket);
        $serviceTicket->setServiceSite($this);

        return $this;
    }

    public function removeServiceTicket(ServiceTicket $serviceTicket): ServiceSite
    {
        if ($this->serviceTickets->contains($serviceTicket)) {
            $this->serviceTickets->removeElement($serviceTicket);
            $serviceTicket->setServiceSite(null);
        }

        return $this;
    }

    public function getDateCreated(): ?DateTimeImmutable
    {
        return $this->dateCreated;
    }

    public function setDateCreated(?DateTimeImmutable $dateCreated): self
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    public function getDateUpdated(): ?DateTimeImmutable
    {
        return $this->dateUpdated;
    }

    public function setDateUpdated(?DateTimeImmutable $dateUpdated): void
    {
        $this->dateUpdated = $dateUpdated;
    }

}
