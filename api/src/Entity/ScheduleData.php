<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use DateTime;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;


#[ORM\Entity]
#[ApiResource(
    normalizationContext: ['groups' => ['schedule_data:read']],
    denormalizationContext: ['groups' => ['schedule_data:write']],
)]
class ScheduleData
{
    public const FREQUENCY_MIN = 0;
    public const FREQUENCY_MAX = 4;

    public const SUNDAY = 0;
    public const MONDAY = 1;
    public const TUESDAY = 2;
    public const WEDNESDAY = 3;
    public const THURSDAY = 4;
    public const FRIDAY = 5;
    public const SATURDAY = 6;

    #[ORM\Id, ORM\Column, ORM\GeneratedValue]
    private ?int $id = null;

    #[ORM\Column]
    #[Groups(['schedule_data:read', 'schedule_data:write'])]
    private bool $sunday = false;

    #[ORM\Column]
    #[Groups(['schedule_data:read', 'schedule_data:write'])]
    private bool $monday = false;

    #[ORM\Column]
    #[Groups(['schedule_data:read', 'schedule_data:write'])]
    private bool $tuesday = false;

    #[ORM\Column]
    #[Groups(['schedule_data:read', 'schedule_data:write'])]
    private bool $wednesday = false;

    #[ORM\Column]
    #[Groups(['schedule_data:read', 'schedule_data:write'])]
    private bool $thursday = false;

    #[ORM\Column]
    #[Groups(['schedule_data:read', 'schedule_data:write'])]
    private bool $friday = false;

    #[ORM\Column]
    #[Groups(['schedule_data:read', 'schedule_data:write'])]
    private bool $saturday = false;

    #[ORM\Column]
    #[Groups(['schedule_data:read', 'schedule_data:write'])]
    private int $frequency = 0;

    #[ORM\Column]
    #[Groups(['schedule_data:read', 'schedule_data:write'])]
    private bool $weekly = false;

    #[ORM\Column]
    #[Groups(['schedule_data:read', 'schedule_data:write'])]
    private bool $monthly = false;

    #[ORM\Column]
    #[Assert\DateTime]
    #[Groups(['schedule_data:read', 'schedule_data:write'])]
    private ?DateTime $startDate = null;

    #[ORM\Column]
    #[Groups(['schedule_data:read'])]
    private ?DateTimeImmutable $dateCreated = null;

    #[ORM\Column]
    #[Groups(['schedule_data:read'])]
    private ?DateTimeImmutable $dateUpdated = null;

    public function __construct()
    {
        $this->startDate = new DateTime();
        $this->dateCreated = new DateTimeImmutable();
        $this->dateUpdated = new DateTimeImmutable();
    }

    public function getStartDate(): ?DateTime
    {
        return $this->startDate;
    }

    public function setStartDate(?DateTime $startDate): ScheduleData
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function isSunday(): bool
    {
        return $this->sunday;
    }

    public function setSunday(bool $sunday = true): ScheduleData
    {
        $this->sunday = $sunday;

        return $this;
    }

    public function isMonday(): bool
    {
        return $this->monday;
    }

    public function setMonday(bool $monday = true): ScheduleData
    {
        $this->monday = $monday;

        return $this;
    }

    public function isTuesday(): bool
    {
        return $this->tuesday;
    }

    public function setTuesday(bool $tuesday = true): ScheduleData
    {
        $this->tuesday = $tuesday;

        return $this;
    }

    public function isWednesday(): bool
    {
        return $this->wednesday;
    }

    public function setWednesday(bool $wednesday = true): ScheduleData
    {
        $this->wednesday = $wednesday;

        return $this;
    }

    public function isThursday(): bool
    {
        return $this->thursday;
    }

    public function setThursday(bool $thursday = true): ScheduleData
    {
        $this->thursday = $thursday;

        return $this;
    }

    public function isFriday(): bool
    {
        return $this->friday;
    }

    public function setFriday(bool $friday = true): ScheduleData
    {
        $this->friday = $friday;

        return $this;
    }

    public function isSaturday(): bool
    {
        return $this->saturday;
    }

    public function setSaturday(bool $saturday = true): ScheduleData
    {
        $this->saturday = $saturday;

        return $this;
    }

    public function getWeekdays(): array
    {
        return [
            self::SUNDAY => $this->sunday,
            self::MONDAY => $this->monday,
            self::TUESDAY => $this->tuesday,
            self::WEDNESDAY => $this->wednesday,
            self::THURSDAY => $this->thursday,
            self::FRIDAY => $this->friday,
            self::SATURDAY => $this->saturday,
        ];
    }

    public function setWeekdays(
        bool $sunday = false,
        bool $monday = false,
        bool $tuesday = false,
        bool $wednesday = false,
        bool $thursday = false,
        bool $friday = false,
        bool $saturday = false
    ): ScheduleData {
        $this->sunday = $sunday;
        $this->monday = $monday;
        $this->tuesday = $tuesday;
        $this->wednesday = $wednesday;
        $this->thursday = $thursday;
        $this->friday = $friday;
        $this->saturday = $saturday;

        return $this;
    }

    public function getFrequency(): int
    {
        return $this->frequency;
    }

    public function isWeekly(): bool
    {
        return $this->weekly;
    }

    public function isMonthly(): bool
    {
        return $this->monthly;
    }

    public function getDateCreated(): ?DateTimeImmutable
    {
        return $this->dateCreated;
    }

    /**
     * @param DateTimeImmutable|null $dateCreated
     * @return self
     */
    public function setDateCreated(?DateTimeImmutable $dateCreated): self
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    public function getDateUpdated(): ?DateTimeImmutable
    {
        return $this->dateUpdated;
    }

    public function setDateUpdated(?DateTimeImmutable $dateUpdated): ScheduleData
    {
        $this->dateUpdated = $dateUpdated;

        return $this;
    }

    public function setFrequencyAndTimeframe(int $frequency = 0, bool $weekly = false, bool $monthly = false): ScheduleData
    {
        if ($frequency > self::FREQUENCY_MAX) {
            $this->frequency = self::FREQUENCY_MAX;
        } else {
            $this->frequency = $frequency;
        }

        if ($weekly && $monthly) {
            throw new Exception('Cannot have both weekly and monthly set to true.');
        }

        if ($frequency > 0 && $weekly === false && $monthly === false) {
            throw new Exception('Must set weekly or monthly to true.');
        }

        $this->weekly = $weekly;
        $this->monthly = $monthly;

        return $this;
    }

    public function isFrequency(): bool
    {
        return $this->getFrequency() > 0 && ($this->isWeekly() || $this->isMonthly());
    }

}