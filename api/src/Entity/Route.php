<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use App\Entity\Enum\RouteStatus;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
#[ApiResource(
    normalizationContext: ['groups' => ['route:read']],
    denormalizationContext: ['groups' => ['route:write']],
)]
class Route
{
    #[ORM\Id, ORM\Column, ORM\GeneratedValue]
    private ?int $id = null;

    #[ORM\Column]
    #[Assert\NotBlank]
    #[Groups(['route:read', 'route:write'])]
    private ?string $name = null;

    #[ORM\JoinColumn, ORM\ManyToOne(targetEntity: RouteStatus::class)]
    #[Groups(['route:read', 'route:write'])]
    #[ApiProperty(push: true)]
    private ?RouteStatus $routeStatus = null;

    #[ORM\JoinColumn, ORM\ManyToOne(targetEntity: Person::class)]
    #[Groups(['route:read', 'route:write'])]
    #[ApiProperty(push: true)]
    private ?Person $driver = null;

    #[ORM\ManyToMany(targetEntity: ServiceSite::class)]
    #[Groups(['route:read', 'route:write'])]
    #[ApiProperty(push: true)]
    private ?Collection $serviceSites = null;

    #[ORM\Column]
    #[Groups(['route:read'])]
    private ?DateTimeImmutable $dateCreated = null;

    #[ORM\Column]
    #[Groups(['route:read'])]
    private ?DateTimeImmutable $dateUpdated = null;

    public function __construct()
    {
        $this->dateCreated = new DateTimeImmutable();
        $this->dateUpdated = new DateTimeImmutable();
        $this->serviceSites = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): Route
    {
        $this->name = $name;

        return $this;
    }

    public function getRouteStatus(): ?RouteStatus
    {
        return $this->routeStatus;
    }

    public function setRouteStatus(?RouteStatus $routeStatus): Route
    {
        $this->routeStatus = $routeStatus;

        return $this;
    }

    public function getDriver(): ?Person
    {
        return $this->driver;
    }

    public function setDriver(?Person $driver): Route
    {
        $this->driver = $driver;

        return $this;
    }

    public function getServiceSites(): ?Collection
    {
        return $this->serviceSites;
    }

    public function setServiceSites(?Collection $serviceSites): Route
    {
        $this->serviceSites = $serviceSites;

        return $this;
    }

    public function addServiceSite(ServiceSite $serviceSite): Route
    {
        if ($this->serviceSites->contains($serviceSite)) {
            return $this;
        }

        $this->serviceSites->add($serviceSite);
        $serviceSite->addRoute($this);

        return $this;
    }

    public function removeServiceSite(ServiceSite $serviceSite): Route
    {
        if ($this->serviceSites->contains($serviceSite)) {
            $this->serviceSites->removeElement($serviceSite);
            $serviceSite->removeRoute($this);
        }

        return $this;
    }

    public function getDateCreated(): ?DateTimeImmutable
    {
        return $this->dateCreated;
    }

    public function setDateCreated(?DateTimeImmutable $dateCreated): self
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    public function getDateUpdated(): ?DateTimeImmutable
    {
        return $this->dateUpdated;
    }

    public function setDateUpdated(?DateTimeImmutable $dateUpdated): Route
    {
        $this->dateUpdated = $dateUpdated;

        return $this;
    }

}