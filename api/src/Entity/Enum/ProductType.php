<?php

namespace App\Entity\Enum;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use App\Repository\ProductTypeRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ProductTypeRepository::class)]
#[ApiResource(
    normalizationContext: ['groups' => ['product_type:read']],
)]
class ProductType
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['product_type:read'])]
    #[ApiProperty(push: true)]
    private ?string $name = null;

    #[ORM\Column(length: 10)]
    #[Groups(['product_type:read'])]
    #[ApiProperty(push: true)]
    private ?string $code = null;

    #[ORM\Column(length: 255)]
    #[Groups(['product_type:read'])]
    #[ApiProperty(push: true)]
    private ?string $description = null;

    #[ORM\Column]
    #[Groups(['product_type:read'])]
    #[ApiProperty(push: true)]
    private ?DateTimeImmutable $date_created = null;

    #[ORM\Column]
    #[Groups(['product_type:read'])]
    #[ApiProperty(push: true)]
    private ?DateTimeImmutable $date_updated = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): ProductType
    {
        $this->description = $description;

        return $this;
    }

    public function getDateCreated(): ?DateTimeImmutable
    {
        return $this->date_created;
    }

    public function setDateCreated(DateTimeImmutable $date_created): self
    {
        $this->date_created = $date_created;

        return $this;
    }

    public function getDateUpdated(): ?DateTimeImmutable
    {
        return $this->date_updated;
    }

    public function setDateUpdated(DateTimeImmutable $date_updated): self
    {
        $this->date_updated = $date_updated;

        return $this;
    }
}
