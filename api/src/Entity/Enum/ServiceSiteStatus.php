<?php

namespace App\Entity\Enum;

use ApiPlatform\Metadata\ApiResource;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
#[ApiResource(
    normalizationContext: ['groups' => ['service_site_status:read']],
)]
class ServiceSiteStatus
{
    #[ORM\Id, ORM\Column, ORM\GeneratedValue]
    private ?int $id = null;

    #[ORM\Column]
    #[Groups(['service_site_status:read'])]
    private ?string $name = null;

    #[ORM\Column(type: "string", unique: true)]
    #[Groups(['service_site_status:read'])]
    private ?string $code = null;

    #[ORM\Column]
    #[Groups(['service_site_status:read'])]
    private ?string $description = null;

    #[ORM\Column]
    #[Groups(['service_site_status:read'])]
    private ?DateTimeImmutable $dateCreated = null;

    #[ORM\Column]
    #[Groups(['service_site_status:read'])]
    private ?DateTimeImmutable $dateUpdated = null;

    public function __construct()
    {
        $this->dateCreated = new DateTimeImmutable();
        $this->dateUpdated = new DateTimeImmutable();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return ServiceSiteStatus
     */
    public function setName(?string $name): ServiceSiteStatus
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string|null $code
     * @return ServiceSiteStatus
     */
    public function setCode(?string $code): ServiceSiteStatus
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return ServiceSiteStatus
     */
    public function setDescription(?string $description): ServiceSiteStatus
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getDateCreated(): ?DateTimeImmutable
    {
        return $this->dateCreated;
    }

    /**
     * @param DateTimeImmutable|null $dateCreated
     * @return self
     */
    public function setDateCreated(?DateTimeImmutable $dateCreated): self
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getDateUpdated(): ?DateTimeImmutable
    {
        return $this->dateUpdated;
    }

    /**
     * @param DateTimeImmutable|null $dateUpdated
     * @return ServiceSiteStatus
     */
    public function setDateUpdated(?DateTimeImmutable $dateUpdated): ServiceSiteStatus
    {
        $this->dateUpdated = $dateUpdated;

        return $this;
    }


}
