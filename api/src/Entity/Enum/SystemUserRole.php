<?php

namespace App\Entity\Enum;

use ApiPlatform\Metadata\ApiResource;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
#[ApiResource(
    normalizationContext: ['groups' => ['system_user_role:read']],
)]
class SystemUserRole
{
    #[ORM\Id, ORM\Column, ORM\GeneratedValue]
    private ?int $id = null;

    #[ORM\Column]
    #[Groups(['system_user_role:read'])]
    private ?string $name = null;

    #[ORM\Column(type: "string", unique: true)]
    #[Groups(['system_user_role:read'])]
    private ?string $code = null;

    #[ORM\Column]
    #[Groups(['system_user_role:read'])]
    private ?string $description = null;

    #[ORM\Column]
    #[Groups(['system_user_role:read'])]
    private ?DateTimeImmutable $dateCreated = null;

    #[ORM\Column]
    #[Groups(['system_user_role:read'])]
    private ?DateTimeImmutable $dateUpdated = null;

    public function __construct()
    {
        $this->dateCreated = new DateTimeImmutable();
        $this->dateUpdated = new DateTimeImmutable();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return SystemUserRole
     */
    public function setName(?string $name): SystemUserRole
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string|null $code
     * @return SystemUserRole
     */
    public function setCode(?string $code): SystemUserRole
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return SystemUserRole
     */
    public function setDescription(?string $description): SystemUserRole
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getDateCreated(): ?DateTimeImmutable
    {
        return $this->dateCreated;
    }

    /**
     * @param DateTimeImmutable|null $dateCreated
     * @return SystemUserRole
     */
    public function setDateCreated(?DateTimeImmutable $dateCreated): SystemUserRole
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getDateUpdated(): ?DateTimeImmutable
    {
        return $this->dateUpdated;
    }

    /**
     * @param DateTimeImmutable|null $dateUpdated
     * @return SystemUserRole
     */
    public function setDateUpdated(?DateTimeImmutable $dateUpdated): SystemUserRole
    {
        $this->dateUpdated = $dateUpdated;

        return $this;
    }


}
