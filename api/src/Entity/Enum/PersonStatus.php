<?php

namespace App\Entity\Enum;

use ApiPlatform\Metadata\ApiResource;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
#[ApiResource(
    normalizationContext: ['groups' => ['person_status:read']],
)]
class PersonStatus
{
    #[ORM\Id, ORM\Column, ORM\GeneratedValue]
    private ?int $id = null;

    #[ORM\Column]
    #[Groups(['person_status:read'])]
    private ?string $name = null;

    #[ORM\Column(type: "string", unique: true)]
    #[Groups(['person_status:read'])]
    private ?string $code = null;

    #[ORM\Column]
    #[Groups(['person_status:read'])]
    private ?string $description = null;

    #[ORM\Column]
    #[Groups(['person_status:read'])]
    private ?DateTimeImmutable $dateCreated = null;

    #[ORM\Column]
    #[Groups(['person_status:read'])]
    private ?DateTimeImmutable $dateUpdated = null;

    public function __construct()
    {
        $this->dateCreated = new DateTimeImmutable();
        $this->dateUpdated = new DateTimeImmutable();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return PersonStatus
     */
    public function setName(?string $name): PersonStatus
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string|null $code
     * @return PersonStatus
     */
    public function setCode(?string $code): PersonStatus
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return PersonStatus
     */
    public function setDescription(?string $description): PersonStatus
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getDateCreated(): ?DateTimeImmutable
    {
        return $this->dateCreated;
    }

    /**
     * @param DateTimeImmutable|null $dateCreated
     * @return self
     */
    public function setDateCreated(?DateTimeImmutable $dateCreated): self
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getDateUpdated(): ?DateTimeImmutable
    {
        return $this->dateUpdated;
    }

    /**
     * @param DateTimeImmutable|null $dateUpdated
     * @return PersonStatus
     */
    public function setDateUpdated(?DateTimeImmutable $dateUpdated): PersonStatus
    {
        $this->dateUpdated = $dateUpdated;

        return $this;
    }

}
