<?php

namespace App\CustomFixtureProvider;

use DateTimeImmutable;
use DateTimeInterface;
use Faker\Provider\DateTime;

final class DateTimeImmutableProvider extends DateTime
{
    public static function dateTimeImmutableBetween($startDate = '-30 month', $endDate = 'now', $timezone = null): DateTimeImmutable
    {
        $startTimestamp = $startDate instanceof DateTimeInterface ? $startDate->getTimestamp() : $startDate;
        $dateTime = parent::dateTimeBetween($startTimestamp, $endDate, $timezone);

        return DateTimeImmutable::createFromMutable($dateTime);
    }
}