<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class GetDemoController extends AbstractController
{
    #[Route(path: '/demo')]
    public function __invoke(): Response
    {
        return $this->json(['hello' => 'james']);
    }

}