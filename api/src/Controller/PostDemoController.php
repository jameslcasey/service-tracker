<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class PostDemoController extends AbstractController
{
    #[Route(path: '/demo', )]
    public function __invoke(Request $request): Response
    {
        return $this->json(['hello' => $request->request->get('name')]);
    }

}