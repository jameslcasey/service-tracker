<?php

namespace App\Collection;

use App\Entity\Address;
use Doctrine\Common\Collections\ArrayCollection;
use LogicException;

class AddressCollection extends ArrayCollection
{
    private string $enforceType = Address::class;

    public function __construct(array $elements = [])
    {
        array_map(fn($element) => $this->enforceType($element), $elements);
        parent::__construct($elements);
    }

    private function enforceType($object)
    {
        if (is_a($object, $this->enforceType) === false) {
            throw new LogicException(sprintf('Must be an instance of %s', $this->enforceType));
        }
    }

    /**
     * @param $serviceSite
     * @return bool|true
     */
    public function add($serviceSite)
    {
        $this->enforceType($serviceSite);
        return parent::add($serviceSite);
    }

    /**
     * @param $key
     * @param $value
     * @return void
     */
    public function set($key, $value)
    {
        $this->enforceType($value);
        parent::set($key, $value);
    }

}