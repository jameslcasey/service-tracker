<?php

namespace App\Service;

use App\Entity\ScheduleData;
use DateInterval;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;

class Scheduler
{
    private ArrayCollection $frequencyServiceDates;
    private ArrayCollection $weekdayServiceDates;

    public function __construct()
    {
        $this->frequencyServiceDates = new ArrayCollection();
        $this->weekdayServiceDates = new ArrayCollection();
    }

    public function resetServcieDates()
    {
        $this->frequencyServiceDates = new ArrayCollection();
        $this->weekdayServiceDates = new ArrayCollection();
    }

    public function getNextServiceDate(ScheduleData $scheduleData): DateTime
    {
        if ($scheduleData->isFrequency()) {
            return $this->calculateNextFrequencyDate($scheduleData);
        }

        return $this->calculateNextWeekdayDate($scheduleData);
    }

    private function calculateNextFrequencyDate(ScheduleData $scheduleData): ?DateTime
    {
        $startDate = $scheduleData->getStartDate();

        return $startDate->add(DateInterval::createFromDateString($this->getDuration($scheduleData)));
    }

    private function getDuration(
        ScheduleData $scheduleData = null
    ): string {
        if ($scheduleData && $scheduleData->isFrequency()) {
            $frequency = $scheduleData->getFrequency();
            $timeframe = $scheduleData->isWeekly() ? 'week' : 'month';
            $duration = sprintf('%s %s', $frequency, $timeframe);
        } else {
            $duration = "1 day";
        }

        return $duration;
    }

    private function calculateNextWeekdayDate(ScheduleData $scheduleData): DateTime
    {
        $date = $this->getNewDateTimeInstance();
        while ($this->isSelectedWeekDay($scheduleData, $date) === false) {
            $date->add(DateInterval::createFromDateString($this->getDuration($scheduleData)));
        }

        return $date;
    }

    private function getNewDateTimeInstance(DateTime $date = new DateTime()): DateTime
    {
        $y = (int)$date->format('Y');
        $m = (int)$date->format('m');
        $d = (int)$date->format('d');

        return (new DateTime())->setTime(0, 0)->setDate($y, $m, $d);
    }

    private function isSelectedWeekDay(ScheduleData $scheduleData, DateTime $startDate): bool
    {
        $startWeekdayNumber = (int)$startDate->format('w');
        $weekdaysWithSelected = $scheduleData->getWeekdays();

        foreach ($weekdaysWithSelected as $day => $isSelected) {
            if ($startWeekdayNumber === $day) {
                return $isSelected;
            }
        }

        return false;
    }

    public function getServiceDatesThroughEndDate(
        ScheduleData $scheduleData,
        DateTime $endDate = new DateTime()
    ): ArrayCollection {
        if ($scheduleData->isFrequency()) {
            return $this->calculateFrequencyDatesThroughDate(
                $scheduleData,
                $this->getNewDateTimeInstance($scheduleData->getStartDate()),
                $this->getNewDateTimeInstance($endDate)
            );
        }

        return $this->calculateWeekdaysThroughDate(
            $scheduleData,
            $this->getNewDateTimeInstance($scheduleData->getStartDate()),
            $this->getNewDateTimeInstance($endDate)
        );
    }

    private function calculateFrequencyDatesThroughDate(
        ScheduleData $scheduleData,
        DateTime $startDate = new DateTime(),
        DateTime $endDate = new DateTime()
    ): ArrayCollection {
        if ($startDate <= $endDate) {
            $this->frequencyServiceDates->add($startDate);
            $nextServiceDate = $this->getNewDateTimeInstance($startDate)
                ->add(DateInterval::createFromDateString($this->getDuration($scheduleData)));
            $this->calculateFrequencyDatesThroughDate($scheduleData, $nextServiceDate, $endDate);
        }

        return $this->frequencyServiceDates;
    }

    private function calculateWeekdaysThroughDate(
        ScheduleData $scheduleData,
        DateTime $startDate = new DateTime(),
        DateTime $endDate = new DateTime()
    ): ArrayCollection {
        if ($this->isSelectedWeekDay($scheduleData, $startDate)) {
            $this->weekdayServiceDates->add($startDate);
        }

        $potentialNextServiceDate = $this->getNewDateTimeInstance($startDate)
            ->add(DateInterval::createFromDateString($this->getDuration()));

        if ($potentialNextServiceDate <= $endDate) {
            $this->calculateWeekdaysThroughDate($scheduleData, $potentialNextServiceDate, $endDate);
        }

        return $this->weekdayServiceDates;
    }

}