<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221212223357 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE service_ticket DROP CONSTRAINT fk_a967a895d42f8111');
        $this->addSql('DROP SEQUENCE service_request_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE service_request_status_id_seq CASCADE');
        $this->addSql('ALTER TABLE service_request DROP CONSTRAINT fk_f413dd03393944da');
        $this->addSql('DROP TABLE service_request');
        $this->addSql('DROP TABLE service_request_status');
        $this->addSql('DROP INDEX uniq_a967a895d42f8111');
        $this->addSql('ALTER TABLE service_ticket DROP service_request_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE SEQUENCE service_request_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE service_request_status_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE service_request (id INT NOT NULL, service_site_id INT DEFAULT NULL, date_created TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, date_updated TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, service_request_status VARCHAR(255) NOT NULL, driver VARCHAR(255) NOT NULL, route VARCHAR(255) NOT NULL, products VARCHAR(255) NOT NULL, scheduler VARCHAR(255) NOT NULL, scheduled_for_date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_f413dd03393944da ON service_request (service_site_id)');
        $this->addSql('COMMENT ON COLUMN service_request.date_created IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN service_request.date_updated IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN service_request.scheduled_for_date IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE service_request_status (id INT NOT NULL, date_created TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, date_updated TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, name VARCHAR(255) NOT NULL, code VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX uniq_333ffb5177153098 ON service_request_status (code)');
        $this->addSql('COMMENT ON COLUMN service_request_status.date_created IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN service_request_status.date_updated IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE service_request ADD CONSTRAINT fk_f413dd03393944da FOREIGN KEY (service_site_id) REFERENCES service_site (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE service_ticket ADD service_request_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE service_ticket ADD CONSTRAINT fk_a967a895d42f8111 FOREIGN KEY (service_request_id) REFERENCES service_request (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX uniq_a967a895d42f8111 ON service_ticket (service_request_id)');
    }
}
