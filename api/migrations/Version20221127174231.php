<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221127174231 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE account ADD uuid VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE account_status ADD uuid VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE account_status ADD name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE account_status ADD code VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE address ADD uuid VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE address_status ADD uuid VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE address_status ADD name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE address_status ADD code VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE address_type ADD uuid VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE address_type ADD name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE address_type ADD code VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE person ADD uuid VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE person_status ADD uuid VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE person_status ADD name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE person_status ADD code VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE product ADD uuid VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE product_status ADD uuid VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE product_status ADD name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE product_status ADD code VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_request ADD uuid VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_request_status ADD uuid VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_request_status ADD name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_request_status ADD code VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_site ADD uuid VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_site_status ADD uuid VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_site_status ADD name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_site_status ADD code VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_ticket ADD uuid VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_ticket_status ADD uuid VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_ticket_status ADD name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_ticket_status ADD code VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE system_user_role ADD uuid VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE system_user_role ADD name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE system_user_role ADD code VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE address_status DROP uuid');
        $this->addSql('ALTER TABLE address_status DROP name');
        $this->addSql('ALTER TABLE address_status DROP code');
        $this->addSql('ALTER TABLE service_request_status DROP uuid');
        $this->addSql('ALTER TABLE service_request_status DROP name');
        $this->addSql('ALTER TABLE service_request_status DROP code');
        $this->addSql('ALTER TABLE service_site DROP uuid');
        $this->addSql('ALTER TABLE product DROP uuid');
        $this->addSql('ALTER TABLE account DROP uuid');
        $this->addSql('ALTER TABLE service_request DROP uuid');
        $this->addSql('ALTER TABLE account_status DROP uuid');
        $this->addSql('ALTER TABLE account_status DROP name');
        $this->addSql('ALTER TABLE account_status DROP code');
        $this->addSql('ALTER TABLE person DROP uuid');
        $this->addSql('ALTER TABLE service_ticket DROP uuid');
        $this->addSql('ALTER TABLE service_site_status DROP uuid');
        $this->addSql('ALTER TABLE service_site_status DROP name');
        $this->addSql('ALTER TABLE service_site_status DROP code');
        $this->addSql('ALTER TABLE address DROP uuid');
        $this->addSql('ALTER TABLE address_type DROP uuid');
        $this->addSql('ALTER TABLE address_type DROP name');
        $this->addSql('ALTER TABLE address_type DROP code');
        $this->addSql('ALTER TABLE service_ticket_status DROP uuid');
        $this->addSql('ALTER TABLE service_ticket_status DROP name');
        $this->addSql('ALTER TABLE service_ticket_status DROP code');
        $this->addSql('ALTER TABLE person_status DROP uuid');
        $this->addSql('ALTER TABLE person_status DROP name');
        $this->addSql('ALTER TABLE person_status DROP code');
        $this->addSql('ALTER TABLE product_status DROP uuid');
        $this->addSql('ALTER TABLE product_status DROP name');
        $this->addSql('ALTER TABLE product_status DROP code');
        $this->addSql('ALTER TABLE system_user_role DROP uuid');
        $this->addSql('ALTER TABLE system_user_role DROP name');
        $this->addSql('ALTER TABLE system_user_role DROP code');
    }
}
