<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221218162630 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE service_ticket_item_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE service_ticket_item (id INT NOT NULL, service_ticket_id INT DEFAULT NULL, product_id INT DEFAULT NULL, date_created TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, date_updated TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E15A168A28917D98 ON service_ticket_item (service_ticket_id)');
        $this->addSql('CREATE INDEX IDX_E15A168A4584665A ON service_ticket_item (product_id)');
        $this->addSql('COMMENT ON COLUMN service_ticket_item.date_created IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN service_ticket_item.date_updated IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE service_ticket_item ADD CONSTRAINT FK_E15A168A28917D98 FOREIGN KEY (service_ticket_id) REFERENCES service_ticket (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE service_ticket_item ADD CONSTRAINT FK_E15A168A4584665A FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE address ADD address_status_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE address DROP address_status');
        $this->addSql('ALTER TABLE address ADD CONSTRAINT FK_D4E6F81F1B63CF5 FOREIGN KEY (address_status_id) REFERENCES address_status (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_D4E6F81F1B63CF5 ON address (address_status_id)');
        $this->addSql('ALTER TABLE person ADD person_status_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE person DROP person_status');
        $this->addSql('ALTER TABLE person ADD CONSTRAINT FK_34DCD176D3DEE317 FOREIGN KEY (person_status_id) REFERENCES person_status (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_34DCD176D3DEE317 ON person (person_status_id)');
        $this->addSql('ALTER TABLE product ADD product_status_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product DROP product_status');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD557B630 FOREIGN KEY (product_status_id) REFERENCES product_status (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_D34A04AD557B630 ON product (product_status_id)');
        $this->addSql('ALTER TABLE route ADD route_status_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE route DROP route_status');
        $this->addSql('ALTER TABLE route ADD CONSTRAINT FK_2C42079C7B5D5B5 FOREIGN KEY (route_status_id) REFERENCES route_status (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_2C42079C7B5D5B5 ON route (route_status_id)');
        $this->addSql('ALTER TABLE schedule_data DROP service_site');
        $this->addSql('ALTER TABLE schedule_data DROP product');
        $this->addSql('ALTER TABLE schedule_data DROP quantity');
        $this->addSql('ALTER TABLE service_site ADD service_site_status_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE service_site ADD account_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE service_site ADD physical_address_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE service_site ADD billing_address_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE service_site DROP service_site_status');
        $this->addSql('ALTER TABLE service_site DROP physical_address');
        $this->addSql('ALTER TABLE service_site DROP billing_address');
        $this->addSql('ALTER TABLE service_site DROP account');
        $this->addSql('ALTER TABLE service_site ADD CONSTRAINT FK_A700BD08E9B2B7E FOREIGN KEY (service_site_status_id) REFERENCES service_site_status (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE service_site ADD CONSTRAINT FK_A700BD089B6B5FBA FOREIGN KEY (account_id) REFERENCES account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE service_site ADD CONSTRAINT FK_A700BD086646778D FOREIGN KEY (physical_address_id) REFERENCES address (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE service_site ADD CONSTRAINT FK_A700BD0879D0C0E4 FOREIGN KEY (billing_address_id) REFERENCES address (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_A700BD08E9B2B7E ON service_site (service_site_status_id)');
        $this->addSql('CREATE INDEX IDX_A700BD089B6B5FBA ON service_site (account_id)');
        $this->addSql('CREATE INDEX IDX_A700BD086646778D ON service_site (physical_address_id)');
        $this->addSql('CREATE INDEX IDX_A700BD0879D0C0E4 ON service_site (billing_address_id)');
        $this->addSql('ALTER TABLE service_ticket ADD service_ticket_status_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE service_ticket ADD driver_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE service_ticket DROP service_ticket_status');
        $this->addSql('ALTER TABLE service_ticket DROP driver');
        $this->addSql('ALTER TABLE service_ticket DROP route');
        $this->addSql('ALTER TABLE service_ticket DROP products');
        $this->addSql('ALTER TABLE service_ticket ADD CONSTRAINT FK_A967A8955688FDE6 FOREIGN KEY (service_ticket_status_id) REFERENCES service_ticket_status (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE service_ticket ADD CONSTRAINT FK_A967A895C3423909 FOREIGN KEY (driver_id) REFERENCES person (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_A967A8955688FDE6 ON service_ticket (service_ticket_status_id)');
        $this->addSql('CREATE INDEX IDX_A967A895C3423909 ON service_ticket (driver_id)');
        $this->addSql('ALTER TABLE system_user_role DROP uuid');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE service_ticket_item_id_seq CASCADE');
        $this->addSql('ALTER TABLE service_ticket_item DROP CONSTRAINT FK_E15A168A28917D98');
        $this->addSql('ALTER TABLE service_ticket_item DROP CONSTRAINT FK_E15A168A4584665A');
        $this->addSql('DROP TABLE service_ticket_item');
        $this->addSql('ALTER TABLE product DROP CONSTRAINT FK_D34A04AD557B630');
        $this->addSql('DROP INDEX IDX_D34A04AD557B630');
        $this->addSql('ALTER TABLE product ADD product_status VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE product DROP product_status_id');
        $this->addSql('ALTER TABLE person DROP CONSTRAINT FK_34DCD176D3DEE317');
        $this->addSql('DROP INDEX IDX_34DCD176D3DEE317');
        $this->addSql('ALTER TABLE person ADD person_status VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE person DROP person_status_id');
        $this->addSql('ALTER TABLE schedule_data ADD service_site VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE schedule_data ADD product VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE schedule_data ADD quantity INT NOT NULL');
        $this->addSql('ALTER TABLE address DROP CONSTRAINT FK_D4E6F81F1B63CF5');
        $this->addSql('DROP INDEX IDX_D4E6F81F1B63CF5');
        $this->addSql('ALTER TABLE address ADD address_status VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE address DROP address_status_id');
        $this->addSql('ALTER TABLE system_user_role ADD uuid UUID NOT NULL');
        $this->addSql('ALTER TABLE route DROP CONSTRAINT FK_2C42079C7B5D5B5');
        $this->addSql('DROP INDEX IDX_2C42079C7B5D5B5');
        $this->addSql('ALTER TABLE route ADD route_status VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE route DROP route_status_id');
        $this->addSql('ALTER TABLE service_ticket DROP CONSTRAINT FK_A967A8955688FDE6');
        $this->addSql('ALTER TABLE service_ticket DROP CONSTRAINT FK_A967A895C3423909');
        $this->addSql('DROP INDEX IDX_A967A8955688FDE6');
        $this->addSql('DROP INDEX IDX_A967A895C3423909');
        $this->addSql('ALTER TABLE service_ticket ADD service_ticket_status VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_ticket ADD driver VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_ticket ADD route VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_ticket ADD products VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_ticket DROP service_ticket_status_id');
        $this->addSql('ALTER TABLE service_ticket DROP driver_id');
        $this->addSql('ALTER TABLE service_site DROP CONSTRAINT FK_A700BD08E9B2B7E');
        $this->addSql('ALTER TABLE service_site DROP CONSTRAINT FK_A700BD089B6B5FBA');
        $this->addSql('ALTER TABLE service_site DROP CONSTRAINT FK_A700BD086646778D');
        $this->addSql('ALTER TABLE service_site DROP CONSTRAINT FK_A700BD0879D0C0E4');
        $this->addSql('DROP INDEX IDX_A700BD08E9B2B7E');
        $this->addSql('DROP INDEX IDX_A700BD089B6B5FBA');
        $this->addSql('DROP INDEX IDX_A700BD086646778D');
        $this->addSql('DROP INDEX IDX_A700BD0879D0C0E4');
        $this->addSql('ALTER TABLE service_site ADD service_site_status VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_site ADD physical_address VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_site ADD billing_address VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_site ADD account VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_site DROP service_site_status_id');
        $this->addSql('ALTER TABLE service_site DROP account_id');
        $this->addSql('ALTER TABLE service_site DROP physical_address_id');
        $this->addSql('ALTER TABLE service_site DROP billing_address_id');
    }
}
