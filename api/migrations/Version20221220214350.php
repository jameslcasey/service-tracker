<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221220214350 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE service_schedule_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE service_schedule_item_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE service_schedule (id INT NOT NULL, service_site_id INT DEFAULT NULL, route_id INT DEFAULT NULL, schedule_data_id INT DEFAULT NULL, date_created TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, date_updated TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A34E4AC6393944DA ON service_schedule (service_site_id)');
        $this->addSql('CREATE INDEX IDX_A34E4AC634ECB4E6 ON service_schedule (route_id)');
        $this->addSql('CREATE INDEX IDX_A34E4AC6BA9F8D8D ON service_schedule (schedule_data_id)');
        $this->addSql('COMMENT ON COLUMN service_schedule.date_created IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN service_schedule.date_updated IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE service_schedule_item (id INT NOT NULL, service_schedule_id INT DEFAULT NULL, product_id INT DEFAULT NULL, quantity INT NOT NULL, date_created TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, date_updated TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_2FECBFC76891E479 ON service_schedule_item (service_schedule_id)');
        $this->addSql('CREATE INDEX IDX_2FECBFC74584665A ON service_schedule_item (product_id)');
        $this->addSql('COMMENT ON COLUMN service_schedule_item.date_created IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN service_schedule_item.date_updated IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE service_schedule ADD CONSTRAINT FK_A34E4AC6393944DA FOREIGN KEY (service_site_id) REFERENCES service_site (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE service_schedule ADD CONSTRAINT FK_A34E4AC634ECB4E6 FOREIGN KEY (route_id) REFERENCES route (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE service_schedule ADD CONSTRAINT FK_A34E4AC6BA9F8D8D FOREIGN KEY (schedule_data_id) REFERENCES schedule_data (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE service_schedule_item ADD CONSTRAINT FK_2FECBFC76891E479 FOREIGN KEY (service_schedule_id) REFERENCES service_schedule (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE service_schedule_item ADD CONSTRAINT FK_2FECBFC74584665A FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE route ADD driver_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE route ADD CONSTRAINT FK_2C42079C3423909 FOREIGN KEY (driver_id) REFERENCES person (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_2C42079C3423909 ON route (driver_id)');
        $this->addSql('ALTER TABLE schedule_data DROP next_service_date');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE service_schedule_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE service_schedule_item_id_seq CASCADE');
        $this->addSql('ALTER TABLE service_schedule DROP CONSTRAINT FK_A34E4AC6393944DA');
        $this->addSql('ALTER TABLE service_schedule DROP CONSTRAINT FK_A34E4AC634ECB4E6');
        $this->addSql('ALTER TABLE service_schedule DROP CONSTRAINT FK_A34E4AC6BA9F8D8D');
        $this->addSql('ALTER TABLE service_schedule_item DROP CONSTRAINT FK_2FECBFC76891E479');
        $this->addSql('ALTER TABLE service_schedule_item DROP CONSTRAINT FK_2FECBFC74584665A');
        $this->addSql('DROP TABLE service_schedule');
        $this->addSql('DROP TABLE service_schedule_item');
        $this->addSql('ALTER TABLE schedule_data ADD next_service_date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL');
        $this->addSql('ALTER TABLE route DROP CONSTRAINT FK_2C42079C3423909');
        $this->addSql('DROP INDEX IDX_2C42079C3423909');
        $this->addSql('ALTER TABLE route DROP driver_id');
    }
}
