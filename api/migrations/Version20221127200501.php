<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221127200501 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE account ADD guid UUID NOT NULL');
        $this->addSql('ALTER TABLE account DROP uuid');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7D3656A42B6FCFB2 ON account (guid)');
        $this->addSql('ALTER TABLE account_status ADD guid UUID NOT NULL');
        $this->addSql('ALTER TABLE account_status DROP uuid');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5C34AB6F2B6FCFB2 ON account_status (guid)');
        $this->addSql('ALTER TABLE address ADD guid UUID NOT NULL');
        $this->addSql('ALTER TABLE address DROP uuid');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D4E6F812B6FCFB2 ON address (guid)');
        $this->addSql('ALTER TABLE address_status ADD guid UUID NOT NULL');
        $this->addSql('ALTER TABLE address_status DROP uuid');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4059E0A32B6FCFB2 ON address_status (guid)');
        $this->addSql('ALTER TABLE address_type ADD guid UUID NOT NULL');
        $this->addSql('ALTER TABLE address_type DROP uuid');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F19287C22B6FCFB2 ON address_type (guid)');
        $this->addSql('ALTER TABLE person ADD guid UUID NOT NULL');
        $this->addSql('ALTER TABLE person DROP uuid');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_34DCD1762B6FCFB2 ON person (guid)');
        $this->addSql('ALTER TABLE person_status ADD guid UUID NOT NULL');
        $this->addSql('ALTER TABLE person_status DROP uuid');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E7A555AA2B6FCFB2 ON person_status (guid)');
        $this->addSql('ALTER TABLE product ADD guid UUID NOT NULL');
        $this->addSql('ALTER TABLE product DROP uuid');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D34A04AD2B6FCFB2 ON product (guid)');
        $this->addSql('ALTER TABLE product_status ADD guid UUID NOT NULL');
        $this->addSql('ALTER TABLE product_status DROP uuid');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_197C24B82B6FCFB2 ON product_status (guid)');
        $this->addSql('ALTER TABLE route ADD guid UUID NOT NULL');
        $this->addSql('ALTER TABLE route DROP uuid');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2C420792B6FCFB2 ON route (guid)');
        $this->addSql('ALTER TABLE route_status ADD guid UUID NOT NULL');
        $this->addSql('ALTER TABLE route_status DROP uuid');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4AF394C12B6FCFB2 ON route_status (guid)');
        $this->addSql('ALTER TABLE service_request ADD guid UUID NOT NULL');
        $this->addSql('ALTER TABLE service_request DROP uuid');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F413DD032B6FCFB2 ON service_request (guid)');
        $this->addSql('ALTER TABLE service_request_status ADD guid UUID NOT NULL');
        $this->addSql('ALTER TABLE service_request_status DROP uuid');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_333FFB512B6FCFB2 ON service_request_status (guid)');
        $this->addSql('ALTER TABLE service_site ADD guid UUID NOT NULL');
        $this->addSql('ALTER TABLE service_site DROP uuid');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A700BD082B6FCFB2 ON service_site (guid)');
        $this->addSql('ALTER TABLE service_site_status ADD guid UUID NOT NULL');
        $this->addSql('ALTER TABLE service_site_status DROP uuid');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C734EDEF2B6FCFB2 ON service_site_status (guid)');
        $this->addSql('ALTER TABLE service_ticket ADD guid UUID NOT NULL');
        $this->addSql('ALTER TABLE service_ticket DROP uuid');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A967A8952B6FCFB2 ON service_ticket (guid)');
        $this->addSql('ALTER TABLE service_ticket_status ADD guid UUID NOT NULL');
        $this->addSql('ALTER TABLE service_ticket_status DROP uuid');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D1FDB5552B6FCFB2 ON service_ticket_status (guid)');
        $this->addSql('ALTER TABLE system_user_role ADD guid UUID NOT NULL');
        $this->addSql('ALTER TABLE system_user_role DROP uuid');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2E37CE1E2B6FCFB2 ON system_user_role (guid)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP INDEX UNIQ_F19287C22B6FCFB2');
        $this->addSql('ALTER TABLE address_type ADD uuid VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE address_type DROP guid');
        $this->addSql('DROP INDEX UNIQ_5C34AB6F2B6FCFB2');
        $this->addSql('ALTER TABLE account_status ADD uuid VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE account_status DROP guid');
        $this->addSql('DROP INDEX UNIQ_D1FDB5552B6FCFB2');
        $this->addSql('ALTER TABLE service_ticket_status ADD uuid VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_ticket_status DROP guid');
        $this->addSql('DROP INDEX UNIQ_C734EDEF2B6FCFB2');
        $this->addSql('ALTER TABLE service_site_status ADD uuid VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_site_status DROP guid');
        $this->addSql('DROP INDEX UNIQ_A967A8952B6FCFB2');
        $this->addSql('ALTER TABLE service_ticket ADD uuid VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_ticket DROP guid');
        $this->addSql('DROP INDEX UNIQ_4AF394C12B6FCFB2');
        $this->addSql('ALTER TABLE route_status ADD uuid VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE route_status DROP guid');
        $this->addSql('DROP INDEX UNIQ_197C24B82B6FCFB2');
        $this->addSql('ALTER TABLE product_status ADD uuid VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE product_status DROP guid');
        $this->addSql('DROP INDEX UNIQ_7D3656A42B6FCFB2');
        $this->addSql('ALTER TABLE account ADD uuid VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE account DROP guid');
        $this->addSql('DROP INDEX UNIQ_2C420792B6FCFB2');
        $this->addSql('ALTER TABLE route ADD uuid VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE route DROP guid');
        $this->addSql('DROP INDEX UNIQ_D4E6F812B6FCFB2');
        $this->addSql('ALTER TABLE address ADD uuid VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE address DROP guid');
        $this->addSql('DROP INDEX UNIQ_2E37CE1E2B6FCFB2');
        $this->addSql('ALTER TABLE system_user_role ADD uuid VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE system_user_role DROP guid');
        $this->addSql('DROP INDEX UNIQ_34DCD1762B6FCFB2');
        $this->addSql('ALTER TABLE person ADD uuid VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE person DROP guid');
        $this->addSql('DROP INDEX UNIQ_E7A555AA2B6FCFB2');
        $this->addSql('ALTER TABLE person_status ADD uuid VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE person_status DROP guid');
        $this->addSql('DROP INDEX UNIQ_D34A04AD2B6FCFB2');
        $this->addSql('ALTER TABLE product ADD uuid VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE product DROP guid');
        $this->addSql('DROP INDEX UNIQ_F413DD032B6FCFB2');
        $this->addSql('ALTER TABLE service_request ADD uuid VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_request DROP guid');
        $this->addSql('DROP INDEX UNIQ_333FFB512B6FCFB2');
        $this->addSql('ALTER TABLE service_request_status ADD uuid VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_request_status DROP guid');
        $this->addSql('DROP INDEX UNIQ_4059E0A32B6FCFB2');
        $this->addSql('ALTER TABLE address_status ADD uuid VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE address_status DROP guid');
        $this->addSql('DROP INDEX UNIQ_A700BD082B6FCFB2');
        $this->addSql('ALTER TABLE service_site ADD uuid VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_site DROP guid');
    }
}
