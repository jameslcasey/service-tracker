<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221204214849 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX uniq_7d3656a42b6fcfb2');
        $this->addSql('ALTER TABLE account DROP guid');
        $this->addSql('DROP INDEX uniq_5c34ab6f2b6fcfb2');
        $this->addSql('ALTER TABLE account_status DROP guid');
        $this->addSql('DROP INDEX uniq_d4e6f812b6fcfb2');
        $this->addSql('ALTER TABLE address DROP guid');
        $this->addSql('DROP INDEX uniq_4059e0a32b6fcfb2');
        $this->addSql('ALTER TABLE address_status DROP guid');
        $this->addSql('DROP INDEX uniq_f19287c22b6fcfb2');
        $this->addSql('ALTER TABLE address_type DROP guid');
        $this->addSql('DROP INDEX uniq_34dcd1762b6fcfb2');
        $this->addSql('ALTER TABLE person DROP guid');
        $this->addSql('DROP INDEX uniq_e7a555aa2b6fcfb2');
        $this->addSql('ALTER TABLE person_status DROP guid');
        $this->addSql('DROP INDEX uniq_d34a04ad2b6fcfb2');
        $this->addSql('ALTER TABLE product DROP guid');
        $this->addSql('DROP INDEX uniq_197c24b82b6fcfb2');
        $this->addSql('ALTER TABLE product_status DROP guid');
        $this->addSql('DROP INDEX uniq_2c420792b6fcfb2');
        $this->addSql('ALTER TABLE route DROP guid');
        $this->addSql('DROP INDEX uniq_4af394c12b6fcfb2');
        $this->addSql('ALTER TABLE route_status DROP guid');
        $this->addSql('DROP INDEX uniq_f413dd032b6fcfb2');
        $this->addSql('ALTER TABLE service_request ADD driver VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_request ADD route VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_request ADD products VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_request ADD scheduler VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_request ADD scheduled_for_date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL');
        $this->addSql('ALTER TABLE service_request DROP guid');
        $this->addSql('COMMENT ON COLUMN service_request.scheduled_for_date IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('DROP INDEX uniq_333ffb512b6fcfb2');
        $this->addSql('ALTER TABLE service_request_status DROP guid');
        $this->addSql('DROP INDEX uniq_a700bd082b6fcfb2');
        $this->addSql('ALTER TABLE service_site DROP guid');
        $this->addSql('DROP INDEX uniq_c734edef2b6fcfb2');
        $this->addSql('ALTER TABLE service_site_status DROP guid');
        $this->addSql('DROP INDEX uniq_a967a8952b6fcfb2');
        $this->addSql('ALTER TABLE service_ticket ADD service_request_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE service_ticket ADD driver VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_ticket ADD route VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_ticket ADD products VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_ticket DROP guid');
        $this->addSql('ALTER TABLE service_ticket ADD CONSTRAINT FK_A967A895D42F8111 FOREIGN KEY (service_request_id) REFERENCES service_request (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A967A895D42F8111 ON service_ticket (service_request_id)');
        $this->addSql('DROP INDEX uniq_d1fdb5552b6fcfb2');
        $this->addSql('ALTER TABLE service_ticket_status DROP guid');
        $this->addSql('DROP INDEX uniq_2e37ce1e2b6fcfb2');
        $this->addSql('ALTER TABLE system_user_role DROP guid');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE product ADD guid UUID NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX uniq_d34a04ad2b6fcfb2 ON product (guid)');
        $this->addSql('ALTER TABLE route ADD guid UUID NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX uniq_2c420792b6fcfb2 ON route (guid)');
        $this->addSql('ALTER TABLE address ADD guid UUID NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX uniq_d4e6f812b6fcfb2 ON address (guid)');
        $this->addSql('ALTER TABLE account_status ADD guid UUID NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX uniq_5c34ab6f2b6fcfb2 ON account_status (guid)');
        $this->addSql('ALTER TABLE person ADD guid UUID NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX uniq_34dcd1762b6fcfb2 ON person (guid)');
        $this->addSql('ALTER TABLE service_site ADD guid UUID NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX uniq_a700bd082b6fcfb2 ON service_site (guid)');
        $this->addSql('ALTER TABLE route_status ADD guid UUID NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX uniq_4af394c12b6fcfb2 ON route_status (guid)');
        $this->addSql('ALTER TABLE address_status ADD guid UUID NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX uniq_4059e0a32b6fcfb2 ON address_status (guid)');
        $this->addSql('ALTER TABLE service_ticket_status ADD guid UUID NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX uniq_d1fdb5552b6fcfb2 ON service_ticket_status (guid)');
        $this->addSql('ALTER TABLE service_ticket DROP CONSTRAINT FK_A967A895D42F8111');
        $this->addSql('DROP INDEX UNIQ_A967A895D42F8111');
        $this->addSql('ALTER TABLE service_ticket ADD guid UUID NOT NULL');
        $this->addSql('ALTER TABLE service_ticket DROP service_request_id');
        $this->addSql('ALTER TABLE service_ticket DROP driver');
        $this->addSql('ALTER TABLE service_ticket DROP route');
        $this->addSql('ALTER TABLE service_ticket DROP products');
        $this->addSql('CREATE UNIQUE INDEX uniq_a967a8952b6fcfb2 ON service_ticket (guid)');
        $this->addSql('ALTER TABLE service_request_status ADD guid UUID NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX uniq_333ffb512b6fcfb2 ON service_request_status (guid)');
        $this->addSql('ALTER TABLE person_status ADD guid UUID NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX uniq_e7a555aa2b6fcfb2 ON person_status (guid)');
        $this->addSql('ALTER TABLE service_site_status ADD guid UUID NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX uniq_c734edef2b6fcfb2 ON service_site_status (guid)');
        $this->addSql('ALTER TABLE system_user_role ADD guid UUID NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX uniq_2e37ce1e2b6fcfb2 ON system_user_role (guid)');
        $this->addSql('ALTER TABLE product_status ADD guid UUID NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX uniq_197c24b82b6fcfb2 ON product_status (guid)');
        $this->addSql('ALTER TABLE address_type ADD guid UUID NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX uniq_f19287c22b6fcfb2 ON address_type (guid)');
        $this->addSql('ALTER TABLE service_request ADD guid UUID NOT NULL');
        $this->addSql('ALTER TABLE service_request DROP driver');
        $this->addSql('ALTER TABLE service_request DROP route');
        $this->addSql('ALTER TABLE service_request DROP products');
        $this->addSql('ALTER TABLE service_request DROP scheduler');
        $this->addSql('ALTER TABLE service_request DROP scheduled_for_date');
        $this->addSql('CREATE UNIQUE INDEX uniq_f413dd032b6fcfb2 ON service_request (guid)');
        $this->addSql('ALTER TABLE account ADD guid UUID NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX uniq_7d3656a42b6fcfb2 ON account (guid)');
    }
}
