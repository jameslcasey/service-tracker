<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221127164354 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE account ADD name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE account ADD account_status VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE address ADD address_status VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE address ADD address1 VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE address ADD city VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE address ADD state VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE address ADD zip VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE person ADD person_status VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE person ADD first_name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE person ADD last_name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE product ADD product_status VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE product ADD name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_request ADD service_site_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE service_request ADD service_request_status VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_request ADD CONSTRAINT FK_F413DD03393944DA FOREIGN KEY (service_site_id) REFERENCES service_site (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_F413DD03393944DA ON service_request (service_site_id)');
        $this->addSql('ALTER TABLE service_site ADD service_site_status VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_ticket ADD service_site_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE service_ticket ADD service_ticket_status VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_ticket ADD CONSTRAINT FK_A967A895393944DA FOREIGN KEY (service_site_id) REFERENCES service_site (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_A967A895393944DA ON service_ticket (service_site_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE account DROP name');
        $this->addSql('ALTER TABLE account DROP account_status');
        $this->addSql('ALTER TABLE address DROP address_status');
        $this->addSql('ALTER TABLE address DROP address1');
        $this->addSql('ALTER TABLE address DROP city');
        $this->addSql('ALTER TABLE address DROP state');
        $this->addSql('ALTER TABLE address DROP zip');
        $this->addSql('ALTER TABLE person DROP person_status');
        $this->addSql('ALTER TABLE person DROP first_name');
        $this->addSql('ALTER TABLE person DROP last_name');
        $this->addSql('ALTER TABLE product DROP product_status');
        $this->addSql('ALTER TABLE product DROP name');
        $this->addSql('ALTER TABLE service_request DROP CONSTRAINT FK_F413DD03393944DA');
        $this->addSql('DROP INDEX IDX_F413DD03393944DA');
        $this->addSql('ALTER TABLE service_request DROP service_site_id');
        $this->addSql('ALTER TABLE service_request DROP service_request_status');
        $this->addSql('ALTER TABLE service_ticket DROP CONSTRAINT FK_A967A895393944DA');
        $this->addSql('DROP INDEX IDX_A967A895393944DA');
        $this->addSql('ALTER TABLE service_ticket DROP service_site_id');
        $this->addSql('ALTER TABLE service_ticket DROP service_ticket_status');
        $this->addSql('ALTER TABLE service_site DROP service_site_status');
    }
}
