<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221222220007 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE "user_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE route_service_site (route_id INT NOT NULL, service_site_id INT NOT NULL, PRIMARY KEY(route_id, service_site_id))');
        $this->addSql('CREATE INDEX IDX_CD8EFDFA34ECB4E6 ON route_service_site (route_id)');
        $this->addSql('CREATE INDEX IDX_CD8EFDFA393944DA ON route_service_site (service_site_id)');
        $this->addSql('CREATE TABLE "user" (id INT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON "user" (email)');
        $this->addSql('ALTER TABLE route_service_site ADD CONSTRAINT FK_CD8EFDFA34ECB4E6 FOREIGN KEY (route_id) REFERENCES route (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE route_service_site ADD CONSTRAINT FK_CD8EFDFA393944DA FOREIGN KEY (service_site_id) REFERENCES service_site (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE "user_id_seq" CASCADE');
        $this->addSql('ALTER TABLE route_service_site DROP CONSTRAINT FK_CD8EFDFA34ECB4E6');
        $this->addSql('ALTER TABLE route_service_site DROP CONSTRAINT FK_CD8EFDFA393944DA');
        $this->addSql('DROP TABLE route_service_site');
        $this->addSql('DROP TABLE "user"');
    }
}
