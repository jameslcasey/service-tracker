<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221205010436 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE account_status ADD description VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE address_status ADD description VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE address_type ADD description VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE person_status ADD description VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE product_status ADD description VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE route_status ADD description VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_request_status ADD description VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_site_status ADD description VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE service_ticket_status ADD description VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE system_user_role ADD description VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE service_request_status DROP description');
        $this->addSql('ALTER TABLE person_status DROP description');
        $this->addSql('ALTER TABLE system_user_role DROP description');
        $this->addSql('ALTER TABLE route_status DROP description');
        $this->addSql('ALTER TABLE address_type DROP description');
        $this->addSql('ALTER TABLE address_status DROP description');
        $this->addSql('ALTER TABLE product_status DROP description');
        $this->addSql('ALTER TABLE service_ticket_status DROP description');
        $this->addSql('ALTER TABLE account_status DROP description');
        $this->addSql('ALTER TABLE service_site_status DROP description');
    }
}
