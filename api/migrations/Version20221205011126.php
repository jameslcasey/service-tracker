<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221205011126 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5C34AB6F77153098 ON account_status (code)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4059E0A377153098 ON address_status (code)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F19287C277153098 ON address_type (code)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E7A555AA77153098 ON person_status (code)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_197C24B877153098 ON product_status (code)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4AF394C177153098 ON route_status (code)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_333FFB5177153098 ON service_request_status (code)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C734EDEF77153098 ON service_site_status (code)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D1FDB55577153098 ON service_ticket_status (code)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2E37CE1E77153098 ON system_user_role (code)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP INDEX UNIQ_333FFB5177153098');
        $this->addSql('DROP INDEX UNIQ_E7A555AA77153098');
        $this->addSql('DROP INDEX UNIQ_D1FDB55577153098');
        $this->addSql('DROP INDEX UNIQ_5C34AB6F77153098');
        $this->addSql('DROP INDEX UNIQ_4059E0A377153098');
        $this->addSql('DROP INDEX UNIQ_2E37CE1E77153098');
        $this->addSql('DROP INDEX UNIQ_C734EDEF77153098');
        $this->addSql('DROP INDEX UNIQ_197C24B877153098');
        $this->addSql('DROP INDEX UNIQ_F19287C277153098');
        $this->addSql('DROP INDEX UNIQ_4AF394C177153098');
    }
}
