<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221221152921 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE address ALTER address2 DROP NOT NULL');
        $this->addSql('ALTER TABLE address ALTER coordinates DROP NOT NULL');
        $this->addSql('ALTER TABLE address ALTER what3_words DROP NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE address ALTER address2 SET NOT NULL');
        $this->addSql('ALTER TABLE address ALTER coordinates SET NOT NULL');
        $this->addSql('ALTER TABLE address ALTER what3_words SET NOT NULL');
    }
}
