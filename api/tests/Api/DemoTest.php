<?php

namespace App\Tests\Api;

use ApiPlatform\Symfony\Bundle\Test\ApiTestCase;

class DemoTest extends ApiTestCase
{
    public function testGetDemo(): void
    {
        static::createClient()->request('GET', '/demo');

        $this->assertResponseStatusCodeSame(200);
        $this->assertJsonContains([
            'hello' => 'james',
        ]);
    }

    public function testPostDemo(): void
    {
        static::createClient()->request('POST', '/demo', ['body' =>['name' => 'james']]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertJsonContains([
            'hello' => 'james',
        ]);
    }




}
