<?php

namespace App\Tests\Service;

use App\Entity\ScheduleData;
use App\Service\Scheduler;
use DateInterval;
use DateTime;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;

class SchedulerTest extends TestCase
{

    public function testGetNextServiceDateWithFrequency()
    {
        $serviceUnderTest = new Scheduler();
        $scheduleData = new ScheduleData();
        $scheduleData->setFrequencyAndTimeframe(2, true);
        $actual = $serviceUnderTest->getNextServiceDate($scheduleData);
        $expected = (new DateTimeImmutable())->add(DateInterval::createFromDateString('2 weeks'));
        $this->assertEquals($expected->format('Y-m-d'), $actual->format('Y-m-d'));
    }

    public function testGetNextServiceDateWithWeekday()
    {
        $serviceUnderTest = new Scheduler();
        $scheduleData = new ScheduleData();
        $scheduleData->setSaturday();

        $actual = $serviceUnderTest->getNextServiceDate($scheduleData);

        $dateString = 'saturday';
        if ((int)(new DateTime())->format('w') !== 6) {
            $dateString = 'next ' . $dateString;
        }
        $expected = (new DateTime())->add(DateInterval::createFromDateString($dateString));

        $this->assertEquals($expected->format('Y-m-d'), $actual->format('Y-m-d'));
    }

    /**
     * @dataProvider dataForGetServiceDatesThroughEndDate
     */
    public function testGetServiceDatesThroughEndDateWithFrequency($year, $month, $day)
    {
        $serviceUnderTest = new Scheduler();
        $scheduleData = new ScheduleData();
        $scheduleData->setStartDate((new DateTime())->setDate($year, $month, $day));
        $scheduleData->setFrequencyAndTimeframe(1, true);
        $endDate = (new DateTime())->setDate($year, $month, $day)->add(DateInterval::createFromDateString('1 month'));
        $actual = $serviceUnderTest->getServiceDatesThroughEndDate($scheduleData, $endDate);

        $serviceDates = [
            (new DateTime())->setDate($year, $month, $day)->setTime(0, 0),
            (new DateTime())->setDate($year, $month, $day)->setTime(0,
                0)->add(DateInterval::createFromDateString('1 week')),
            (new DateTime())->setDate($year, $month, $day)->setTime(0,
                0)->add(DateInterval::createFromDateString('2 week')),
            (new DateTime())->setDate($year, $month, $day)->setTime(0,
                0)->add(DateInterval::createFromDateString('3 week')),
            (new DateTime())->setDate($year, $month, $day)->setTime(0,
                0)->add(DateInterval::createFromDateString('4 week')),
        ];

        $expected = new ArrayCollection($serviceDates);
        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider dataForGetServiceDatesThroughEndDate
     */
    public function testGetServiceDatesThroughEndDateWithWeekdays($year, $month, $day)
    {
        $serviceUnderTest = new Scheduler();
        $scheduleData = new ScheduleData();
        $scheduleData->setMonday();

        $startDate = (new DateTime())
            ->setDate($year, $month, $day)
            ->setTime(0, 0);
        $scheduleData->setStartDate($startDate);

        $daysInMonth = $startDate->format('t');
        $endDate = (new DateTime())
            ->setDate($year, $month, $daysInMonth)
            ->setTime(0,0);

        $actual = $serviceUnderTest->getServiceDatesThroughEndDate($scheduleData, $endDate);
        $expected = $this->collectionOfWeekdaysInMonth('Mon', $startDate);
        $this->assertEquals($expected, $actual);
    }

    public function dataForGetServiceDatesThroughEndDate()
    {
        $year = (int)(new DateTime())->add(DateInterval::createFromDateString('1 year'))->format('Y');

        return [
            [$year, 1, 1],
            [$year, 2, 1],
            [$year, 3, 1],
            [$year, 4, 1],
            [$year, 5, 1],
            [$year, 6, 1],
            [$year, 7, 1],
            [$year, 8, 1],
            [$year, 9, 1],
            [$year, 10, 1],
            [$year, 11, 1],
            [$year, 12, 1],
        ];
    }


    private function collectionOfWeekdaysInMonth(string $dayName, DateTime $date)
    {
        $dateCollection = new ArrayCollection();

        $mon = $date->format('F');
        $year = $date->format('Y');

        // https://www.php.net/manual/en/datetime.formats.relative.php#datetime.formats.relative.dayname-note
        $firstDayOfMonth = new DateTime(sprintf('first day of %s %s', $mon, $year));
        $lastDayOfMonth = new DateTime(sprintf('last day of %s %s', $mon, $year));

        $idxDate = clone $firstDayOfMonth;
        while ($idxDate <= $lastDayOfMonth)
        {
            $idxDayName = $idxDate->format('D');
            if ($idxDayName === $dayName) {
                $dateCollection->add(new DateTime($idxDate->format('Y-m-d')));
            }

            $idxDate->add(DateInterval::createFromDateString('1 day'));
        }

        return $dateCollection;
    }

}
